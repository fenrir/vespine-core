package org.fenrir.vespine.core;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140719
 */
public class AuditConstants 
{
	public static final String RESULT_CODE_OK = "OK";
	public static final String RESULT_CODE_ERROR = "ERROR";

	public static final String ACTION_PROJECT_CREATE = "project.create";
	public static final String ACTION_PROJECT_UPDATE = "project.update";
	public static final String ACTION_PROJECT_DELETE = "project.delete";
	
	public static final String ACTION_CATEGORY_CREATE = "category.create";
	public static final String ACTION_CATEGORY_UPDATE = "category.update";
	public static final String ACTION_CATEGORY_DELETE = "category.delete";
	
	public static final String ACTION_SEVERITY_CREATE = "severity.create";
	public static final String ACTION_SEVERITY_UPDATE = "severity.update";
	public static final String ACTION_SEVERITY_DELETE = "severity.delete";
	
	public static final String ACTION_STATUS_CREATE = "status.create";
	public static final String ACTION_STATUS_UPDATE = "status.update";
	public static final String ACTION_STATUS_DELETE = "status.delete";
	
	public static final String ACTION_WORKFLOW_CREATE = "workflow.create";
	public static final String ACTION_WORKFLOW_UPDATE = "workflow.update";
	public static final String ACTION_WORKFLOW_DELETE = "workflow.delete";
	
	public static final String ACTION_TAG_CREATE = "tag.create";
	public static final String ACTION_TAG_UPDATE = "tag.update";
	public static final String ACTION_TAG_DELETE = "tag.delete";
	
	public static final String ACTION_DICTIONARY_CREATE = "dictionary.create";
	public static final String ACTION_DICTIONARY_UPDATE = "dictionary.update";
	public static final String ACTION_DICTIONARY_DELETE = "dictionary.delete";
	
	public static final String ACTION_ALERT_TYPE_CREATE = "alertType.create";
	public static final String ACTION_ALERT_TYPE_UPDATE = "alertType.update";
	public static final String ACTION_ALERT_TYPE_DELETE = "alertType.delete";
	
	public static final String ACTION_SPRINT_CREATE = "sprint.create";
	public static final String ACTION_SPRINT_UPDATE = "sprint.update";
	public static final String ACTION_SPRINT_DELETE = "sprint.delete";
	
	public static final String ACTION_ISSUE_CREATE = "issue.create";
	public static final String ACTION_ISSUE_UPDATE = "issue.update";
	public static final String ACTION_ISSUE_SEND2TRASH_BIN = "issue.send2trashBin";
	public static final String ACTION_ISSUE_RESTORE = "issue.restore";
	public static final String ACTION_ISSUE_DELETE = "issue.delete";
	public static final String ACTION_ISSUE_UPDATE_SLA_DATE = "issue.sla.update";
	public static final String ACTION_ISSUE_TAG_ATTACH = "issue.tag.attach";
	public static final String ACTION_ISSUE_TAG_DETTACH = "issue.tag.dettach";
	
	public static final String ACTION_ISSUE_NOTE_CREATE = "issue.note.create";
	public static final String ACTION_ISSUE_NOTE_UPDATE = "issue.note.update";
	public static final String ACTION_ISSUE_NOTE_DELETE = "issue.note.delete";
	
	public static final String ACTION_CUSTOM_FIELD_CREATE = "issue.customField.create";
	public static final String ACTION_CUSTOM_FIELD_UPDATE = "issue.customField.update";
	public static final String ACTION_CUSTOM_FIELD_DELETE = "issue.customField.delete";
	
	public static final String ACTION_WORK_REGISTRY_CREATE = "issue.workRegistry.create";
	public static final String ACTION_WORK_REGISTRY_UDPATE = "issue.workRegistry.udpate";
	public static final String ACTION_WORK_REGISTRY_DELETE = "issue.workRegistry.delete";
	
	public static final String ACTION_VIEW_FILTER_CREATE = "viewFilter.create";
	public static final String ACTION_VIEW_FILTER_UPDATE = "viewFilter.update";
	public static final String ACTION_VIEW_FILTER_DELETE = "viewFilter.delete";
	public static final String ACTION_VIEW_FILTER_REORDER = "viewFilter.reorder";
	
	public static final String ACTION_SEARCH_INDEX_CREATE = "searchIndex.create";
	public static final String ACTION_SEARCH_INDEX_ENABLE = "searchIndex.enable";
	public static final String ACTION_SEARCH_INDEX_DISABLE = "searchIndex.disable";
}
