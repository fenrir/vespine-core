package org.fenrir.vespine.core;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140803
 */
public class SecurityConstants 
{
	public static final String SCOPE_SERVER = "server";
	public static final String SCOPE_LOCAL = "local";
	
	public static final String GROUP_PROJECT = "project";
	
	public static final String OPERATION_SEARCH = "search";
	public static final String OPERATION_CREATE = "create";
	public static final String OPERATION_UPDATE = "update";
	public static final String OPERATION_DELETE = "delete";
	
	public static final String PERMISSION_SERVER_PROJECT_SEARCH = SCOPE_SERVER + ":" + GROUP_PROJECT + ":" + OPERATION_SEARCH;
	public static final String PERMISSION_SERVER_PROJECT_CREATE = SCOPE_SERVER + ":" + GROUP_PROJECT + ":" + OPERATION_CREATE;
	public static final String PERMISSION_SERVER_PROJECT_UPDATE = SCOPE_SERVER + ":" + GROUP_PROJECT + ":" + OPERATION_UPDATE + ":{0}";
	public static final String PERMISSION_SERVER_PROJECT_DELETE = SCOPE_SERVER + ":" + GROUP_PROJECT + ":" + OPERATION_DELETE + ":{0}";
	
	/* Rols */
	public static final String AUTHORIZATION_ROLE_SYSTEM = "system";
	public static final String AUTHORIZATION_ROLE_ADMINISTRATOR = "administrator";
	public static final String AUTHORIZATION_ROLE_REPORTER = "reporter";
	public static final String AUTHORIZATION_ROLE_OBSERVER = "observer";
}
