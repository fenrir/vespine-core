package org.fenrir.vespine.core;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140111
 */
public class VespineCoreConstants 
{
	public static final String PROVIDER_VESPINE = "VESPINE";
	
	public static final String PROVIDER_ELEMENT_PROJECT = "PROVIDER_PROJECT";
	public static final String PROVIDER_ELEMENT_CATEGORY = "PROVIDER_CATEGORY";
	public static final String PROVIDER_ELEMENT_SEVERITY = "PROVIDER_SEVERITY";
	public static final String PROVIDER_ELEMENT_STATUS = "PROVIDER_STATUS";
}
