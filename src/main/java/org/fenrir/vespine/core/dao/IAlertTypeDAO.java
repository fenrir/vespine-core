package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.AlertType;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IAlertTypeDAO 
{
    public List<AlertType> findAllAlertTypes();
    public AlertType findAlertTypeById(Long id);

    public AlertType createAlertType(AlertType alertType);
    public AlertType updateAlertType(AlertType alertType);
    public void deleteAlertType(AlertType alertType);
}
