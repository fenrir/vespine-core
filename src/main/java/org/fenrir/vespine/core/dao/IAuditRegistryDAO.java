package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.AuditRegistry;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140416
 */
public interface IAuditRegistryDAO 
{
	public List<AuditRegistry> findAllRegistries();
	public List<AuditRegistry> findRegistriesByAction(String action, String objectId);
	public List<AuditRegistry> findRegistriesByActions(List<String> actions, String objectId);
	
	public AuditRegistry findRegistryById(Long id);
	
	public AuditRegistry createRegistry(AuditRegistry registry);
    public AuditRegistry updateRegistry(AuditRegistry registry);
    public void deleteRegistry(AuditRegistry registry);
}
