package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.Dictionary;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140325
 */
public interface IDictionaryDAO 
{
	public List<Dictionary> findAllDictionaries();
	public Dictionary findDictionaryById(Long id);
	public Dictionary findDictionaryByItemId(Long itemId);
	
	public Dictionary createDictionary(Dictionary dictionary);
	public Dictionary updateDictionary(Dictionary dictionary);
	public void deleteDictionary(Dictionary dictionary);
}
