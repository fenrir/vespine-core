package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.DictionaryItem;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140325
 */
public interface IDictionaryItemDAO 
{
	public List<DictionaryItem> findAllItems();
	public List<DictionaryItem> findItemsByDictionaryId(Long dictionaryId);
	public DictionaryItem findItemById(Long id);
	
	public DictionaryItem createItem(DictionaryItem item);
	public DictionaryItem updateItem(DictionaryItem item);
	public void deleteItem(DictionaryItem item);
}
