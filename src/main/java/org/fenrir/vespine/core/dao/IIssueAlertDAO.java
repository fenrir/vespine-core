package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.AlertType;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueAlert;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IIssueAlertDAO
{
    public List<IssueAlert> findAllAlerts();
    public List<IssueAlert> findIssueAlerts(AbstractIssue issue);
    public List<IssueAlert> findAlertsByType(AlertType alertType);
    public List<IssueAlert> findIssueAlertsByType(AlertType alertType, AbstractIssue issue);

    public IssueAlert findAlertById(Long id);

    public IssueAlert createAlert(IssueAlert alert);
    public IssueAlert updateAlert(IssueAlert alert);
    public void deleteAlert(IssueAlert alert);
}
