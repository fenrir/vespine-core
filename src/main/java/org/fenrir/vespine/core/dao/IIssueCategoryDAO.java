package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueProject;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IIssueCategoryDAO 
{
    public List<IssueCategory> findAllCategories();
    public IssueCategory findCategoryById(Long id);
    public IssueCategory findCategory(IssueProject project, String description);
	
    public IssueCategory createCategory(IssueCategory category);
    public IssueCategory updateCategory(IssueCategory category);
    public void deleteCategory(IssueCategory category);
}
