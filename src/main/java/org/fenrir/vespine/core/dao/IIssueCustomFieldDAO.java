package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.FieldType;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueProject;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140325
 */
public interface IIssueCustomFieldDAO 
{
    public List<IssueCustomField> findAllCustomFields();
    public long countAllCustomFields();
    public List<IssueCustomField> findCustomFieldsByProject(IssueProject project);
    public long countCustomFieldsByProject(IssueProject project);
    public List<IssueCustomField> findCustomFieldsByType(FieldType type);
    public long countCustomFieldsByType(FieldType type);
    public IssueCustomField findCustomFieldById(Long id);
    public IssueCustomField createCustomField(IssueCustomField field);
    public IssueCustomField updateCustomField(IssueCustomField field);
    public void deleteCustomField(IssueCustomField field);
}
