package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueCustomValue;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140325
 */
public interface IIssueCustomValueDAO 
{
    public List<IssueCustomValue> findAllCustomValues();
    public List<IssueCustomValue> findCustomValues(IssueCustomField field);
    public List<IssueCustomValue> findIssueCustomValues(AbstractIssue issue);
    public IssueCustomValue findCustomValueById(Long id);
    public IssueCustomValue findIssueCustomValue(AbstractIssue issue, IssueCustomField field);
    public IssueCustomValue createCustomValue(IssueCustomValue value);
    public IssueCustomValue updateCustomValue(IssueCustomValue value);
    public void deleteCustomValue(IssueCustomValue value);
}
