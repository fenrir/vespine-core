package org.fenrir.vespine.core.dao;

import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.core.entity.Tag;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140613
 */
public interface IIssueDAO 
{
    public List<AbstractIssue> findAllIssues();
    public List<AbstractIssue> findAllIssues(int page, int pageSize);
    public long countAllIssues();
    public List<AbstractIssue> findIssuesBySlaDate(Date slaDate);
    public long countIssuesBySlaDate(Date slaDate);
    public List<AbstractIssue> findFilteredIssues(String filterQuery);
    public List<AbstractIssue> findFilteredIssues(String filterQuery, int page, int pageSize);
    public long countFilteredIssues(String filterQuery);
    public List<AbstractIssue> findTaggedIssues(Tag tag);
    public List<AbstractIssue> findTaggedIssues(Tag tag, int page, int pageSize);
    public long countTaggedIssues(Tag tag);
    public List<AbstractIssue> findIssuesByProject(IssueProject project);
    public long countIssuesByProject(IssueProject project);
    public List<AbstractIssue> findIssuesByCategory(IssueCategory category);
    public long countIssuesByCategory(IssueCategory category);
    public List<AbstractIssue> findIssuesBySeverity(IssueSeverity severity);
    public long countIssuesBySeverity(IssueSeverity severity);
    public List<AbstractIssue> findIssuesByStatus(IssueStatus status);
    public long countIssuesByStatus(IssueStatus status);
    public List<AbstractIssue> findIssuesBySprint(Sprint sprint);
    public long countIssuesBySprint(Sprint sprint);
    
    public AbstractIssue findIssueById(Long id);
    public AbstractIssue findIssueByVisibleId(String visibleId);
    public AbstractIssue findLastModifiedIssueByProject(IssueProject project);
    public AbstractIssue findTaggedIssue(Long id, Tag tag);
    
    public AbstractIssue createIssue(AbstractIssue issue);
    public AbstractIssue updateIssue(AbstractIssue issue);
    public void deleteIssue(AbstractIssue issue);
}
