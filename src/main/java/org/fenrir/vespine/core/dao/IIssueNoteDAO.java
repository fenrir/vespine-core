package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueNote;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Intentar eliminar el DAO si es puja la versió de Hibernate per sobre de la 4.0
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IIssueNoteDAO 
{
    public List<IssueNote> findAllNotes();
    public List<IssueNote> findIssueNotes(AbstractIssue issue);
    public IssueNote findNoteById(Long id);
    public IssueNote createNote(IssueNote note);
    public IssueNote updateNote(IssueNote note);
    public void deleteNote(IssueNote note);
}
