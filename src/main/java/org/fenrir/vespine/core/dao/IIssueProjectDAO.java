package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.Workflow;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IIssueProjectDAO 
{
    public List<IssueProject> findAllProjects();
    public List<IssueProject> findProjectsByWorkflow(Workflow workflow);
    public long countProjectsByWorkflow(Workflow workflow);
    public IssueProject findProjectById(Long id);
	
    public IssueProject createProject(IssueProject project);
    public IssueProject updateProject(IssueProject project);
    public void deleteProject(IssueProject project);
}
