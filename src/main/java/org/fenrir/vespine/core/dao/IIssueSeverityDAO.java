package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.IssueSeverity;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.0.20121027
 */
public interface IIssueSeverityDAO 
{
    public List<IssueSeverity> findAllSeverities();
    public IssueSeverity findSeverityById(Long id);
    public IssueSeverity findSeverityBySeverityId(Long severityId);
	
    public IssueSeverity createSeverity(IssueSeverity severity);
    public IssueSeverity updateSeverity(IssueSeverity severity);
    public void deleteSeverity(IssueSeverity severity);
}
