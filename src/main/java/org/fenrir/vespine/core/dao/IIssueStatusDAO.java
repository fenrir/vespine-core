package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.IssueStatus;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IIssueStatusDAO 
{
    public List<IssueStatus> findAllStatus();
    public IssueStatus findStatusById(Long id);
	
    public IssueStatus createStatus(IssueStatus status);
    public IssueStatus updateStatus(IssueStatus status);
    public void deleteStatus(IssueStatus status);
}
