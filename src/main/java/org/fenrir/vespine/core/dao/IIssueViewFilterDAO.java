package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.IssueViewFilter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IIssueViewFilterDAO
{
    public List<IssueViewFilter> findViewFilters(Boolean isApplicationFilter);
    public long countViewFilters(Boolean isApplicationFilter);
    public IssueViewFilter findViewFilterById(Long id);

    public IssueViewFilter createViewFilter(IssueViewFilter viewFilter);
    public IssueViewFilter updateViewFilter(IssueViewFilter viewFilter);
    public void deleteViewFilter(IssueViewFilter viewFilter);
}
