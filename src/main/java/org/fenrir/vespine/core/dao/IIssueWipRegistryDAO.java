package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueWipRegistry;
import org.fenrir.vespine.core.entity.User;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140413
 */
public interface IIssueWipRegistryDAO 
{
	public List<IssueWipRegistry> findAllRegistries();
	public long countAllRegistries();
	public List<IssueWipRegistry> findRegistryByUser(User user);

	public IssueWipRegistry findRegistryById(Long id);
	public IssueWipRegistry findRegistryByUserIssue(AbstractIssue issue, User user);
	
	public IssueWipRegistry createRegistry(IssueWipRegistry registry);
	public IssueWipRegistry updateRegistry(IssueWipRegistry registry);
	public void deleteRegistry(IssueWipRegistry registry);
}
