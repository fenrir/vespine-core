package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueWorkRegistry;
import org.fenrir.vespine.core.entity.User;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140413
 */
public interface IIssueWorkRegistryDAO 
{
	public List<IssueWorkRegistry> findAllWorkRegistries();
	public List<IssueWorkRegistry> findWorkRegistriesByIssue(AbstractIssue issue);
	public List<IssueWorkRegistry> findWorkRegistriesByUser(User user);
    public IssueWorkRegistry findWorkRegistryById(Long id);
	
    public IssueWorkRegistry createWorkRegistry(IssueWorkRegistry workRegistry);
    public IssueWorkRegistry updateWorkRegistry(IssueWorkRegistry workRegistry);
    public void deleteWorkRegistry(IssueWorkRegistry workRegistry);
}
