package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.ProviderCategory;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IProviderCategoryDAO 
{
	public List<ProviderCategory> findAllCategories();
	public List<ProviderCategory> findCategoriesByProvider(String provider);
    public ProviderCategory findCategoryById(Long id);
    public ProviderCategory findCategoryByProviderData(String provider, String providerId);
	
    public ProviderCategory createCategory(ProviderCategory category);
    public ProviderCategory updateCategory(ProviderCategory category);
    public void deleteCategory(ProviderCategory category);
}
