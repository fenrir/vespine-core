package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.ProviderProject;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IProviderProjectDAO 
{
	public List<ProviderProject> findAllProjects();
	public List<ProviderProject> findProjectsByProvider(String provider);
    public ProviderProject findProjectById(Long id);
    public ProviderProject findProjectByProviderData(String provider, String providerId);
	
    public ProviderProject createProject(ProviderProject project);
    public ProviderProject updateProject(ProviderProject project);
    public void deleteProject(ProviderProject project);
}
