package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.ProviderSeverity;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IProviderSeverityDAO 
{
	public List<ProviderSeverity> findAllSeverities();
	public List<ProviderSeverity> findSeveritiesByProvider(String provider);
    public ProviderSeverity findSeverityById(Long id);
    public ProviderSeverity findSeverityByProviderData(String provider, String providerId);
	
    public ProviderSeverity createSeverity(ProviderSeverity severity);
    public ProviderSeverity updateSeverity(ProviderSeverity severity);
    public void deleteSeverity(ProviderSeverity severity);
}
