package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.ProviderStatus;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IProviderStatusDAO 
{
	public List<ProviderStatus> findAllStatus();
	public List<ProviderStatus> findStatusByProvider(String provider);
    public ProviderStatus findStatusById(Long id);
    public ProviderStatus findStatusByProviderData(String provider, String providerId);
	
    public ProviderStatus createStatus(ProviderStatus status);
    public ProviderStatus updateStatus(ProviderStatus status);
    public void deleteStatus(ProviderStatus status);
}
