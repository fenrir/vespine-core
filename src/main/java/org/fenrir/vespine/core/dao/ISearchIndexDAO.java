package org.fenrir.vespine.core.dao;

import java.util.Collection;
import org.fenrir.vespine.core.entity.AbstractIssue;

/**
 * TODO Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface ISearchIndexDAO 
{
    public void indexObject(Object obj, boolean manageTransaction);
    public Collection<AbstractIssue> searchIssues(String query, int page, int size);
    public int countIssueSearchResults(String query);
}
