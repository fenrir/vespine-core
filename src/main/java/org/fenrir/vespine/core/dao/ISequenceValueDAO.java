package org.fenrir.vespine.core.dao;

import java.util.Date;
import org.fenrir.vespine.core.entity.SequenceValue;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140325
 */
public interface ISequenceValueDAO 
{
	public SequenceValue createValue(SequenceValue value);
	public void deleteValue(SequenceValue value);
	public void deleteOlderValues(Date date);
}
