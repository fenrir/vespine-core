package org.fenrir.vespine.core.dao;

import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.Sprint;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface ISprintDAO 
{
	public List<Sprint> findAllSprints();
	public List<Sprint> findSprintsByProjectNameLike(String name);
	
	/**
	 * Mètode de consulta dels sprints en els que la data d'inici conté el fragment de data indicat com a paràmetre
	 * @param startDatePart {@link String} - Fragment de data en format YYYY-MM-DD
	 * @return {@link List}<{@link Sprint}> - Llista d'Sprints en els que la data d'inici conté el fragment indicat
	 */
	public List<Sprint> findSprintsByStartDateLike(String startDatePart);
	
	/**
	 * Mètode de consulta dels sprints en els que la data de final conté el fragment de data indicat com a paràmetre
	 * @param endDatePart {@link String} - Fragment de data en format YYYY-MM-DD
	 * @return {@link List}<{@link Sprint}> - Llista d'Sprints en els que la data de final conté el fragment indicat
	 */
	public List<Sprint> findSprintsByEndDateLike(String endDatePart);
    public Sprint findSprintById(Long id);
    public Sprint findSprintByDate(IssueProject project, Date date);
	
    public Sprint createSprint(Sprint sprint);
    public Sprint updateSprint(Sprint sprint);
    public void deleteSprint(Sprint sprint);
}
