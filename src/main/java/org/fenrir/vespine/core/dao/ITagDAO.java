package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.Tag;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface ITagDAO 
{
    public List<Tag> findAllTags();
    public List<Tag> findTagsByName(String name);
    public List<Tag> findPreferredTags(boolean preferred);
    public List<Tag> findPreferredTagsNameLike(String namePrefix);
    public List<Tag> findTagsNameLike(String namePrefix); 
    public List<Tag> findIssueTags(AbstractIssue issue); 
    public Tag findTagById(Long id);
	
    public Tag createTag(Tag tag);
    public Tag updateTag(Tag tag);
    public void deleteTag(Tag tag);
}
