package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.User;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IUserDAO 
{
	public List<User> findAllUsers();
	public List<User> findAllActiveUsers();
	public List<User> findUsersUsernameLike(String name);
	public List<User> findUsersCompleteNameLike(String name);
	public User findUserById(Long id);
	public User findUserByUsername(String username);
	
	public User createUser(User user);
	public User updateUser(User user);
	public void deleteUser(User user);
}
