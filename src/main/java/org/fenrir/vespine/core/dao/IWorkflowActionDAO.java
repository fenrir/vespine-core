package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.WorkflowAction;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20141019
 */
public interface IWorkflowActionDAO 
{
	public List<WorkflowAction> findAllWorkflowActions();
    public WorkflowAction findWorkflowActionById(Long id);
	
    public WorkflowAction createWorkflowAction(WorkflowAction action);
    public WorkflowAction updateWorkflowAction(WorkflowAction action);
    public void deleteWorkflowAction(WorkflowAction action);
}
