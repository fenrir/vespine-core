package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.Workflow;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IWorkflowDAO 
{	
	public List<Workflow> findAllWorkflows();
    public Workflow findWorkflowById(Long id);
	
    public Workflow createWorkflow(Workflow workflow);
    public Workflow updateWorkflow(Workflow workflow);
    public void deleteWorkflow(Workflow workflow);
}	
