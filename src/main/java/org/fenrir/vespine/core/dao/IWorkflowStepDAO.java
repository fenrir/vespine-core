package org.fenrir.vespine.core.dao;

import java.util.List;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.Workflow;
import org.fenrir.vespine.core.entity.WorkflowStep;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IWorkflowStepDAO 
{
	public List<WorkflowStep> findAllWorkflowSteps();
	public List<WorkflowStep> findStepsByWorkflow(Workflow workflow);
	public List<WorkflowStep> findStepsBySourceStatus(Workflow workflow, IssueStatus status);
	public List<WorkflowStep> findStepsByDestintionStatus(Workflow workflow, IssueStatus status);
    public WorkflowStep findWorkflowStepById(Long id);
	
    public WorkflowStep createWorkflowStep(WorkflowStep workflowStep);
    public WorkflowStep updateWorkflowStep(WorkflowStep workflowStep);
    public void deleteWorkflowStep(WorkflowStep workflowStep);
}
