package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IAlertTypeDAO;
import org.fenrir.vespine.core.entity.AlertType;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class AlertTypeDAOImpl implements IAlertTypeDAO
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject
    private Provider<EntityManager> entityManagerProvider;

    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
    public List<AlertType> findAllAlertTypes() 
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AlertType").getResultList();
    }

    @Override
    @Transactional
    public AlertType findAlertTypeById(Long id) 
    {
        EntityManager em = entityManagerProvider.get();
        return em.find(AlertType.class, id);
    }

    @Override
    @Transactional
    public AlertType createAlertType(AlertType alertType) 
    {
        EntityManager em = entityManagerProvider.get();
        em.persist(alertType);

        return alertType;
    }

    @Override
    @Transactional
    public AlertType updateAlertType(AlertType alertType) 
    {
        /* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
         * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
         */
        AlertType managedAlertType = findAlertTypeById(alertType.getId());
        managedAlertType.setName(alertType.getName());
        managedAlertType.setDescription(alertType.getDescription());
        managedAlertType.setIcon(alertType.getIcon());
        managedAlertType.setAlertRule(alertType.getAlertRule());

        return managedAlertType;
    }

    @Override
    @Transactional
    public void deleteAlertType(AlertType alertType) 
    {
        EntityManager em = entityManagerProvider.get();
        em.remove(alertType);
    }
}
