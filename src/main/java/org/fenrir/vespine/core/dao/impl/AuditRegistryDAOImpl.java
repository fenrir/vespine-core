package org.fenrir.vespine.core.dao.impl;

import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IAuditRegistryDAO;
import org.fenrir.vespine.core.entity.AuditRegistry;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140416
 */
public class AuditRegistryDAOImpl implements IAuditRegistryDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Transactional
	@Override
	public List<AuditRegistry> findAllRegistries() 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AuditRegistry", AuditRegistry.class).getResultList();
	}

    @Transactional
	@Override
	public List<AuditRegistry> findRegistriesByAction(String action, String objectId) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AuditRegistry where action=:action and objectId=:objectId", AuditRegistry.class)
            .setParameter("action", action)
            .setParameter("objectId", objectId)
            .getResultList();
	}

    @Transactional
	@Override
	public List<AuditRegistry> findRegistriesByActions(List<String> actions, String objectId) 
	{
    	if(actions.isEmpty()){
    		return Collections.emptyList();
    	}
    	
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AuditRegistry where action in(:actions) and objectId=:objectId", AuditRegistry.class)
            .setParameter("actions", actions)
            .setParameter("objectId", objectId)
            .getResultList();
	}

    @Transactional
	@Override
	public AuditRegistry findRegistryById(Long id) 
    {
    	EntityManager em = entityManagerProvider.get();
        return em.find(AuditRegistry.class, id);
	}

    @Transactional
	@Override
	public AuditRegistry createRegistry(AuditRegistry registry) 
    {
    	EntityManager em = entityManagerProvider.get();
        em.persist(registry);
        return registry;
	}

    @Transactional
	@Override
	public AuditRegistry updateRegistry(AuditRegistry registry) 
    {
    	EntityManager em = entityManagerProvider.get();
        return em.merge(registry);
	}

    @Transactional
	@Override
	public void deleteRegistry(AuditRegistry registry) 
    {
    	EntityManager em = entityManagerProvider.get();
        em.remove(registry);
	}
}
