package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IDictionaryDAO;
import org.fenrir.vespine.core.entity.Dictionary;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140201
 */
public class DictionaryDAOImpl implements IDictionaryDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
    public List<Dictionary> findAllDictionaries()
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from Dictionary", Dictionary.class).getResultList();
    }
    
    @Override
    @Transactional
	public Dictionary findDictionaryById(Long id) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.find(Dictionary.class, id);
	}
    
    @Override
    @Transactional
    public Dictionary findDictionaryByItemId(Long itemId)
    {
    	EntityManager em = entityManagerProvider.get();
        List<Dictionary> resultList = em.createQuery("select d from Dictionary d join d.items i where i.id=:itemId", Dictionary.class)
            .setParameter("itemId", itemId)
            .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
    }
    
    @Override
	@Transactional
	public Dictionary createDictionary(Dictionary dictionary) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(dictionary);
		
        return dictionary;
	}

	@Override
	@Transactional
	public Dictionary updateDictionary(Dictionary dictionary)
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(dictionary);
	}

	@Override
	@Transactional
	public void deleteDictionary(Dictionary dictionary) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(dictionary);
	}
}
