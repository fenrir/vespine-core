package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IDictionaryItemDAO;
import org.fenrir.vespine.core.entity.DictionaryItem;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140216
 */
public class DictionaryItemDAOImpl implements IDictionaryItemDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
    public List<DictionaryItem> findAllItems()
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from DictionaryItem", DictionaryItem.class).getResultList();
    }
    
    @Override
    @Transactional
    public List<DictionaryItem> findItemsByDictionaryId(Long dictionaryId)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("select i from DictionaryItem i join i.dictionary d where d.id=:dictionaryId", DictionaryItem.class)
            .setParameter("dictionaryId", dictionaryId)
            .getResultList();
    }
    
    @Override
    @Transactional
	public DictionaryItem findItemById(Long id) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.find(DictionaryItem.class, id);
	}
    
    @Override
	@Transactional
	public DictionaryItem createItem(DictionaryItem item) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(item);
		
        return item;
	}

	@Override
	@Transactional
	public DictionaryItem updateItem(DictionaryItem item)
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(item);
	}

	@Override
	@Transactional
	public void deleteItem(DictionaryItem item) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(item);
	}
}
