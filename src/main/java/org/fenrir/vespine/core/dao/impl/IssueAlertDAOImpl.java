package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IIssueAlertDAO;
import org.fenrir.vespine.core.entity.AlertType;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueAlert;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueAlertDAOImpl implements IIssueAlertDAO
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject
    private Provider<EntityManager> entityManagerProvider;

    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Override
    @Transactional
    public List<IssueAlert> findAllAlerts()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueAlert").getResultList();
    }

    @Override
    @Transactional
    public List<IssueAlert> findIssueAlerts(AbstractIssue issue)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueAlert where issue=:issue", IssueAlert.class)
            .setParameter("issue", issue)
            .getResultList();
    }
    
    @Override
    @Transactional
    public List<IssueAlert> findAlertsByType(AlertType alertType)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueAlert where type=:type", IssueAlert.class)
            .setParameter("type", alertType)
            .getResultList();
    }

    @Override
    @Transactional
    public List<IssueAlert> findIssueAlertsByType(AlertType alertType, AbstractIssue issue)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueAlert where type=:type and issue=:issue", IssueAlert.class)
            .setParameter("type", alertType)
            .setParameter("issue", issue)
            .getResultList();
    }

    @Override
    @Transactional
    public IssueAlert findAlertById(Long id)
    {
        EntityManager em = entityManagerProvider.get();
        return em.find(IssueAlert.class, id);
    }

    @Override
    @Transactional
    public IssueAlert createAlert(IssueAlert alert)
    {
        EntityManager em = entityManagerProvider.get();
        em.persist(alert);

        return alert;
    }

    @Override
    @Transactional
    public IssueAlert updateAlert(IssueAlert alert)
    {
        /* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
         * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
         */
        IssueAlert managedAlert = findAlertById(alert.getId());
        managedAlert.setIssue(alert.getIssue());
        managedAlert.setType(alert.getType());
        managedAlert.setCreationDate(alert.getCreationDate());

        return managedAlert;
    }

    @Override
    @Transactional
    public void deleteAlert(IssueAlert alert)
    {
        EntityManager em = entityManagerProvider.get();
        em.remove(alert);
    }
}
