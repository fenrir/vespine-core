package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IIssueCategoryDAO;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueProject;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueCategoryDAOImpl implements IIssueCategoryDAO
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
    public List<IssueCategory> findAllCategories()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueCategory").getResultList();        
    }
    
    @Override
    @Transactional
    public IssueCategory findCategoryById(Long id) 
    {
        EntityManager em = entityManagerProvider.get();
        return em.find(IssueCategory.class, id);
    }

    @Override
    @Transactional
    public IssueCategory findCategory(IssueProject project, String description)
    {
        EntityManager em = entityManagerProvider.get();
        List<IssueCategory> resultList = em.createQuery("FROM IssueCategory WHERE project=:project AND description=:description", IssueCategory.class)
                .setParameter("project", project)
                .setParameter("description", description)
                .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
    }

    @Override
    @Transactional
    public IssueCategory createCategory(IssueCategory category) 
    {
        EntityManager em = entityManagerProvider.get();
        em.persist(category);
		
        return category;
    }

    @Override
    @Transactional
    public IssueCategory updateCategory(IssueCategory category) 
    {
    	EntityManager em = entityManagerProvider.get();
        return em.merge(category);
    }

    @Override
    @Transactional
    public void deleteCategory(IssueCategory category) 
    {
        EntityManager em = entityManagerProvider.get();
        em.remove(category);
    }
}
