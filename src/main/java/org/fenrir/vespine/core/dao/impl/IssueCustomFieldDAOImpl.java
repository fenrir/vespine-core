package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IIssueCustomFieldDAO;
import org.fenrir.vespine.core.entity.FieldType;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueProject;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140202
 */
public class IssueCustomFieldDAOImpl implements IIssueCustomFieldDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
	public List<IssueCustomField> findAllCustomFields() 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueCustomField", IssueCustomField.class).getResultList();
	}
    
    @Override
    @Transactional
    public long countAllCustomFields() 
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(f) from IssueCustomField f", Long.class)
            .getSingleResult();
    }

    @Override
    @Transactional
    public List<IssueCustomField> findCustomFieldsByProject(IssueProject project) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("select distinct f from IssueCustomField f join f.projects p where p=:project", IssueCustomField.class)
            .setParameter("project", project)
            .getResultList();
	}
    
    @Override
    @Transactional
    public long countCustomFieldsByProject(IssueProject project) 
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(f) from IssueCustomField f join f.projects p where p=:project", Long.class)
    		.setParameter("project", project)
            .getSingleResult();
    }

    @Override
    @Transactional
    public List<IssueCustomField> findCustomFieldsByType(FieldType type) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueCustomField where fieldType=:fieldType", IssueCustomField.class)
            .setParameter("fieldType", type)
            .getResultList();
	}
    
    @Override
    @Transactional
    public long countCustomFieldsByType(FieldType type) 
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(f) from IssueCustomField f where fieldType=:fieldType", Long.class)
        		.setParameter("fieldType", type)
            .getSingleResult();
    }

    @Override
    @Transactional
	public IssueCustomField findCustomFieldById(Long id) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.find(IssueCustomField.class, id);
	}

    @Override
    @Transactional
	public IssueCustomField createCustomField(IssueCustomField field) 
	{
    	EntityManager em = entityManagerProvider.get();
        em.persist(field);
		
        return field;
	}

    @Override
    @Transactional
	public IssueCustomField updateCustomField(IssueCustomField field) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.merge(field);
	}

    @Override
    @Transactional
	public void deleteCustomField(IssueCustomField field) 
	{
    	EntityManager em = entityManagerProvider.get();
        em.remove(field);
	}
}
