package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IIssueCustomValueDAO;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueCustomValue;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140119
 */
public class IssueCustomValueDAOImpl implements IIssueCustomValueDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
	public List<IssueCustomValue> findAllCustomValues() 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueCustomValue", IssueCustomValue.class).getResultList();
	}

    @Override
    @Transactional
	public List<IssueCustomValue> findCustomValues(IssueCustomField field) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueCustomValue where field=:field", IssueCustomValue.class)
            .setParameter("field", field)
            .getResultList();
	}
    
    @Override
    @Transactional
	public List<IssueCustomValue> findIssueCustomValues(AbstractIssue issue) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueCustomValue where issue=:issue", IssueCustomValue.class)
            .setParameter("issue", issue)
            .getResultList();
	}

    @Override
    @Transactional
	public IssueCustomValue findCustomValueById(Long id) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.find(IssueCustomValue.class, id);
	}
    
    @Override
    @Transactional
	public IssueCustomValue findIssueCustomValue(AbstractIssue issue, IssueCustomField field) 
	{
    	EntityManager em = entityManagerProvider.get();
        List<IssueCustomValue> resultList = em.createQuery("from IssueCustomValue where issue=:issue and field=:field", IssueCustomValue.class)
            .setParameter("issue", issue)
            .setParameter("field", field)
            .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
	}

    @Override
    @Transactional
	public IssueCustomValue createCustomValue(IssueCustomValue value) 
	{
    	EntityManager em = entityManagerProvider.get();
        em.persist(value);
		
        return value;
	}

    @Override
    @Transactional
	public IssueCustomValue updateCustomValue(IssueCustomValue value) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.merge(value);
	}

    @Override
    @Transactional
	public void deleteCustomValue(IssueCustomValue value) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(value);
	}
}
