package org.fenrir.vespine.core.dao.impl;

import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IIssueDAO;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.core.entity.Tag;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140613
 */
public class IssueDAOImpl implements IIssueDAO
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Override
    @Transactional
    public List<AbstractIssue> findAllIssues()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AbstractIssue", AbstractIssue.class)
                .getResultList();
    }

    @Override
    @Transactional
    public List<AbstractIssue> findAllIssues(int page, int pageSize)
    {
        int firstResult = page * pageSize;
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AbstractIssue", AbstractIssue.class)
                .setFirstResult(firstResult)
                .setMaxResults(pageSize)
                .getResultList();
    }

    @Override
    @Transactional
    public long countAllIssues()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from AbstractIssue i", Long.class)
                .getSingleResult();
    }

    @Override
    @Transactional
    public List<AbstractIssue> findIssuesBySlaDate(Date slaDate)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AbstractIssue where slaDate=:slaDate", AbstractIssue.class)
            .setParameter("slaDate", slaDate)
            .getResultList();
    }

    @Override
    @Transactional
    public long countIssuesBySlaDate(Date slaDate)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from AbstractIssue i where slaDate=:slaDate", Long.class)
            .setParameter("slaDate", slaDate)
            .getSingleResult();
    }

    @Override
    @Transactional
    public List<AbstractIssue> findFilteredIssues(String filterQuery)
    {
        String completeQuery = "select issue.*, issue.type as clazz_ " + filterQuery;
        EntityManager em = entityManagerProvider.get();
        return em.createNativeQuery(completeQuery, AbstractIssue.class)
                .getResultList();
    }

    @Override
    @Transactional
    public List<AbstractIssue> findFilteredIssues(String filterQuery, int page, int pageSize)
    {
        int firstResult = page * pageSize;
        String completeQuery = "select issue.*, issue.type as clazz_ " + filterQuery;
        EntityManager em = entityManagerProvider.get();
        return em.createNativeQuery(completeQuery, AbstractIssue.class)
                .setFirstResult(firstResult)
                .setMaxResults(pageSize)
                .getResultList();
    }

    @Override
    @Transactional
    public long countFilteredIssues(String filterQuery)
    {
        String completeQuery = "select count(issue.id) " + filterQuery;
        EntityManager em = entityManagerProvider.get();
        return ((Number)em.createNativeQuery(completeQuery)
                .getSingleResult()).longValue();
    }

    @Override
    @Transactional
    public List<AbstractIssue> findTaggedIssues(Tag tag)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("select i from AbstractIssue i join i.tags t where t=:tag", AbstractIssue.class)
            .setParameter("tag", tag)
            .getResultList();
    }

    @Override
    @Transactional
    public List<AbstractIssue> findTaggedIssues(Tag tag, int page, int pageSize)
    {
        int firstResult = page * pageSize;
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("select i from AbstractIssue i join i.tags t where t=:tag", AbstractIssue.class)
            .setParameter("tag", tag)
            .setFirstResult(firstResult)
            .setMaxResults(pageSize)
            .getResultList();
    }

    @Override
    @Transactional
    public long countTaggedIssues(Tag tag)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from AbstractIssue i join i.tags t where t=:tag", Long.class)
            .setParameter("tag", tag)
            .getSingleResult();
    }
    
    @Override
    @Transactional
    public List<AbstractIssue> findIssuesByProject(IssueProject project)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AbstractIssue where project=:project", AbstractIssue.class)
            .setParameter("project", project)
            .getResultList();
    }
    
    @Override
    @Transactional
    public long countIssuesByProject(IssueProject project)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from AbstractIssue i where project=:project", Long.class)
    		.setParameter("project", project)
            .getSingleResult();
    }
    
    @Override
    @Transactional
    public List<AbstractIssue> findIssuesByCategory(IssueCategory category)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AbstractIssue where category=:category", AbstractIssue.class)
            .setParameter("category", category)
            .getResultList();
    }
    
    @Override
    @Transactional
    public long countIssuesByCategory(IssueCategory category)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from AbstractIssue i where category=:category", Long.class)
    		.setParameter("category", category)
            .getSingleResult();
    }

    @Override
    @Transactional
    public List<AbstractIssue> findIssuesBySeverity(IssueSeverity severity)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AbstractIssue where severity=:severity", AbstractIssue.class)
            .setParameter("severity", severity)
            .getResultList();
    }
    
    @Override
    @Transactional
    public long countIssuesBySeverity(IssueSeverity severity)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from AbstractIssue i where severity=:severity", Long.class)
    		.setParameter("severity", severity)
            .getSingleResult();
    }
    
    @Override
    @Transactional
    public List<AbstractIssue> findIssuesByStatus(IssueStatus status)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AbstractIssue where status=:status", AbstractIssue.class)
    		.setParameter("status", status)
            .getResultList();
    }
    
    @Override
    @Transactional
    public long countIssuesByStatus(IssueStatus status)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from AbstractIssue i where status=:status", Long.class)
    		.setParameter("status", status)
            .getSingleResult();
    }
    
    @Override
    @Transactional
    public List<AbstractIssue> findIssuesBySprint(Sprint sprint)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from AbstractIssue where sprint=:sprint", AbstractIssue.class)
    		.setParameter("sprint", sprint)
            .getResultList();
    }
    
    @Override
    @Transactional
    public long countIssuesBySprint(Sprint sprint)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from AbstractIssue i where sprint=:sprint", Long.class)
    		.setParameter("sprint", sprint)
            .getSingleResult();
    }
    
    @Override
    @Transactional
    public AbstractIssue findIssueById(Long id) 
    {
        EntityManager em = entityManagerProvider.get();
        return em.find(AbstractIssue.class, id);
    }
    
    @Override
    @Transactional
    public AbstractIssue findIssueByVisibleId(String visibleId)
    {
    	EntityManager em = entityManagerProvider.get();
        List<AbstractIssue> resultList = em.createQuery("from AbstractIssue where visibleId=:visibleId", AbstractIssue.class)
            .setParameter("visibleId", visibleId)
            .getResultList();

        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
    }

    @Override
    @Transactional
    public AbstractIssue findLastModifiedIssueByProject(IssueProject project)
    {
        EntityManager em = entityManagerProvider.get();
        List<AbstractIssue> resultList = em.createQuery("from AbstractIssue where project=:project order by modifiedDate desc", AbstractIssue.class)
            .setParameter("project", project)
            .getResultList();

        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
    }

    @Override
    @Transactional
    public AbstractIssue findTaggedIssue(Long id, Tag tag)
    {
        EntityManager em = entityManagerProvider.get();
        List<AbstractIssue> resultList = em.createQuery("select i from AbstractIssue i join i.tags t where i.id=:id and t=:tag", AbstractIssue.class)
            .setParameter("id", id)
            .setParameter("tag", tag)
            .getResultList();

        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
    }

    @Override
    @Transactional
    public AbstractIssue createIssue(AbstractIssue issue) 
    {
        EntityManager em = entityManagerProvider.get();
        em.persist(issue);
		
        return issue;
    }

    @Override
    @Transactional
    public AbstractIssue updateIssue(AbstractIssue issue) 
    {
        EntityManager em = entityManagerProvider.get();
        return em.merge(issue);
    }

    @Override
    @Transactional
    public void deleteIssue(AbstractIssue issue) 
    {
        EntityManager em = entityManagerProvider.get();
        em.remove(issue);
    }    
}
