package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IIssueNoteDAO;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueNote;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueNoteDAOImpl implements IIssueNoteDAO
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject
    private Provider<EntityManager> entityManagerProvider;

    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Override
    @Transactional
    public List<IssueNote> findAllNotes()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueNote", IssueNote.class).getResultList();
    }

    @Override
    @Transactional
    public List<IssueNote> findIssueNotes(AbstractIssue issue)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueNote where issue=:issue", IssueNote.class)
            .setParameter("issue", issue)
            .getResultList();
    }
       
    @Override
    @Transactional
    public IssueNote findNoteById(Long id)
    {
        EntityManager em = entityManagerProvider.get();
        return em.find(IssueNote.class, id);
    }

    @Override
    @Transactional
    public IssueNote createNote(IssueNote note)
    {
        EntityManager em = entityManagerProvider.get();
        em.persist(note);

        return note;
    }

    @Override
    @Transactional
    public IssueNote updateNote(IssueNote note)
    {
    	EntityManager em = entityManagerProvider.get();
    	return em.merge(note);
    }

    @Override
    @Transactional
    public void deleteNote(IssueNote note)
    {
        EntityManager em = entityManagerProvider.get();
        em.remove(note);
    }
}
