package org.fenrir.vespine.core.dao.impl;

import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.fenrir.vespine.core.dao.IIssueProjectDAO;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.Workflow;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueProjectDAOImpl implements IIssueProjectDAO
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
    public List<IssueProject> findAllProjects()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueProject", IssueProject.class).getResultList();        
    }
    
    @Override
    @Transactional
    public List<IssueProject> findProjectsByWorkflow(Workflow workflow)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueProject where workflow=:workflow", IssueProject.class)
            .setParameter("workflow", workflow)
            .getResultList();
    }
    
    @Override
    @Transactional
    public long countProjectsByWorkflow(Workflow workflow)
    {
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from IssueProject i where workflow=:workflow", Long.class)
    		.setParameter("workflow", workflow)
            .getSingleResult();
    }
    
    @Override
    @Transactional
    public IssueProject findProjectById(Long id) 
    {
        EntityManager em = entityManagerProvider.get();
        return em.find(IssueProject.class, id);
    }

    @Override
    @Transactional
    public IssueProject createProject(IssueProject project) 
    {
        EntityManager em = entityManagerProvider.get();
        em.persist(project);
		
        return project;
    }

    @Override
    @Transactional
    public IssueProject updateProject(IssueProject project) 
    {
    	EntityManager em = entityManagerProvider.get();
        return em.merge(project);
    }

    @Override
    @Transactional
    public void deleteProject(IssueProject project) 
    {
        EntityManager em = entityManagerProvider.get();
        em.remove(project);
    }    
}
