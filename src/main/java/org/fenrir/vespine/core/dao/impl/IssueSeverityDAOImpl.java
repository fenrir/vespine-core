package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IIssueSeverityDAO;
import org.fenrir.vespine.core.entity.IssueSeverity;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueSeverityDAOImpl implements IIssueSeverityDAO
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
    public List<IssueSeverity> findAllSeverities()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueSeverity").getResultList();        
    }
    
    @Override
    @Transactional
    public IssueSeverity findSeverityById(Long id) 
    {
        EntityManager em = entityManagerProvider.get();
        return em.find(IssueSeverity.class, id);
    }
    
    @Override
    @Transactional
    public IssueSeverity findSeverityBySeverityId(Long severityId)
    {
        EntityManager em = entityManagerProvider.get();
        List<IssueSeverity> resultList = em.createQuery("FROM IssueSeverity WHERE severityId=:severityId", IssueSeverity.class)
                .setParameter("severityId", severityId)
                .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
    }

    @Override
    @Transactional
    public IssueSeverity createSeverity(IssueSeverity severity) 
    {
        EntityManager em = entityManagerProvider.get();
        em.persist(severity);
		
        return severity;
    }

    @Override
    @Transactional
    public IssueSeverity updateSeverity(IssueSeverity severity) 
    {
    	EntityManager em = entityManagerProvider.get();
        return em.merge(severity);
    }

    @Override
    @Transactional
    public void deleteSeverity(IssueSeverity severity) 
    {
        EntityManager em = entityManagerProvider.get();
        em.remove(severity);
    }
}
