package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IIssueStatusDAO;
import org.fenrir.vespine.core.entity.IssueStatus;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueStatusDAOImpl implements IIssueStatusDAO
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
    public List<IssueStatus> findAllStatus()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueStatus").getResultList();        
    }
    
    @Override
    @Transactional
    public IssueStatus findStatusById(Long id) 
    {
        EntityManager em = entityManagerProvider.get();
        return em.find(IssueStatus.class, id);
    }

    @Override
    @Transactional
    public IssueStatus createStatus(IssueStatus status) 
    {
        EntityManager em = entityManagerProvider.get();
        em.persist(status);
		
        return status;
    }

    @Override
    @Transactional
    public IssueStatus updateStatus(IssueStatus status) 
    {
    	EntityManager em = entityManagerProvider.get();
        return em.merge(status);
    }

    @Override
    @Transactional
    public void deleteStatus(IssueStatus status) 
    {
        EntityManager em = entityManagerProvider.get();
        em.remove(status);
    }
}
