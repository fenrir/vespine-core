package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IIssueViewFilterDAO;
import org.fenrir.vespine.core.entity.IssueViewFilter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueViewFilterDAOImpl implements IIssueViewFilterDAO
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject
    private Provider<EntityManager> entityManagerProvider;

    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Override
    @Transactional
    public List<IssueViewFilter> findViewFilters(Boolean isApplicationFilter)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueViewFilter i where i.applicationFilter=:applicationFilter order by filterOrder asc", IssueViewFilter.class)
                .setParameter("applicationFilter", isApplicationFilter)
                .getResultList();
    }

    @Override
    @Transactional
    public long countViewFilters(Boolean isApplicationFilter)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from IssueViewFilter i where i.applicationFilter=:applicationFilter", Long.class)
                .setParameter("applicationFilter", isApplicationFilter)
                .getSingleResult();
    }
    
    @Override
    @Transactional
    public IssueViewFilter findViewFilterById(Long id)
    {
        EntityManager em = entityManagerProvider.get();
        return em.find(IssueViewFilter.class, id);
    }

    @Override
    @Transactional
    public IssueViewFilter createViewFilter(IssueViewFilter viewFilter)
    {
        EntityManager em = entityManagerProvider.get();
        em.persist(viewFilter);

        return viewFilter;
    }

    @Override
    @Transactional
    public IssueViewFilter updateViewFilter(IssueViewFilter viewFilter)
    {
        /* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
         * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
         */
        IssueViewFilter managedViewFilter = findViewFilterById(viewFilter.getId());
        managedViewFilter.setName(viewFilter.getName());
        managedViewFilter.setDescription(viewFilter.getDescription());
        managedViewFilter.setFilterQuery(viewFilter.getFilterQuery());
        managedViewFilter.setOrderByClause(viewFilter.getOrderByClause());
        managedViewFilter.setApplicationFilter(viewFilter.isApplicationFilter());
        managedViewFilter.setFilterOrder(viewFilter.getFilterOrder());

        return managedViewFilter;
    }

    @Override
    @Transactional
    public void deleteViewFilter(IssueViewFilter viewFilter)
    {
        EntityManager em = entityManagerProvider.get();
        em.remove(viewFilter);
    }
}
