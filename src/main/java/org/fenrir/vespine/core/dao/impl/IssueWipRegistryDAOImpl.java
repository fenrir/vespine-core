package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IIssueWipRegistryDAO;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueWipRegistry;
import org.fenrir.vespine.core.entity.User;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140413
 */
public class IssueWipRegistryDAOImpl implements IIssueWipRegistryDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
	@Override
	@Transactional
	public List<IssueWipRegistry> findAllRegistries() 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueWipRegistry", IssueWipRegistry.class).getResultList();
	}

	@Override
    @Transactional
    public long countAllRegistries()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from IssueWipRegistry i", Long.class)
                .getSingleResult();
    }
	
	@Override
	@Transactional
	public List<IssueWipRegistry> findRegistryByUser(User user)
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueWipRegistry where user=:user", IssueWipRegistry.class)
            .setParameter("user", user)
            .getResultList();
	}
	
	@Override
	@Transactional
	public IssueWipRegistry findRegistryById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(IssueWipRegistry.class, id);
	}
	
	@Override
	@Transactional
	public IssueWipRegistry findRegistryByUserIssue(AbstractIssue issue, User user)
	{
		EntityManager em = entityManagerProvider.get();
        List<IssueWipRegistry> resultList =  em.createQuery("from IssueWipRegistry where issue=:issue and user=:user", IssueWipRegistry.class)
            .setParameter("issue", issue)
            .setParameter("user", user)
            .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
	}

	@Override
	@Transactional
	public IssueWipRegistry createRegistry(IssueWipRegistry registry) 
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(registry);
		
		return registry;
	}

	@Override
	@Transactional
	public IssueWipRegistry updateRegistry(IssueWipRegistry registry) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(registry);
	}

	@Override
	@Transactional
	public void deleteRegistry(IssueWipRegistry registry) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(registry);
	}
}
