package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

import org.fenrir.vespine.core.dao.IIssueWorkRegistryDAO;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueWorkRegistry;
import org.fenrir.vespine.core.entity.User;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140413
 */
public class IssueWorkRegistryDAOImpl implements IIssueWorkRegistryDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
    @Transactional
	@Override
	public List<IssueWorkRegistry> findAllWorkRegistries() 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueWorkRegistry", IssueWorkRegistry.class).getResultList();
	}

    @Transactional
	@Override
	public List<IssueWorkRegistry> findWorkRegistriesByIssue(AbstractIssue issue) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueWorkRegistry where issue=:issue", IssueWorkRegistry.class)
            .setParameter("issue", issue)
            .getResultList();
	}
    
    @Transactional
	@Override
	public List<IssueWorkRegistry> findWorkRegistriesByUser(User user) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueWorkRegistry where user=:user", IssueWorkRegistry.class)
            .setParameter("user", user)
            .getResultList();
	}

    @Transactional
	@Override
	public IssueWorkRegistry findWorkRegistryById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(IssueWorkRegistry.class, id);
	}

    @Transactional
	@Override
	public IssueWorkRegistry createWorkRegistry(IssueWorkRegistry workRegistry) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(workRegistry);
        return workRegistry;
	}

    @Transactional
	@Override
	public IssueWorkRegistry updateWorkRegistry(IssueWorkRegistry workRegistry) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(workRegistry);
	}

    @Transactional
	@Override
	public void deleteWorkRegistry(IssueWorkRegistry workRegistry) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(workRegistry);
	}
}
