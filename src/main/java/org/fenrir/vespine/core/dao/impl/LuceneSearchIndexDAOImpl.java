package org.fenrir.vespine.core.dao.impl;

import java.util.Collection;
import java.util.HashSet;
import javax.inject.Inject;
import org.compass.core.Compass;
import org.compass.core.CompassHit;
import org.compass.core.CompassQuery;
import org.compass.core.CompassSession;
import org.compass.core.CompassTransaction;
import org.compass.core.support.search.CompassSearchCommand;
import org.compass.core.support.search.CompassSearchHelper;
import org.compass.core.support.search.CompassSearchResults;
import org.fenrir.vespine.core.dao.ISearchIndexDAO;
import org.fenrir.vespine.core.entity.AbstractIssue;

/**
 * TODO Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class LuceneSearchIndexDAOImpl implements ISearchIndexDAO
{
    private final String LUCENE_SPECIAL_CHARACTERS = "[\\[\\]\\\\+\\-\\!\\(\\)\\:\\^\\]\\{\\}\\~\\*\\?]";
	
    @Inject
    private Compass compass;
	
    public void setCompass(Compass compass)
    {
        this.compass = compass;
    }
    
    @Override
    public void indexObject(Object obj, boolean manageTransaction)
    {
        CompassSession session = compass.openSession();
        CompassTransaction tx = null;
        try {
            if(manageTransaction){
                tx = session.beginTransaction();
            }	
            /* Si només es guarda i hi ha canvis en els camps del POJO de tipus transient calculats no es reindexaran correctament
             * Fins trobar una altra sol.lució, s'esborra i es reidexa el concepte
             */
            session.delete(obj);
            session.save(obj);
            if(tx!=null){
                tx.commit();
            }
        } 
        catch(RuntimeException e){
            if(tx != null) {
                tx.rollback();
                throw e;
            }
        } 
        finally{
            session.close();
        }		
    }
    
    @Override
    public Collection<AbstractIssue> searchIssues(String query, int page, int size)
    {   
        CompassSearchHelper searchHelper = new CompassSearchHelper(compass, size);
        CompassQuery compassQuery = compass.queryBuilder().queryString(query).useOrDefaultOperator().toQuery();
        Collection<AbstractIssue> vHits = new HashSet<AbstractIssue>();
        CompassSearchResults results = searchHelper.search(new CompassSearchCommand(compassQuery, page));	
        for(CompassHit hit : results.getHits()){
            vHits.add((AbstractIssue)hit.getData());
        }            
            
        return vHits;        
    }
    
    @Override
    public int countIssueSearchResults(String query)
    {
        
        CompassSearchHelper searchHelper = new CompassSearchHelper(compass, 1);	
        CompassQuery compassQuery = compass.queryBuilder().queryString(query).useOrDefaultOperator().toQuery();
        CompassSearchResults results = searchHelper.search(new CompassSearchCommand(compassQuery, 0));		            

        return results.getTotalHits();        
    }	
}
