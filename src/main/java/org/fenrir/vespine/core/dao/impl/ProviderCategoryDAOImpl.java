package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IProviderCategoryDAO;
import org.fenrir.vespine.core.entity.ProviderCategory;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class ProviderCategoryDAOImpl implements IProviderCategoryDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
	public List<ProviderCategory> findAllCategories() 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from ProviderCategory").getResultList();
	}

	@Override
	@Transactional
	public List<ProviderCategory> findCategoriesByProvider(String provider) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from ProviderCategory where provider=:provider", ProviderCategory.class)
            .setParameter("provider", provider)
            .getResultList();
	}

	@Override
	@Transactional
	public ProviderCategory findCategoryById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(ProviderCategory.class, id);
	}

	@Override
	@Transactional
	public ProviderCategory findCategoryByProviderData(String provider, String providerId) 
	{
		EntityManager em = entityManagerProvider.get();
        List<ProviderCategory> resultList = em.createQuery("from ProviderCategory where provider=:provider", ProviderCategory.class)
            .setParameter("provider", provider)
            .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de donar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
	}

	@Override
	@Transactional
	public ProviderCategory createCategory(ProviderCategory category) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(category);
		
        return category;
	}

	@Override
	@Transactional
	public ProviderCategory updateCategory(ProviderCategory category) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(category);
	}

	@Override
	@Transactional
	public void deleteCategory(ProviderCategory category) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(category);
	}
}
