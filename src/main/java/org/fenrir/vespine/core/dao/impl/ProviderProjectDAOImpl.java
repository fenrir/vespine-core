package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IProviderProjectDAO;
import org.fenrir.vespine.core.entity.ProviderProject;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class ProviderProjectDAOImpl implements IProviderProjectDAO
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
	public List<ProviderProject> findAllProjects() 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from ProviderProject").getResultList();
	}

	@Override
	@Transactional
	public List<ProviderProject> findProjectsByProvider(String provider) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from ProviderProject where provider=:provider", ProviderProject.class)
            .setParameter("provider", provider)
            .getResultList();
	}

	@Override
	@Transactional
	public ProviderProject findProjectById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(ProviderProject.class, id);
	}

	@Override
	@Transactional
	public ProviderProject findProjectByProviderData(String provider, String providerId) 
	{
		EntityManager em = entityManagerProvider.get();
        List<ProviderProject> resultList = em.createQuery("from ProviderProject where provider=:provider", ProviderProject.class)
            .setParameter("provider", provider)
            .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de donar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
	}

	@Override
	@Transactional
	public ProviderProject createProject(ProviderProject project) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(project);
		
        return project;
	}

	@Override
	@Transactional
	public ProviderProject updateProject(ProviderProject project) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(project);
	}

	@Override
	@Transactional
	public void deleteProject(ProviderProject project) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(project);
	}
}
