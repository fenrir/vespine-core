package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IProviderSeverityDAO;
import org.fenrir.vespine.core.entity.ProviderSeverity;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class ProviderSeverityDAOImpl implements IProviderSeverityDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
	public List<ProviderSeverity> findAllSeverities() 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from ProviderSeverity").getResultList();
	}

	@Override
	@Transactional
	public List<ProviderSeverity> findSeveritiesByProvider(String provider) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from ProviderSeverity where provider=:provider", ProviderSeverity.class)
            .setParameter("provider", provider)
            .getResultList();
	}

	@Override
	@Transactional
	public ProviderSeverity findSeverityById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(ProviderSeverity.class, id);
	}

	@Override
	@Transactional
	public ProviderSeverity findSeverityByProviderData(String provider, String providerId) 
	{
		EntityManager em = entityManagerProvider.get();
        List<ProviderSeverity> resultList = em.createQuery("from ProviderSeverity where provider=:provider", ProviderSeverity.class)
            .setParameter("provider", provider)
            .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de donar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
	}

	@Override
	@Transactional
	public ProviderSeverity createSeverity(ProviderSeverity severity) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(severity);
		
        return severity;
	}

	@Override
	@Transactional
	public ProviderSeverity updateSeverity(ProviderSeverity severity) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(severity);
	}

	@Override
	@Transactional
	public void deleteSeverity(ProviderSeverity severity) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(severity);
	}
}
