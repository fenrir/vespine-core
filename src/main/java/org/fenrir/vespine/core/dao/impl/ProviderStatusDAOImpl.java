package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IProviderStatusDAO;
import org.fenrir.vespine.core.entity.ProviderStatus;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class ProviderStatusDAOImpl implements IProviderStatusDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
	public List<ProviderStatus> findAllStatus() 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from ProviderStatus").getResultList();
	}

	@Override
	@Transactional
	public List<ProviderStatus> findStatusByProvider(String provider) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from ProviderStatus where provider=:provider", ProviderStatus.class)
            .setParameter("provider", provider)
            .getResultList();
	}

	@Override
	@Transactional
	public ProviderStatus findStatusById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(ProviderStatus.class, id);
	}

	@Override
	@Transactional
	public ProviderStatus findStatusByProviderData(String provider, String providerId) 
	{
		EntityManager em = entityManagerProvider.get();
        List<ProviderStatus> resultList = em.createQuery("from ProviderStatus where provider=:provider", ProviderStatus.class)
            .setParameter("provider", provider)
            .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de donar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
	}

	@Override
	@Transactional
	public ProviderStatus createStatus(ProviderStatus status) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(status);
		
        return status;
	}

	@Override
	@Transactional
	public ProviderStatus updateStatus(ProviderStatus status) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(status);
	}

	@Override
	@Transactional
	public void deleteStatus(ProviderStatus status) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(status);
	}
}
