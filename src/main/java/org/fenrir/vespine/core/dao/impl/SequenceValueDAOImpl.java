package org.fenrir.vespine.core.dao.impl;

import java.util.Date;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.ISequenceValueDAO;
import org.fenrir.vespine.core.entity.SequenceValue;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140323
 */
public class SequenceValueDAOImpl implements ISequenceValueDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
	@Transactional
	@Override
	public SequenceValue createValue(SequenceValue value) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(value);
		
        return value;
	}

	@Transactional
	@Override
	public void deleteValue(SequenceValue value) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(value);
	}

	@Transactional
	@Override
	public void deleteOlderValues(Date date) 
	{
		EntityManager em = entityManagerProvider.get();
        em.createQuery("delete from SequenceValue where lastUpdated<:date")
            .setParameter("date", date)
            .executeUpdate();
	}
}
