package org.fenrir.vespine.core.dao.impl;

import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.ISprintDAO;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.Sprint;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class SprintDAOImpl implements ISprintDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
	@Override
	@Transactional
	public List<Sprint> findAllSprints() 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from Sprint", Sprint.class).getResultList();
	}
	
	@Override
	@Transactional
	public List<Sprint> findSprintsByProjectNameLike(String name)
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("select s from Sprint s join s.project p where upper(p.name) like :name", Sprint.class)
            .setParameter("name", "%" + name.toUpperCase() + "%")
            .getResultList();
	}
	
	@Override
	@Transactional
	public List<Sprint> findSprintsByStartDateLike(String startDatePart)
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from Sprint where cast(startDate as string) like :startDatePart", Sprint.class)
            .setParameter("startDatePart", "%" + startDatePart + "%")
            .getResultList();
	}
	
	@Override
	@Transactional
	public List<Sprint> findSprintsByEndDateLike(String endDatePart)
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from Sprint where cast(endDate as string) like :endDatePart", Sprint.class)
            .setParameter("endDatePart", "%" + endDatePart + "%")
            .getResultList();
	}

	@Override
	@Transactional
	public Sprint findSprintById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(Sprint.class, id);
	}

	@Override
	@Transactional
	public Sprint findSprintByDate(IssueProject project, Date date)
	{
		EntityManager em = entityManagerProvider.get();
        List<Sprint> resultList = em.createQuery("from Sprint where project=:project and :date between startDate and endDate", Sprint.class)
            .setParameter("project", project)
            .setParameter("date", date)
            .getResultList();

        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
	}
	
	@Override
	@Transactional
	public Sprint createSprint(Sprint sprint) 
	{
		 EntityManager em = entityManagerProvider.get();
		 em.persist(sprint);
		
		 return sprint;
	}

	@Override
	@Transactional
	public Sprint updateSprint(Sprint sprint) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(sprint);
	}

	@Override
	@Transactional
	public void deleteSprint(Sprint sprint) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(sprint);
	}
}
