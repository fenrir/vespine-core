package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.ITagDAO;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.Tag;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class TagDAOImpl implements ITagDAO
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Override
    @Transactional
    public List<Tag> findAllTags()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from Tag").getResultList();        
    }

    @Override
    @Transactional
    public List<Tag> findTagsByName(String name)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from Tag where upper(name)=:name", Tag.class)
                .setParameter("name", name.toUpperCase())
                .getResultList();        
    }
    
    @Override
    @Transactional
    public List<Tag> findPreferredTags(boolean preferred)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from Tag where preferred=:preferred", Tag.class)
                .setParameter("preferred", preferred)
                .getResultList();        
    }
    
    @Override
    @Transactional
    public List<Tag> findPreferredTagsNameLike(String namePrefix)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("FROM Tag WHERE preferred=:preferred AND name LIKE :namePrefix", Tag.class)
                .setParameter("preferred", Boolean.TRUE)
                .setParameter("namePrefix", namePrefix + "%")
                .getResultList();
    }
    
    @Override
    @Transactional
    public List<Tag> findTagsNameLike(String namePrefix)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("FROM Tag WHERE upper(name) LIKE :namePrefix", Tag.class)
                .setParameter("namePrefix", namePrefix.toUpperCase() + "%")
                .getResultList();
    }
    
    @Override
    @Transactional
    public List<Tag> findIssueTags(AbstractIssue issue)
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("SELECT DISTINCT t FROM Tag t JOIN t.issues i WHERE i=:issue")
                .setParameter("issue", issue)
                .getResultList();       
    }
    
    @Override
    @Transactional
    public Tag findTagById(Long id) 
    {
        EntityManager em = entityManagerProvider.get();
        return em.find(Tag.class, id);
    }

    @Override
    @Transactional
    public Tag createTag(Tag tag) 
    {
        EntityManager em = entityManagerProvider.get();
        em.persist(tag);
		
        return tag;
    }

    @Override
    @Transactional
    public Tag updateTag(Tag tag) 
    {
        /* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
         * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
         */
        Tag managedTag = findTagById(tag.getId());
        managedTag.setName(tag.getName());
        managedTag.setIssues(tag.getIssues());
		
        return managedTag;
    }

    @Override
    @Transactional
    public void deleteTag(Tag tag) 
    {
        EntityManager em = entityManagerProvider.get();
        em.remove(tag);
    }    
}
