package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IUserDAO;
import org.fenrir.vespine.core.entity.User;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class UserDAOImpl implements IUserDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
	@Override
	@Transactional
	public List<User> findAllUsers() 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from User", User.class).getResultList();
	}

	@Override
	@Transactional
	public List<User> findAllActiveUsers() 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from User where active=:active", User.class)
            .setParameter("active", Boolean.TRUE)
            .getResultList();
	}
	
	@Override
	@Transactional
	public List<User> findUsersUsernameLike(String name)
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from User where UPPER(username) like :name", User.class)
	            .setParameter("name", "%" + name.toUpperCase() + "%")
	            .getResultList();
	}
	
	@Override
	@Transactional
	public List<User> findUsersCompleteNameLike(String name)
	{
		EntityManager em = entityManagerProvider.get();
		return em.createQuery("from User where UPPER(completeName) like :name", User.class)
				.setParameter("name", "%" + name.toUpperCase() + "%")
	            .getResultList();
	}

	@Override
	@Transactional
	public User findUserById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(User.class, id);
	}

	@Override
	@Transactional
	public User findUserByUsername(String username) 
	{
		EntityManager em = entityManagerProvider.get();
        List<User> resultList = em.createQuery("from User where username=:username", User.class)
            .setParameter("username", username)
            .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
	}

	@Override
	@Transactional
	public User createUser(User user) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(user);
		
        return user;
	}

	@Override
	@Transactional
	public User updateUser(User user) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(user);
	}

	@Override
	@Transactional
	public void deleteUser(User user) 
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(user);
	}
}
