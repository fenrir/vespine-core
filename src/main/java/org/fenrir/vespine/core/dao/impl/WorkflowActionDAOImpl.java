package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IWorkflowActionDAO;
import org.fenrir.vespine.core.entity.WorkflowAction;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20141019
 */
public class WorkflowActionDAOImpl implements IWorkflowActionDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
    @Override
    @Transactional
	public List<WorkflowAction> findAllWorkflowActions() 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from WorkflowAction", WorkflowAction.class).getResultList();
	}

    @Override
    @Transactional
	public WorkflowAction findWorkflowActionById(Long id) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.find(WorkflowAction.class, id);
	}

    @Override
    @Transactional
	public WorkflowAction createWorkflowAction(WorkflowAction action) 
	{
    	EntityManager em = entityManagerProvider.get();
        em.persist(action);
		
        return action;
	}

    @Override
    @Transactional
	public WorkflowAction updateWorkflowAction(WorkflowAction action) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.merge(action);
	}

    @Override
    @Transactional
	public void deleteWorkflowAction(WorkflowAction action) 
	{
    	EntityManager em = entityManagerProvider.get();
        em.remove(action);
	}
}
