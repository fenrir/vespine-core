package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IWorkflowDAO;
import org.fenrir.vespine.core.entity.Workflow;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class WorkflowDAOImpl implements IWorkflowDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
	public List<Workflow> findAllWorkflows() 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from Workflow", Workflow.class).getResultList();
	}

    @Override
    @Transactional
	public Workflow findWorkflowById(Long id) 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.find(Workflow.class, id);
	}

	@Override
	@Transactional
	public Workflow createWorkflow(Workflow workflow) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(workflow);
		
        return workflow;
	}

	@Override
	@Transactional
	public Workflow updateWorkflow(Workflow workflow)
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(workflow);
	}

	@Override
	@Transactional
	public void deleteWorkflow(Workflow workflow) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(workflow);
	}
}
