package org.fenrir.vespine.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IWorkflowStepDAO;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.Workflow;
import org.fenrir.vespine.core.entity.WorkflowStep;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class WorkflowStepDAOImpl implements IWorkflowStepDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    @Override
    @Transactional
	public List<WorkflowStep> findAllWorkflowSteps() 
	{
    	EntityManager em = entityManagerProvider.get();
        return em.createQuery("from WorkflowStep", WorkflowStep.class).getResultList();
	}

	@Override
	@Transactional
	public List<WorkflowStep> findStepsByWorkflow(Workflow workflow) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from WorkflowStep where workflow=:workflow", WorkflowStep.class)
            .setParameter("workflow", workflow)
            .getResultList();
	}

	@Override
	@Transactional
	public List<WorkflowStep> findStepsBySourceStatus(Workflow workflow, IssueStatus status) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from WorkflowStep where workflow=:workflow and sourceStatus=:status", WorkflowStep.class)
    		.setParameter("workflow", workflow)
            .setParameter("status", status)
            .getResultList();
	}

	@Override
	@Transactional
	public List<WorkflowStep> findStepsByDestintionStatus(Workflow workflow, IssueStatus status) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from WorkflowStep where workflow=:workflow and destinationStatus=:status", WorkflowStep.class)
    		.setParameter("workflow", workflow)
            .setParameter("status", status)
            .getResultList();
	}

	@Override
	@Transactional
	public WorkflowStep findWorkflowStepById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(WorkflowStep.class, id);
	}

	@Override
	@Transactional
	public WorkflowStep createWorkflowStep(WorkflowStep workflowStep) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(workflowStep);
		
        return workflowStep;
	}

	@Override
	@Transactional
	public WorkflowStep updateWorkflowStep(WorkflowStep workflowStep) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(workflowStep);
	}

	@Override
	@Transactional
	public void deleteWorkflowStep(WorkflowStep workflowStep) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(workflowStep);
	}
}
