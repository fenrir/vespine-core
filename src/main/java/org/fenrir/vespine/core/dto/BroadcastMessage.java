package org.fenrir.vespine.core.dto;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140815
 */
public class BroadcastMessage 
{
	public static final String MESSAGE_TYPE_SIMPLE = "SIMPLE";
	public static final String MESSAGE_TYPE_CONNECTION = "CONNECTION";
	
	public static final String MESSAGE_TYPE_ISSUE_CREATED = "ISSUE_CREATED";
	public static final String MESSAGE_TYPE_ISSUE_UPDATED = "ISSUE_UPDATED";
	public static final String MESSAGE_TYPE_ISSUE_DELETED = "ISSUE_DELETED";
	
	public static final String MESSAGE_TYPE_PROJECT_CREATED = "PROJECT_CREATED";
	public static final String MESSAGE_TYPE_PROJECT_UPDATED = "PROJECT_UPDATED";
	public static final String MESSAGE_TYPE_PROJECT_DELETED = "PROJECT_DELETED";
	
	public static final String MESSAGE_TYPE_CATEGORY_CREATED = "CATEGORY_CREATED";
	public static final String MESSAGE_TYPE_CATEGORY_UPDATED = "CATEGORY_UPDATED";
	public static final String MESSAGE_TYPE_CATEGORY_DELETED = "CATEGORY_DELETED";
	
	public static final String MESSAGE_TYPE_SEVERITY_CREATED = "SEVERITY_CREATED";
	public static final String MESSAGE_TYPE_SEVERITY_UPDATED = "SEVERITY_UPDATED";
	public static final String MESSAGE_TYPE_SEVERITY_DELETED = "SEVERITY_DELETED";
	
	public static final String MESSAGE_TYPE_STATUS_CREATED = "STATUS_CREATED";
	public static final String MESSAGE_TYPE_STATUS_UPDATED = "STATUS_UPDATED";
	public static final String MESSAGE_TYPE_STATUS_DELETED = "STATUS_DELETED";
	
	public static final String MESSAGE_TYPE_WORKFLOW_CREATED = "WORKFLOW_CREATED";
	public static final String MESSAGE_TYPE_WORKFLOW_UPDATED = "WORKFLOW_UPDATED";
	public static final String MESSAGE_TYPE_WORKFLOW_DELETED = "WORKFLOW_DELETED";
	
	public static final String MESSAGE_TYPE_DICTIONARY_CREATED = "DICTIONARY_CREATED";
	public static final String MESSAGE_TYPE_DICTIONARY_UPDATED = "DICTIONARY_UPDATED";
	public static final String MESSAGE_TYPE_DICTIONARY_DELETED = "DICTIONARY_DELETED";
	
	public static final String MESSAGE_TYPE_CUSTOM_FIELD_CREATED = "CUSTOM_FIELD_CREATED";
	public static final String MESSAGE_TYPE_CUSTOM_FIELD_UPDATED = "CUSTOM_FIELD_UPDATED";
	public static final String MESSAGE_TYPE_CUSTOM_FIELD_DELETED = "CUSTOM_FIELD_DELETED";
	
	public static final String MESSAGE_TYPE_VIEW_FILTER_CREATED = "VIEW_FILTER_CREATED";
	public static final String MESSAGE_TYPE_VIEW_FILTER_UPDATED = "VIEW_FILTER_UPDATED";
	public static final String MESSAGE_TYPE_VIEW_FILTER_DELETED = "VIEW_FILTER_DELETED";
	public static final String MESSAGE_TYPE_VIEW_FILTER_REORDER = "VIEW_FILTER_REORDER";
	
	public static final String MESSAGE_TYPE_ALERT_TYPE_CREATED = "ALERT_TYPE_CREATED";
	public static final String MESSAGE_TYPE_ALERT_TYPE_UPDATED = "ALERT_TYPE_UPDATED";
	public static final String MESSAGE_TYPE_ALERT_TYPE_DELETED = "ALERT_TYPE_DELETED";
	
	// TODO
	public static final String MESSAGE_TYPE_TAG_LIST_UPDATED = "TAG_LIST_UPDATED";
	
	public static final String MESSAGE_TYPE_SPRINT_CREATED = "SPRINT_CREATED";
	public static final String MESSAGE_TYPE_SPRINT_UPDATED = "SPRINT_UPDATED";
	public static final String MESSAGE_TYPE_SPRINT_DELETED = "SPRINT_DELETED";
	
	public static final String MESSAGE_EXTRA_DATA_CONNECTION_ID = "CONNECTION_ID";
	public static final String MESSAGE_EXTRA_DATA_OLD_VALUE = "OLD_VALUE";
	public static final String MESSAGE_EXTRA_DATA_NEW_VALUE = "NEW_VALUE";
	
	private String type;
	private String originator;
	private String message; 
	private Map<String, Object> extraData;
	private Date timestamp;
	
	public String getType() 
	{
		return type;
	}
	
	public void setType(String type) 
	{
		this.type = type;
	}
	
	public String getOriginator()
	{
		return originator;
	}
	
	public void setOriginator(String originator)
	{
		this.originator = originator;
	}
	
	public String getMessage() 
	{
		return message;
	}
	
	public void setMessage(String message) 
	{
		this.message = message;
	}
	
	public Map<String, Object> getExtraData()
	{
		if(extraData==null){
			return Collections.emptyMap();
		}
		return extraData;
	}
	
	public void setExtraData(Map<String, Object> extraData)
	{
		this.extraData = extraData;
	}
	
	public void putExtraData(String key, Object value)
	{
		if(extraData==null){
			extraData = new HashMap<String, Object>();
		}
		extraData.put(key, value);
	}
	
	public Date getTimestamp() 
	{
		if(timestamp==null){
			return new Date();
		}
		return timestamp;
	}
	
	public void setTimestamp(Date timestamp) 
	{
		this.timestamp = timestamp;
	}
	
	@Override
	public String toString()
	{
		return "[" + type + "] " + message;
	}
}
