package org.fenrir.vespine.core.dto;

import java.util.Date;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140105
 */
public interface IAlertTypeDTO 
{
	public String getAlertTypeId();
	public String getName();
	public String getDescription();
	public String getIcon();
	public String getAlertRule();
	public Date getCreationDate();
	public Date getLastUpdated();
}
