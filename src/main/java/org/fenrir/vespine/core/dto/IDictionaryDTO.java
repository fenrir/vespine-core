package org.fenrir.vespine.core.dto;

import java.util.Date;
import java.util.List;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140420
 */
public interface IDictionaryDTO 
{
	public String getDictionaryId();
	public String getName();
	public String getDescription();
	public List<IDictionaryItemDTO> getDictionaryItems();
	public Date getLastUpdated();
}
