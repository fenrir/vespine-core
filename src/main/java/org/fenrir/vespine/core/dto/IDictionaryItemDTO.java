package org.fenrir.vespine.core.dto;

import java.util.Date;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140402
 */
public interface IDictionaryItemDTO 
{
	public String getItemId();
	public IDictionaryDTO getDictionary();
	public String getValue();
	public String getDescription();
	public Date getLastUpdated();
}
