package org.fenrir.vespine.core.dto;

import java.util.Date;
import org.fenrir.vespine.spi.dto.IIssueDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140107
 */
public interface IIssueAlertDTO 
{
	public String getAlertId();
	public IIssueDTO getIssue();
	public IAlertTypeDTO getType();
	public Date getCreationDate();
}
