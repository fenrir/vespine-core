package org.fenrir.vespine.core.dto;

import java.util.Date;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140523
 */
public interface IIssueWipRegistryDTO 
{
	public String getRegistryId();
	public IIssueDTO getIssue();
	public IUserDTO getUser();
	public Date getLastUpdated();
}
