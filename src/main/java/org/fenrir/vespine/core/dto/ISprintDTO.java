package org.fenrir.vespine.core.dto;

import java.util.Date;
import org.fenrir.vespine.spi.dto.IProjectDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140110
 */
public interface ISprintDTO 
{
	public String getSprintId();
	public IProjectDTO getProject();
	public Date getStartDate();
	public Date getEndDate();
	public Date getLastUpdated();
}
