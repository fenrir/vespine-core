package org.fenrir.vespine.core.dto;

import java.util.List;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140111
 */
public interface IVespineCategoryDTO extends ICategoryDTO 
{
	public List<IProviderElementDTO> getProviderCategories();
}
