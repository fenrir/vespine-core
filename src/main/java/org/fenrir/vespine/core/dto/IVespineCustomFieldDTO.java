package org.fenrir.vespine.core.dto;

import java.util.List;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140513
 */
public interface IVespineCustomFieldDTO extends ICustomFieldDTO 
{
	public String getDataProviderId();
	public List<IVespineProjectDTO> getProjects();
}
