package org.fenrir.vespine.core.dto;

import java.util.List;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140505
 */
public interface IVespineIssueDTO extends IIssueDTO 
{
	public ISprintDTO getSprint();
	public List<IWorkRegistryDTO> getWorkRegistries();
	public Integer getWorkTime();
	public String getWorkTimeFormated();
	public boolean isDeleted();
}
