package org.fenrir.vespine.core.dto;

import java.util.List;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140111
 */
public interface IVespineProjectDTO extends IProjectDTO
{
	public List<IProviderElementDTO> getProviderProjects();
    public IWorkflowDTO getWorkflow();
}
