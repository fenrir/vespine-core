package org.fenrir.vespine.core.dto;

import java.util.Date;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140511
 */
public interface IViewFilterDTO 
{
	public String getFilterId();
	public String getName();
	public String getDescription();
	public String getFilterQuery();
	public String getOrderClause();
	public Boolean isApplicationFilter();
	public Long getFilterOrder();
	public Date getCreationDate();
	public Date getLastUpdated();
	
	public Boolean isTransient();
}
