package org.fenrir.vespine.core.dto;

import java.util.Date;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140504
 */
public interface IWorkRegistryDTO 
{
	public String getRegistryId();
	public IIssueDTO getIssue();
	public String getDescription();
	public ISprintDTO getSprint();
	public IUserDTO getUser();
	public Date getWorkingDay();
	public Integer getTime();
	public Date getLastUpdated();
}
