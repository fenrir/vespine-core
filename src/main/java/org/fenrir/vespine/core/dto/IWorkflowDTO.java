package org.fenrir.vespine.core.dto;

import java.util.Date;
import java.util.List;

import org.fenrir.vespine.spi.dto.IStatusDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140105
 */
public interface IWorkflowDTO 
{
	public String getWorkflowId();
	public String getName();
	public IStatusDTO getInitialStatus();
	public List<IWorkflowStepDTO> getWorkflowSteps();
	public Date getLastUpdated();
}
