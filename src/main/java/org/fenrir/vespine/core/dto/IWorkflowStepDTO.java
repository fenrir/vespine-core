package org.fenrir.vespine.core.dto;

import java.util.Date;
import org.fenrir.vespine.spi.dto.IStatusDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140105
 */
public interface IWorkflowStepDTO 
{
	public String getWorkflowStepId();
	public IWorkflowDTO getWorkflow();
	public IStatusDTO getSourceStatus();
	public IStatusDTO getDestinationStatus();
	public Date getLastUpdated();
}
