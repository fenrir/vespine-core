package org.fenrir.vespine.core.dto;

import java.util.Collection;
import org.apache.commons.lang.StringUtils;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class SearchQuery 
{
    public static final String INDEX_FIELD_ISSUE_CLASS = "issueClass";
    public static final String INDEX_FIELD_ISSUE_ID = "issueId";
    public static final String INDEX_FIELD_ISSUE_DESCRIPTION = "description";    
    public static final String INDEX_FIELD_ISSUE_SUMMARY = "summary";    
    public static final String INDEX_FIELD_ISSUE_ASSIGNED_USER = "assignedUser";    
    public static final String INDEX_FIELD_NOTE_CONTENT = "content";
        
    private String indexQuery;	
    
    private int page;
    private int size;
    
    public String getIndexQuery() 
    {
        return indexQuery;
    }
	
    public void setIndexQuery(String indexQuery) 
    {
        this.indexQuery = indexQuery;
    }					
	
    public void addIndexQueryPart(String field, String value, boolean mustOperation) 
    {
        indexQuery = StringUtils.defaultString(indexQuery);
        indexQuery += mustOperation ? " +" : " ";			
        indexQuery += field + ":(" + value + ")";
    }
	
    public void addIndexQueryPart(String field, Collection<String> values, boolean mustOperation) 
    {
        indexQuery = StringUtils.defaultString(indexQuery);
        indexQuery += mustOperation ? " +" : " ";			
        indexQuery += field + ":(" + StringUtils.join(values, " ") + ")";
    }
	
    public int getPage() 
    {
        return page;
    }
	
    public void setPage(int page) 
    {
        this.page = page;
    }
	
    public int getSize() 
    {
        return size;
    }
	
    public void setSize(int size) 
    {
        this.size = size;
    }	
}
