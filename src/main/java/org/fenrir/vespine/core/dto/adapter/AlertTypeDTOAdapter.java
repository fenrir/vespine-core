package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;
import org.fenrir.vespine.core.entity.AlertType;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140112
 */
public class AlertTypeDTOAdapter implements IAlertTypeDTO 
{
	private AlertType alertType;
	
	public AlertTypeDTOAdapter(AlertType alertType)
	{
		this.alertType = alertType;
	}
	
	@Override
	public String getAlertTypeId() 
	{
		return alertType.getId().toString();
	}

	@Override
	public String getName() 
	{
		return alertType.getName();
	}

	@Override
	public String getDescription() 
	{
		return alertType.getDescription();
	}

	@Override
	public String getIcon() 
	{
		return alertType.getIcon();
	}

	@Override
	public String getAlertRule() 
	{
		return alertType.getAlertRule();
	}

	@Override
	public Date getCreationDate() 
	{
		return alertType.getCreationDate();
	}

	@Override
	public Date getLastUpdated() 
	{
		return alertType.getLastUpdated();
	}

	@Override
    public int hashCode()
    {
        return getAlertTypeId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IAlertTypeDTO)){
            return false;
        }

        IAlertTypeDTO other = (IAlertTypeDTO)obj;
        return getAlertTypeId().equals(other.getAlertTypeId());
    }
}
