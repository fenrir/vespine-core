package org.fenrir.vespine.core.dto.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.dto.IVespineCategoryDTO;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.ProviderCategory;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140111
 */
public class CategoryDTOAdapter implements IVespineCategoryDTO 
{
	private IssueCategory category;
	
	public CategoryDTOAdapter(IssueCategory category)
	{
		this.category = category;
	}

	@Override
	public String getProvider()
	{
		return VespineCoreConstants.PROVIDER_VESPINE;
	}
	
	@Override
	public String getCategoryId() 
	{
		return category.getId().toString();
	}

	@Override
	public String getName() 
	{
		return category.getName();
	}

	@Override
	public IProjectDTO getProject() 
	{
		if(category.getProject()!=null){
			return new ProjectDTOAdapter(category.getProject());
		}
		
		return null;
	}

	@Override
	public List<IProviderElementDTO> getProviderCategories() 
	{
		List<IProviderElementDTO> elements = new ArrayList<IProviderElementDTO>();
    	if(category.getProviderCategories()!=null){
            for(ProviderCategory elem:category.getProviderCategories()){
                elements.add(new ProviderCategoryDTOAdapter(elem));
            }
        }
        
        return elements;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return category.getLastUpdated();
	}

	@Override
	public String toString()
	{
		return category.toString();
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getCategoryId() == null) ? 0 : getCategoryId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ICategoryDTO)){
            return false;
        }
        
        final ICategoryDTO other = (ICategoryDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Category ID */
        if(getCategoryId()==null){
            if(other.getCategoryId()!=null){
                return false;
            }
        } 
        else if(!getCategoryId().equals(other.getCategoryId())){
            return false;
        }      
        
        return true;
    }
}
