package org.fenrir.vespine.core.dto.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.dto.IVespineCustomFieldDTO;
import org.fenrir.vespine.core.dto.IVespineProjectDTO;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.IFieldType;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140601
 */
public class CustomFieldDTOAdapter implements IVespineCustomFieldDTO 
{
	private IssueCustomField customField;
	
	public CustomFieldDTOAdapter(IssueCustomField customField)
	{
		this.customField = customField;
	}
	
	@Override
	public String getProvider() 
	{
		return VespineCoreConstants.PROVIDER_VESPINE;
	}

	@Override
	public String getFieldId() 
	{
		return customField.getId().toString();
	}

	@Override
	public String getName() 
	{
		return customField.getName();
	}

	@Override
	public boolean isMandatory() 
	{
		return customField.isMandatory();
	}

	@Override
	public Date getLastUpdated() 
	{
		return customField.getLastUpdated();
	}

	@Override
	public IFieldType getFieldType() 
	{
		return new FieldTypeDTOAdapter(customField.getFieldType());
	}

	@Override
	public String getDataProviderId() 
	{
		return customField.getDataProviderId();
	}

	@Override
	public List<IVespineProjectDTO> getProjects() 
	{
		List<IVespineProjectDTO> projects = new ArrayList<IVespineProjectDTO>();
		
		for(IssueProject project:customField.getProjects()){
			projects.add(new ProjectDTOAdapter(project));
		}
		
		return projects;
	}
	
	@Override
	public String toString()
	{
		return customField.toString();
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getFieldId() == null) ? 0 : getFieldId().hashCode());
        
        return result;
    }
	
	@Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ICustomFieldDTO)){
            return false;
        }
        
        final ICustomFieldDTO other = (ICustomFieldDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Field ID */
        if(getFieldId()==null){
            if(other.getFieldId()!=null){
                return false;
            }
        } 
        else if(!getFieldId().equals(other.getFieldId())){
            return false;
        }      
        
        return true;
    }
}
