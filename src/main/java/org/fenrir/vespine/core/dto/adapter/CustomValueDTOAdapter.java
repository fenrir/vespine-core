package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.entity.IssueCustomValue;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.ICustomValueDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140110
 */
public class CustomValueDTOAdapter implements ICustomValueDTO 
{
	private IssueCustomValue customValue;
	
	public CustomValueDTOAdapter(IssueCustomValue customValue)
	{
		this.customValue = customValue;
	}
	
	@Override
	public String getProvider() 
	{
		return VespineCoreConstants.PROVIDER_VESPINE;
	}

	@Override
	public String getValueId() 
	{
		return customValue.getId().toString();
	}

	@Override
	public ICustomFieldDTO getField() 
	{
		return new CustomFieldDTOAdapter(customValue.getField());
	}

	@Override
	public IIssueDTO getIssue() 
	{
		return new IssueDTOAdapter(customValue.getIssue());
	}

	@Override
	public String getValue() 
	{
		return customValue.getValue();
	}

	@Override
	public Date getLastUpdated() 
	{
		return customValue.getLastUpdated();
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getValueId() == null) ? 0 : getValueId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ICustomValueDTO)){
            return false;
        }
        
        final ICustomValueDTO other = (ICustomValueDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Value ID */
        if(getValueId()==null){
            if(other.getValueId()!=null){
                return false;
            }
        } 
        else if(!getValueId().equals(other.getValueId())){
            return false;
        }      
        
        return true;
    }
}
