package org.fenrir.vespine.core.dto.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;
import org.fenrir.vespine.core.entity.Dictionary;
import org.fenrir.vespine.core.entity.DictionaryItem;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140715
 */
public class DictionaryDTOAdapter implements IDictionaryDTO 
{
	private Dictionary dictionary;
	
	public DictionaryDTOAdapter(Dictionary dictionary)
	{
		this.dictionary = dictionary;
	}
	
	@Override
	public String getDictionaryId() 
	{
		return dictionary.getId().toString();
	}

	@Override
	public String getName() 
	{
		return dictionary.getName();
	}

	@Override
	public String getDescription() 
	{
		return dictionary.getDescription();
	}
	
	@Override
	@JsonManagedReference
	public List<IDictionaryItemDTO> getDictionaryItems()
	{
		List<IDictionaryItemDTO> items = new ArrayList<IDictionaryItemDTO>();
		for(DictionaryItem item:dictionary.getItems()){
			items.add(new DictionaryItemDTOAdapter(item));
		}
		return items;
	}

	@Override
	public Date getLastUpdated() 
	{
		return dictionary.getLastUpdated();
	}

	@Override
	public String toString()
	{
		return dictionary.toString();
	}
	
	@Override
    public int hashCode()
    {
        return getDictionaryId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IDictionaryDTO)){
            return false;
        }

        IDictionaryDTO other = (IDictionaryDTO)obj;
        return getDictionaryId().equals(other.getDictionaryId());
    }
}
