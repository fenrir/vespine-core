package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;
import org.fenrir.vespine.core.entity.DictionaryItem;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140715
 */
public class DictionaryItemDTOAdapter implements IDictionaryItemDTO 
{
	private DictionaryItem dictionaryItem;
	
	public DictionaryItemDTOAdapter(DictionaryItem dictionaryItem)
	{
		this.dictionaryItem = dictionaryItem;
	}
	
	@Override
	public String getItemId() 
	{
		return dictionaryItem.getId().toString();
	}

	@Override
	@JsonBackReference
	public IDictionaryDTO getDictionary()
	{
		if(dictionaryItem.getDictionary()!=null){
			return new DictionaryDTOAdapter(dictionaryItem.getDictionary());
		}
		return null;
	}
	
	@Override
	public String getValue() 
	{
		return dictionaryItem.getValue();
	}

	@Override
	public String getDescription() 
	{
		return dictionaryItem.getDescription();
	}

	@Override
	public Date getLastUpdated() 
	{
		return dictionaryItem.getLastUpdated();
	}
	
	@Override
	public String toString()
	{
		return dictionaryItem.toString();
	}
	
	@Override
    public int hashCode()
    {
        return getItemId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IDictionaryItemDTO)){
            return false;
        }

        IDictionaryItemDTO other = (IDictionaryItemDTO)obj;
        return getItemId().equals(other.getItemId());
    }
}
