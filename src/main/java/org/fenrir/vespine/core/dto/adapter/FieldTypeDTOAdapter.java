package org.fenrir.vespine.core.dto.adapter;

import java.util.Map;
import org.codehaus.jackson.annotate.JsonGetter;
import org.fenrir.vespine.core.util.DictionaryItemDTOConverter;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.spi.util.IAttributeConverter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140613
 */
public class FieldTypeDTOAdapter implements IFieldType 
{
	private IFieldType fieldType;
	
	public FieldTypeDTOAdapter(IFieldType fieldType)
	{
		this.fieldType = fieldType;
	}
	
	@Override
	public String getType() 
	{
		return fieldType.getType();
	}

	@Override
	public Class<? extends IAttributeConverter<?, ?>> getConverter() 
	{
		if(IFieldType.TYPE_DICTIONARY.equals(fieldType.getType())){
			return DictionaryItemDTOConverter.class;
		}
		
		return fieldType.getConverter();
	}

	@Override
	public Map<String, String> getConverterParameters() 
	{
		return fieldType.getConverterParameters();
	}

	@Override
	@JsonGetter("dataProviderid")
	public boolean needDataProviderId() 
	{
		return fieldType.needDataProviderId();
	}

	@Override
	public String toString()
	{
		return fieldType.toString();
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fieldType.getType() == null) ? 0 : fieldType.getType().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(this == obj){
			return true;
		}
		if(obj == null){
			return false;
		}
		if(getClass() != obj.getClass()){
			return false;
		}
		
		IFieldType other = (IFieldType) obj;
		if(fieldType.getType()==null){
			if(other.getType()!=null){
				return false;
			}
		}
		else if(!fieldType.equals(other.getType())){
			return false;
		}
		
		return true;
	}
}
