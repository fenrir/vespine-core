package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;

import org.fenrir.vespine.core.dto.IAlertTypeDTO;
import org.fenrir.vespine.core.dto.IIssueAlertDTO;
import org.fenrir.vespine.core.entity.IssueAlert;
import org.fenrir.vespine.spi.dto.IIssueDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
public class IssueAlertDTOAdapter implements IIssueAlertDTO 
{
	private IssueAlert issueAlert;
	
	public IssueAlertDTOAdapter(IssueAlert issueAlert)
	{
		this.issueAlert = issueAlert;
	}
	
	@Override
	public String getAlertId() 
	{
		return issueAlert.getId().toString();
	}

	@Override
	public IIssueDTO getIssue() 
	{
		return new IssueDTOAdapter(issueAlert.getIssue());
	}

	@Override
	public IAlertTypeDTO getType() 
	{
		return new AlertTypeDTOAdapter(issueAlert.getType());
	}

	@Override
	public Date getCreationDate() 
	{
		return issueAlert.getCreationDate();
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getAlertId() == null) ? 0 : getAlertId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IIssueAlertDTO other = (IIssueAlertDTO) obj;
		if (getAlertId() == null) {
			if (other.getAlertId() != null)
				return false;
		} else if (!getAlertId().equals(other.getAlertId()))
			return false;
		return true;
	}
}
