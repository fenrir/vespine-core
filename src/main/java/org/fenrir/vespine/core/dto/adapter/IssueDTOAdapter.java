package org.fenrir.vespine.core.dto.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.dto.IVespineIssueDTO;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueNote;
import org.fenrir.vespine.core.entity.IssueWorkRegistry;
import org.fenrir.vespine.core.entity.Tag;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140813
 */
public class IssueDTOAdapter implements IVespineIssueDTO
{
    private AbstractIssue issue;

    public IssueDTOAdapter(AbstractIssue issue)
    {
        this.issue = issue;
    }

    @Override
    public String getProvider()
    {
    	return VespineCoreConstants.PROVIDER_VESPINE;
    }
    
    @Override
    public String getIssueId()
    {
    	return issue.getId().toString();
    }
    
    @Override
    public String getVisibleId()
    {
    	return issue.getVisibleId();
    }
    
    @Override
    public String getSummary()
    {
        return issue.getSummary();
    }

    @Override
    public String getDescription()
    {
        return issue.getDescription();
    }
    
    @Override
    public IStatusDTO getStatus()
    {
        if(issue.getStatus()!=null){
            return new StatusDTOAdapter(issue.getStatus());
        }
        return null;
    }

    @Override
    public IProjectDTO getProject()
    {
        return new ProjectDTOAdapter(issue.getProject());
    }

    @Override
    public ISeverityDTO getSeverity()
    {
        return new SeverityDTOAdapter(issue.getSeverity());
    }
    
    @Override
    public ICategoryDTO getCategory()
    {
        return new CategoryDTOAdapter(issue.getCategory());
    }

    @Override
    public IUserDTO getAssignedUser() 
    {
    	if(issue.getAssignedUser()!=null){
    		return new UserDTOAdapter(issue.getAssignedUser());
    	}
    	
		return null;
    }

    @Override
    public Date getSendDate()
    {
        return issue.getSendDate();
    }

    @Override
    public Date getResolutionDate()
    {
        return issue.getResolutionDate();
    }

    @Override
    public Date getModifiedDate()
    {
        return issue.getModifiedDate();
    }

    @Override
    public Boolean isSla()
    {
        return issue.getSlaDate()!=null;
    }

    @Override
    public Date getSlaDate() 
    {
        return issue.getSlaDate();
    }
    
    @Override
    @JsonManagedReference
    public List<IIssueNoteDTO> getNotes() 
    {
        List<IIssueNoteDTO> issueNotes = new ArrayList<IIssueNoteDTO>();
        for(IssueNote elem:issue.getNotes()){
            issueNotes.add(new IssueNoteDTOAdapter(elem));
        }
                
        return issueNotes;
    }
            
    @Override
    public List<ITagDTO> getTags()
    {
    	List<ITagDTO> issueTags = new ArrayList<ITagDTO>();
        for(Tag elem:issue.getTags()){
            issueTags.add(new TagDTOAdapter(elem));
        }
                
        return issueTags;
    }
    
	@Override
	public IUserDTO getReporter() 
	{
		return new UserDTOAdapter(issue.getReporter());
	}
	
	@Override
	public Integer getEstimatedTime() 
	{
		return issue.getEstimatedTime();
	}
	
	@Override
	public String getEstimatedTimeFormated()
	{
		return issue.getEstimatedTimeFormated();
	}

	@Override
	public ISprintDTO getSprint() 
	{
		if(issue.getSprint()!=null){
			return new SprintDTOAdapter(issue.getSprint());
		}
		
		return null;
	}

	@Override
	@JsonManagedReference
	public List<IWorkRegistryDTO> getWorkRegistries() 
	{
		List<IWorkRegistryDTO> registries = new ArrayList<IWorkRegistryDTO>();
        for(IssueWorkRegistry elem:issue.getWorkingRegistries()){
            registries.add(new WorkRegistryDTOAdapter(elem));
        }
                
        return registries;
	}
	
	@Override
	public Integer getWorkTime()
	{
		return issue.getWorkTime();
	}
	
	@Override
	public String getWorkTimeFormated()
	{
		return issue.getWorkTimeFormated();
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return issue.getLastUpdated();
	}
	
	@Override
	public boolean isDeleted()
    {
        return issue.isDeleted();
    }
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getIssueId() == null) ? 0 : getIssueId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IIssueDTO)){
            return false;
        }
        
        final IIssueDTO other = (IIssueDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Issue ID */
        if(getIssueId()==null){
            if(other.getIssueId()!=null){
                return false;
            }
        } 
        else if(!getIssueId().equals(other.getIssueId())){
            return false;
        }      
        
        return true;
    }
}
