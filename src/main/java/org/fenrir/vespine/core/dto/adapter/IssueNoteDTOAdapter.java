package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.entity.IssueNote;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140811
 */
public class IssueNoteDTOAdapter implements IIssueNoteDTO
{
    private IssueNote noteData;
    
    public IssueNoteDTOAdapter(IssueNote noteData)
    {
        this.noteData = noteData;
    }

    public String getProvider()
    {
    	return VespineCoreConstants.PROVIDER_VESPINE;
    }
    
    @Override
    public String getNoteId() 
    {
        return noteData.getId().toString();
    }
    
    @Override
    @JsonBackReference
    public IIssueDTO getIssue()
    {
    	return new IssueDTOAdapter(noteData.getIssue());
    }

    @Override
    public String getReporter() 
    {
        return noteData.getReporter();
    }

    @Override
    public String getText() 
    {
        return noteData.getContent();
    }

    @Override
    public Date getSendDate() 
    {
        return noteData.getSendDate();
    }

    @Override
    public Date getLastEditionDate() 
    {
        return noteData.getLastEditionDate();
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getNoteId() == null) ? 0 : getNoteId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IIssueNoteDTO)){
            return false;
        }
        
        final IIssueNoteDTO other = (IIssueNoteDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Note ID */
        if(getNoteId()==null){
            if(other.getNoteId()!=null){
                return false;
            }
        } 
        else if(!getNoteId().equals(other.getNoteId())){
            return false;
        }      
        
        return true;
    }
}
