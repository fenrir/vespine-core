package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;
import org.fenrir.vespine.core.dto.IIssueWipRegistryDTO;
import org.fenrir.vespine.core.entity.IssueWipRegistry;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140523
 */
public class IssueWipRegistryDTOAdapter implements IIssueWipRegistryDTO 
{
	private IssueWipRegistry registry;
	
	public IssueWipRegistryDTOAdapter(IssueWipRegistry registry)
	{
		this.registry = registry;
	}

	@Override
	public String getRegistryId()
	{
		return registry.getId().toString();
	}
	
	@Override
	public IIssueDTO getIssue() 
	{
		return new IssueDTOAdapter(registry.getIssue());
	}
	
	@Override
	public IUserDTO getUser() 
	{
		return new UserDTOAdapter(registry.getUser());
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return registry.getLastUpdated();
	}
	
	@Override
    public int hashCode()
    {
        return getRegistryId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IIssueWipRegistryDTO)){
            return false;
        }

        IIssueWipRegistryDTO other = (IIssueWipRegistryDTO)obj;
        return getRegistryId().equals(other.getRegistryId());
    }
}
