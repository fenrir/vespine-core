package org.fenrir.vespine.core.dto.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.dto.IVespineProjectDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.ProviderProject;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140706
 */
public class ProjectDTOAdapter implements IVespineProjectDTO
{
    private IssueProject project;
    
    public ProjectDTOAdapter(IssueProject project)
    {
        this.project = project;
    }

    @Override
    public String getProvider()
    {
    	return VespineCoreConstants.PROVIDER_VESPINE;
    }
    
    @Override
    public String getProjectId() 
    {
        return project.getId().toString();
    }

    @Override
    public String getName() 
    {
        return project.getName();
    }

    @Override
    public String getAbbreviation()
    {
    	return project.getAbbreviation();
    }
    
    @Override
    @JsonBackReference
    public IProjectDTO getParentProject() 
    {
    	if(project.getParent()!=null){
    		return new ProjectDTOAdapter(project.getParent());
    	}
    	return null;
    }

    @Override
    @JsonManagedReference
    public List<IProjectDTO> getSubprojects() 
    {
    	List<IProjectDTO> subprojects = new ArrayList<IProjectDTO>();
        if(project.getChildren()!=null){
            for(IssueProject elem:project.getChildren()){
                subprojects.add(new ProjectDTOAdapter(elem));
            }
        }
        
        return subprojects;
    }
    
    @Override
	public List<IProviderElementDTO> getProviderProjects() 
	{
    	List<IProviderElementDTO> elements = new ArrayList<IProviderElementDTO>();
    	if(project.getProviderProjects()!=null){
            for(ProviderProject elem:project.getProviderProjects()){
                elements.add(new ProviderProjectDTOAdapter(elem));
            }
        }
        
        return elements;
	}

	@Override
	public IWorkflowDTO getWorkflow() 
	{
		return new WorkflowDTOAdapter(project.getWorkflow());
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return project.getLastUpdated();
	}
    
    @Override
    public String toString()
    {
        return project.toString();
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getProjectId() == null) ? 0 : getProjectId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IProjectDTO)){
            return false;
        }
        
        final IProjectDTO other = (IProjectDTO)obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Project ID */
        if(getProjectId()==null){
            if(other.getProjectId()!=null){
                return false;
            }
        } 
        else if(!getProjectId().equals(other.getProjectId())){
            return false;
        }      
        
        return true;
    }
}
