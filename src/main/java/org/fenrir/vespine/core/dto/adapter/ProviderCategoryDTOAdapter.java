package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.entity.ProviderCategory;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140111
 */
public class ProviderCategoryDTOAdapter implements IProviderElementDTO 
{
	private ProviderCategory providerCategory;
	
	public ProviderCategoryDTOAdapter(ProviderCategory providerCategory)
	{
		this.providerCategory = providerCategory;
	}
	
	@Override
	public String getType()
	{
		return VespineCoreConstants.PROVIDER_ELEMENT_CATEGORY;
	}
	
	@Override
	public String getProviderElementId() 
	{
		return providerCategory.getId().toString();
	}

	@Override
	public String getName() 
	{
		return providerCategory.getName();
	}

	@Override
	public String getProvider() 
	{
		return providerCategory.getProvider();
	}

	@Override
	public String getProviderId() 
	{
		return providerCategory.getProviderId();
	}

	@Override
	public Date getLastUpdated() 
	{
		return providerCategory.getLastUpdated();
	}
	
	@Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getProviderId() == null) ? 0 : getProviderId().hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IProviderElementDTO)){
            return false;
        }
        
        final IProviderElementDTO other = (IProviderElementDTO)obj;
        /* Type */
        if(getType()==null){
        	if(other.getType()!=null){
        		return false;
        	}
        }
        else if(!getType().equals(other.getType())){
        	return false;
        }
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Provider ID */
        if(getProviderId()==null){
            if(other.getProviderId()!=null){
                return false;
            }
        } 
        else if(!getProviderId().equals(other.getProviderId())){
            return false;
        }      
        
        return true;
    }
}
