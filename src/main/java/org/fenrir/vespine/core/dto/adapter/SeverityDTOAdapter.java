package org.fenrir.vespine.core.dto.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.dto.IVespineSeverityDTO;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.ProviderSeverity;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140111
 */
public class SeverityDTOAdapter implements IVespineSeverityDTO
{
    private IssueSeverity severity;
    
    public SeverityDTOAdapter(IssueSeverity severity)
    {
        this.severity = severity;
    }
    
    @Override
    public String getProvider()
    {
    	return VespineCoreConstants.PROVIDER_VESPINE;
    }
    
    @Override
    public String getSeverityId() 
    {
        return severity.getId().toString();
    }

    @Override
    public String getName() 
    {
        return severity.getName();
    }

    @Override
    public String getSlaTerm() 
    {
        return severity.getSlaPattern();
    }
    
    @Override
	public List<IProviderElementDTO> getProviderSeverities() 
	{
		List<IProviderElementDTO> elements = new ArrayList<IProviderElementDTO>();
    	if(severity.getProviderSeverities()!=null){
            for(ProviderSeverity elem:severity.getProviderSeverities()){
                elements.add(new ProviderSeverityDTOAdapter(elem));
            }
        }
        
        return elements;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return severity.getLastUpdated();
	}

	@Override
	public String toString()
	{
		return severity.toString();
	}
	
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getSeverityId() == null) ? 0 : getSeverityId().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ISeverityDTO)){
            return false;
        }

        ISeverityDTO other = (ISeverityDTO) obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Severity Id */
        if(getSeverityId()==null){
            if(other.getSeverityId()!=null){
                return false;
            }
        }
        else if(!getSeverityId().equals(other.getSeverityId())){
            return false;
        }

        return true;
    }
}
