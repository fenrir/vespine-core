package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.spi.dto.IProjectDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140524
 */
public class SprintDTOAdapter implements ISprintDTO 
{
	private Sprint sprint;
	
	public SprintDTOAdapter(Sprint sprint)
	{
		this.sprint = sprint;
	}
	
	@Override
	public String getSprintId() 
	{
		return sprint.getId().toString();
	}

	@Override
	public IProjectDTO getProject() 
	{
		return new ProjectDTOAdapter(sprint.getProject());
	}

	@Override
	public Date getStartDate() 
	{
		return sprint.getStartDate();
	}

	@Override
	public Date getEndDate() 
	{
		return sprint.getEndDate();
	}

	@Override
	public Date getLastUpdated() 
	{
		return sprint.getLastUpdated();
	}
	
	public String toString()
	{
		return sprint.toString();
	}
	
	@Override
    public int hashCode()
    {
        return getSprintId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ISprintDTO)){
            return false;
        }

        ISprintDTO other = (ISprintDTO)obj;
        return getSprintId().equals(other.getSprintId());
    }
}
