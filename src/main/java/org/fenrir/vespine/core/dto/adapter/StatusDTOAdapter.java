package org.fenrir.vespine.core.dto.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.ProviderStatus;
import org.fenrir.vespine.core.dto.IVespineStatusDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140110
 */
public class StatusDTOAdapter implements IVespineStatusDTO
{
    private IssueStatus status;

    public StatusDTOAdapter(IssueStatus status)
    {
        this.status = status;
    }

    @Override
    public String getProvider()
    {
    	return VespineCoreConstants.PROVIDER_VESPINE;
    }
    
    @Override
    public String getStatusId()
    {
        return status.getId().toString();
    }

    @Override
    public String getName()
    {
        return status.getName();
    }

    @Override
    public String getColorRGB()
    {
        return status.getColor();
    }
    
    @Override
	public Date getLastUpdated() 
    {
		return status.getLastUpdated();
	}

	@Override
	public List<IProviderElementDTO> getProviderStatus() 
	{
		List<IProviderElementDTO> elements = new ArrayList<IProviderElementDTO>();
    	if(status.getProviderStatus()!=null){
            for(ProviderStatus elem:status.getProviderStatus()){
                elements.add(new ProviderStatusDTOAdapter(elem));
            }
        }
        
        return elements;
	}

	@Override
	public String toString()
	{
		return status.toString();
	}
	
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getStatusId() == null) ? 0 : getStatusId().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IStatusDTO)){
            return false;
        }

        IStatusDTO other = (IStatusDTO) obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* Status Id */
        if(getStatusId()==null){
            if(other.getStatusId()!=null){
                return false;
            }
        }
        else if(!getStatusId().equals(other.getStatusId())){
            return false;
        }

        return true;
    }
}
