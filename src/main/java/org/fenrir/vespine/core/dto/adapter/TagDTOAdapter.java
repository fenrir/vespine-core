package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;
import org.fenrir.vespine.core.entity.Tag;
import org.fenrir.vespine.spi.dto.ITagDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140111
 */
public class TagDTOAdapter implements ITagDTO 
{
	private Tag tag;
	
	public TagDTOAdapter(Tag tag)
	{
		this.tag = tag;
	}

	@Override
	public String getTagId() 
	{
		return tag.getId().toString();
	}

	@Override
	public String getName() 
	{
		return tag.getName();
	}
	
	@Override
	public Date getCreationDate() 
	{
		return tag.getCreationDate();
	}

	@Override
	public Boolean isPreferred() 
	{
		return tag.isPreferred();
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return tag.getLastUpdated();
	}

	@Override
	public String toString()
	{
		return tag.toString();
	}
	
	@Override
    public int hashCode()
    {
        return getTagId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ITagDTO)){
            return false;
        }

        ITagDTO other = (ITagDTO)obj;
        return getTagId().equals(other.getTagId());
    }
}
