package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.entity.User;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140601
 */
public class UserDTOAdapter implements IUserDTO  
{
	private User user;
	
	public UserDTOAdapter(User user)
	{
		this.user = user;
	}
	
	@Override
	public String getProvider()
	{
		return VespineCoreConstants.PROVIDER_VESPINE;
	}
	
	@Override
	public String getUserId() 
	{
		return user.getId().toString();
	}

	@Override
	public String getUsername() 
	{
		return user.getUsername();
	}
	
	@Override
	public String getCompleteName() 
	{
		return user.getCompleteName();
	}

	@Override
	public Boolean isActive() 
	{
		return user.isActive();
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return user.getLastUpdated();
	}
	
	@Override
	public String toString()
	{
		return user.toString();
	}
	
	@Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProvider() == null) ? 0 : getProvider().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IUserDTO)){
            return false;
        }

        IUserDTO other = (IUserDTO) obj;
        /* Provider */
        if(getProvider()==null){
            if(other.getProvider()!=null){
                return false;
            }
        } 
        else if(!getProvider().equals(other.getProvider())){
            return false;
        }
        /* User Id */
        if(getUserId()==null){
            if(other.getUserId()!=null){
                return false;
            }
        }
        else if(!getUserId().equals(other.getUserId())){
            return false;
        }

        return true;
    }
}
