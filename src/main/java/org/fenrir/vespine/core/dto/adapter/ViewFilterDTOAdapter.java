package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.entity.IssueViewFilter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140524
 */
public class ViewFilterDTOAdapter implements IViewFilterDTO 
{
	private IssueViewFilter filter;
	
	public ViewFilterDTOAdapter(IssueViewFilter filter)
	{
		this.filter = filter;
	}
	
	@Override
	public String getFilterId() 
	{
		return filter.getId().toString();
	}

	@Override
	public String getName() 
	{
		return filter.getName();
	}

	@Override
	public String getDescription() 
	{
		return filter.getDescription();
	}

	@Override
	public String getFilterQuery() 
	{
		return filter.getFilterQuery();
	}

	@Override
	public String getOrderClause() 
	{
		return filter.getOrderByClause();
	}

	@Override
	public Boolean isApplicationFilter() 
	{
		return filter.isApplicationFilter();
	}

	@Override
	public Long getFilterOrder() 
	{
		return filter.getFilterOrder();
	}

	@Override
	public Date getCreationDate() 
	{
		return filter.getCreationDate();
	}

	@Override
	public Date getLastUpdated() 
	{
		return filter.getLastUpdated();
	}
	
	@Override
	public Boolean isTransient() 
	{
		return Boolean.FALSE;
	}
	
	@Override
	public String toString()
	{
		return filter.toString();
	}
	
	@Override
    public int hashCode()
    {
        return getFilterId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof ISprintDTO)){
            return false;
        }

        IViewFilterDTO other = (IViewFilterDTO)obj;
        return getFilterId().equals(other.getFilterId());
    }
}
