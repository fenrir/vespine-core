package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;
import org.fenrir.vespine.core.entity.IssueWorkRegistry;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140813
 */
public class WorkRegistryDTOAdapter implements IWorkRegistryDTO 
{
	private IssueWorkRegistry workRegistry;
	
	public WorkRegistryDTOAdapter(IssueWorkRegistry workRegistry)
	{
		this.workRegistry = workRegistry;
	}
	
	@Override
	public String getRegistryId() 
	{
		return workRegistry.getId().toString();
	}

	@Override
	@JsonBackReference
	public IIssueDTO getIssue() 
	{
		return new IssueDTOAdapter(workRegistry.getIssue());
	}
	
	@Override
	public String getDescription()
	{
		return workRegistry.getDescription();
	}

	@Override
	public ISprintDTO getSprint() 
	{
		if(workRegistry.getSprint()!=null){
			return new SprintDTOAdapter(workRegistry.getSprint());
		}
		
		return null;
	}

	@Override
	public IUserDTO getUser() 
	{
		return new UserDTOAdapter(workRegistry.getUser());
	}

	@Override
	public Date getWorkingDay() 
	{
		return workRegistry.getWorkingDay();
	}

	@Override
	public Integer getTime() 
	{
		return workRegistry.getTime();
	}

	@Override
	public Date getLastUpdated() 
	{
		return workRegistry.getLastUpdated();
	}
	
	@Override
    public int hashCode()
    {
        return getRegistryId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IWorkRegistryDTO)){
            return false;
        }

        IWorkRegistryDTO other = (IWorkRegistryDTO)obj;
        return getRegistryId().equals(other.getRegistryId());
    }
}
