package org.fenrir.vespine.core.dto.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;
import org.fenrir.vespine.core.entity.Workflow;
import org.fenrir.vespine.core.entity.WorkflowStep;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140110
 */
public class WorkflowDTOAdapter implements IWorkflowDTO 
{
	private Workflow workflow;
	
	public WorkflowDTOAdapter(Workflow workflow)
	{
		this.workflow = workflow;
	}
	
	@Override
	public String getWorkflowId() 
	{
		return workflow.getId().toString();
	}

	@Override
	public String getName() 
	{
		return workflow.getName();
	}

	@Override
	public IStatusDTO getInitialStatus() 
	{
		return new StatusDTOAdapter(workflow.getInitialStatus());
	}

	@Override
	@JsonManagedReference
	public List<IWorkflowStepDTO> getWorkflowSteps() 
	{
		List<IWorkflowStepDTO> steps = new ArrayList<IWorkflowStepDTO>();
        for(WorkflowStep elem:workflow.getSteps()){
            steps.add(new WorkflowStepDTOAdapter(elem));
        }
                
        return steps;
	}

	@Override
	public Date getLastUpdated() 
	{
		return workflow.getLastUpdated();
	}

	@Override
	public String toString()
	{
		return workflow.getName();
	}
	
	@Override
    public int hashCode()
    {
        return getWorkflowId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IWorkflowDTO)){
            return false;
        }

        IWorkflowDTO other = (IWorkflowDTO)obj;
        return getWorkflowId().equals(other.getWorkflowId());
    }
}
