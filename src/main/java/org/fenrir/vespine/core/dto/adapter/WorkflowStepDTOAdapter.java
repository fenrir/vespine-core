package org.fenrir.vespine.core.dto.adapter;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;
import org.fenrir.vespine.core.entity.WorkflowStep;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140110
 */
public class WorkflowStepDTOAdapter implements IWorkflowStepDTO 
{
	private WorkflowStep step;
	
	public WorkflowStepDTOAdapter(WorkflowStep step)
	{
		this.step = step;
	}
	
	@Override
	public String getWorkflowStepId() 
	{
		return step.getId().toString();
	}

	@Override
	@JsonBackReference
	public IWorkflowDTO getWorkflow() 
	{
		return new WorkflowDTOAdapter(step.getWorkflow());
	}

	@Override
	public IStatusDTO getSourceStatus() 
	{
		return new StatusDTOAdapter(step.getSourceStatus());
	}

	@Override
	public IStatusDTO getDestinationStatus() 
	{
		return new StatusDTOAdapter(step.getDestinationStatus());
	}

	@Override
	public Date getLastUpdated() 
	{
		return step.getLastUpdated();
	}
	
	@Override
    public int hashCode()
    {
        return getWorkflowStepId().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IWorkflowStepDTO)){
            return false;
        }

        IWorkflowStepDTO other = (IWorkflowStepDTO)obj;
        return getWorkflowStepId().equals(other.getWorkflowStepId());
    }
}
