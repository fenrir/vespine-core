package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.id.enhanced.TableGenerator;
import org.hibernate.annotations.Parameter;
import org.compass.annotations.Index;
import org.compass.annotations.Searchable;
import org.compass.annotations.SearchableComponent;
import org.compass.annotations.SearchableId;
import org.compass.annotations.SearchableProperty;
import org.fenrir.vespine.core.entity.annotation.ConverterParam;
import org.fenrir.vespine.core.entity.annotation.TrackedEntity;
import org.fenrir.vespine.core.entity.annotation.TrackedField;
import org.fenrir.vespine.core.entity.converter.IssueCategoryConverter;
import org.fenrir.vespine.core.entity.converter.IssueProjectConverter;
import org.fenrir.vespine.core.entity.converter.IssueSeverityConverter;
import org.fenrir.vespine.core.entity.converter.IssueStatusConverter;
import org.fenrir.vespine.core.entity.converter.SprintConverter;
import org.fenrir.vespine.core.entity.converter.UserConverter;
import org.fenrir.vespine.core.util.IAuditableElement;
import org.fenrir.vespine.core.util.ITrackEntityRules;
import org.fenrir.vespine.spi.util.DateConverter;
import org.fenrir.vespine.spi.util.IntegerConverter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140828
 */
@Entity
@GenericGenerator(name = "ISSUE_ID_GENERATOR", 
        strategy = "org.fenrir.vespine.core.entity.generator.IssueIdTableGenerator", 
        parameters = {
            @Parameter(name = TableGenerator.TABLE_PARAM, value = "SEQUENCES"),
            @Parameter(name = TableGenerator.SEGMENT_VALUE_PARAM, value = "org.fenrir.vespine.core.entity.AbstractIssue")
        }
)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@Searchable(alias="issue")
@TrackedEntity(name="ISSUE")
public abstract class AbstractIssue implements Serializable, IAuditableElement, ITrackEntityRules
{
	private static final long serialVersionUID = 6794975490047866244L;

	public static final String ALIAS = "issue";
    
    public static final String VISIBLE_ID_PATTERN = "{0}-{1}";
    
    @Id
    /* Important que el generated value segueixi l'estrategia TABLE en comptes de AUTO.
     * D'aquesta manera es crea una taula auxiliar per gestionar les sequences i així asegurar
     * valors generats únics sigui quina sigui la subclasse  
     */
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ISSUE_ID_GENERATOR")
    @SearchableId
    protected Long id;

    @SearchableProperty(index = Index.ANALYZED)
    protected String visibleId;
    
    @Version
    protected Long version;
    
    @Transient
    protected Long auditRecordReference;
    @Transient
    protected boolean instanceTracked = true;
        
    @SearchableProperty(index = Index.ANALYZED)
    @TrackedField
    protected String summary;
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(length=1000)
    @SearchableProperty(index = Index.ANALYZED)
    @TrackedField
    protected String description;

    /**
     * TODO Mirar si hi ha forma de crear un proxy per obtenir la col.lecció a fi de que sigui LAZY
     * Les col.leccions s'han de posar en mode fetch=EAGER perquè es pot donar el cas que 
     * el get es faci en un entorn no transaccional, el que produeix un error.
     */
    @ManyToMany(mappedBy="issues", fetch= FetchType.EAGER)
    protected Set<Tag> tags;
    
    @ManyToOne
    @TrackedField(converter=IssueProjectConverter.class)
    protected IssueProject project;
    
    @ManyToOne
    @TrackedField(converter=IssueStatusConverter.class)
    protected IssueStatus status;

    @ManyToOne
    @TrackedField(converter=IssueSeverityConverter.class)
    protected IssueSeverity severity;

    @ManyToOne
    @TrackedField(converter=IssueCategoryConverter.class)
    protected IssueCategory category;

    /**
     * Usuari informador de la incidencia
     */
    @ManyToOne
    @SearchableComponent
    @TrackedField(converter=UserConverter.class)
    protected User reporter;
    
    /**
     * Usuari a qui està assignada la incidencia en el moment actual
     */
    @ManyToOne
    @SearchableComponent
    @TrackedField(converter=UserConverter.class)
    protected User assignedUser;      
    
    /**
     * TODO Mirar si hi ha forma de crear un proxy per obtenir la col.lecció a fi de que sigui LAZY
     * Les col.leccions s'han de posar en mode fetch=EAGER perquè es pot donar el cas que 
     * el get es faci en un entorn no transaccional, el que produeix un error.
     */
    @OneToMany(mappedBy="issue", fetch= FetchType.EAGER, cascade= CascadeType.REMOVE, orphanRemoval=true)
    @SearchableComponent
    protected Set<IssueNote> notes;
    
    /**
     * Data d'enviament inicial de la incidència. Correspon amb la data inicial del
     * periode SLA
     */
    @Temporal(TemporalType.TIMESTAMP)
    @TrackedField(converter=DateConverter.class, 
		parameters={
    		@ConverterParam(name=DateConverter.PARAM_NAME_PATTERN, value="dd/MM/yyyy HH:mm:ss")
		}
    )
    protected Date sendDate;
    
    /**
     * Data de compliment del periode SLA
     */
    @Temporal(TemporalType.DATE)
    @TrackedField(converter=DateConverter.class)
    protected Date slaDate;

    /**
     * Indica si la data del periode SLA ha sigut modificada manualment
     */
    @Column(columnDefinition="Boolean default false")
    protected Boolean slaDateModified;
    
    @Temporal(TemporalType.TIMESTAMP)
    @TrackedField(converter=DateConverter.class, 
		parameters={
			@ConverterParam(name=DateConverter.PARAM_NAME_PATTERN, value="dd/MM/yyyy HH:mm:ss")
		}
	)
    protected Date resolutionDate;
    
    @ManyToOne
    @TrackedField(converter=SprintConverter.class)
    protected Sprint sprint;
    
    /**
     * Temps estimat de resolució d'incidencia en minuts
     */
    @TrackedField(converter=IntegerConverter.class)
    protected Integer estimatedTime;
    
    /**
     * TODO Mirar si hi ha forma de crear un proxy per obtenir la col.lecció a fi de que sigui LAZY
     * Les col.leccions s'han de posar en mode fetch=EAGER perquè es pot donar el cas que 
     * el get es faci en un entorn no transaccional, el que produeix un error.
     */
    @OneToMany(mappedBy="issue", fetch= FetchType.EAGER, cascade= CascadeType.REMOVE, orphanRemoval=true)
    protected Set<IssueWorkRegistry> workRegistries;
    
    /** 
     * Data de l'última modificació de l'incidència
     */
    @Temporal(TemporalType.TIMESTAMP)
    @TrackedField(converter=DateConverter.class, 
		parameters={
			@ConverterParam(name=DateConverter.PARAM_NAME_PATTERN, value="dd/MM/yyyy HH:mm:ss")
		}
	)
    protected Date modifiedDate;
    
    /**
     * Data de l'última actualització del registre a BDD
     */
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastUpdated;
    
    /**
     * Discriminant per tractar l'herencia de les diferents classes que extenen d'aquesta classe
     */
    protected Integer type;
    
    public AbstractIssue()
    {
        
    }
    
    /**
     * S'ignoren les col.leccions d'entitats relacionades MatyToOne i OneToMany (tags i notes)
     * per tal d'evitar errors a les operacions d'esborrat i restaurat
     * @param issue 
     */
    public AbstractIssue(AbstractIssue issue)
    {
        this.id = issue.id;
        this.visibleId = issue.visibleId;
        this.summary = issue.summary;
        this.description = issue.description;
        this.project = issue.project;
        this.status = issue.status;
        this.severity = issue.severity;
        this.category = issue.category;
        this.reporter = issue.reporter;
        this.assignedUser = issue.assignedUser;
        this.sendDate = issue.sendDate;
        this.slaDate = issue.slaDate;
        this.slaDateModified = issue.slaDateModified;
        this.resolutionDate = issue.resolutionDate;
        this.sprint = issue.sprint;
        this.estimatedTime = issue.estimatedTime;
        this.modifiedDate = issue.modifiedDate;                
        this.type = issue.type;            
    }
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
    @Override
    public Long getAuditRecordReference()
    {
    	return auditRecordReference;
    }
    
    public void setAuditRecordReference(Long auditRecordReference)
    {
    	this.auditRecordReference = auditRecordReference;
    }
    
    @Override
    public boolean isInstanceTracked()
    {
    	return instanceTracked;
    }
    
    public void setInstanceTracked(boolean instanceTracked)
    {
    	this.instanceTracked = instanceTracked;
    }
    
    public String getVisibleId()
    {
    	return visibleId;
    }
    
    public void setVisibleId(String visibleId)
    {
    	this.visibleId = visibleId;
    }
    
    public String getSummary()
    {
        return summary;
    }
    
    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getDescription()
    {
        return this.description;
    }
    
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public Set<Tag> getTags()
    {
        if(tags==null){
            return new HashSet<Tag>();
        }
        return tags;
    }
    
    public void setTags(Set<Tag> tags)
    {
        this.tags = tags;
    }
    
    public IssueProject getProject()
    {
        return project;
    }
    
    public void setProject(IssueProject project)
    {
        this.project = project;
    }
    
    public IssueStatus getStatus()
    {
        return status;
    }
    
    public void setStatus(IssueStatus status)
    {
        this.status = status;
    }

    public IssueSeverity getSeverity()
    {
        return severity;
    }

    public void setSeverity(IssueSeverity severity)
    {
        this.severity = severity;
    }

    public IssueCategory getCategory()
    {
        return category;
    }

    public void setCategory(IssueCategory category)
    {
        this.category = category;
    }

    public User getReporter()
    {
    	return reporter;
    }
    
    public void setReporter(User reporter)
    {
    	this.reporter = reporter;
    }
    
    public User getAssignedUser() 
    {
        return assignedUser;
    }

    public void setAssignedUser(User assignedUser) 
    {
        this.assignedUser = assignedUser;
    }
    
    public Set<IssueNote> getNotes()
    {
        if(notes==null){
            notes = new HashSet<IssueNote>();
        }
        return notes;
    }
    
    public void setNotes(Set<IssueNote> notes)
    {
        this.notes = notes;
    }
    
    public abstract boolean isDeleted();

    public Date getSendDate()
    {
        return sendDate;
    }
    
    public void setSendDate(Date sendDate)
    {
        this.sendDate = sendDate;
    }

    public Boolean isSlaDateModified()
    {
        return slaDateModified;
    }

    public void setSlaDateModified(Boolean slaDateModified)
    {
        this.slaDateModified = slaDateModified;
    }
    
    public Date getSlaDate()
    {
        return slaDate;
    }
    
    public void setSlaDate(Date slaDate)
    {
        this.slaDate = slaDate;
    }
    
    public boolean isSLA()
    {
        return getSlaDate()!=null;
    }
    
    public Date getResolutionDate()
    {
        return resolutionDate;
    }
    
    public void setResolutionDate(Date resolutionDate)
    {
        this.resolutionDate = resolutionDate;
    }
    
    public Sprint getSprint()
    {
    	return sprint;
    }
    
    public void setSprint(Sprint sprint)
    {
    	this.sprint = sprint;
    }
    
    /**
     * Mètode per accedir a l'especificació del temps de resolució de l'incidencia
     * @return {@link Integer} - Temps estimat de resolució de l'incidencia (En minuts)
     */
    public Integer getEstimatedTime()
    {
    	return estimatedTime;
    }
    
    /**
     * Mètode d'obtenció del temps estimat de resolució de l'incidencia en una cadena de text formatada
     * @return {@link String} - Temps estimat en format ##h ##m
     */
    public String getEstimatedTimeFormated()
    {
    	if(estimatedTime!=null){
    		int hours = estimatedTime / 60;
    		int minutes = estimatedTime % 60;
    		return MessageFormat.format("{0, number, integer}h {1, number, integer}m", new Object[]{hours, minutes});
    	}
    	
    	return null;
    }
    
    /**
     * Mètode que especifica el temps estimat de resolució de l'incidencia. 
     * @param estimatedTime {@link Integer} - Temps estimat de resolució de l'incidencia (En minuts)
     */
    public void setEstimatedTime(Integer estimatedTime)
    {
    	this.estimatedTime = estimatedTime;
    }
    
    public Set<IssueWorkRegistry> getWorkingRegistries()
    {
    	if(workRegistries==null){
    		return new HashSet<IssueWorkRegistry>();
    	}
    	return workRegistries;
    }
    
    public void setWorkingRegistries(Set<IssueWorkRegistry> workingRegistries)
    {
    	this.workRegistries = workingRegistries;
    }
    
    /**
     * Mètode d'obtenció de temps treballat a l'incidencia. S'obté de sumar els minuts de tots els registres de
     * treball relatius a l'incidencia
     * @return {@link Integer} - Nombre de minuts totals treballats a l'incidencia
     */
    public Integer getWorkTime()
    {
    	int minutes = 0;
    	if(workRegistries!=null){
	    	for(IssueWorkRegistry elem:workRegistries){
	    		minutes += elem.getTime();
	    	}
    	}
    	
    	return minutes;
    }
    
    public String getWorkTimeFormated()
    {
    	int total = getWorkTime();
    	int hours = total / 60;
    	int minutes = total % 60;
    	return MessageFormat.format("{0, number, integer}h {1, number, integer}m", new Object[]{hours, minutes});
    }
    
    public Date getModifiedDate()
    {
        return modifiedDate;
    }
    
    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }
    
    public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }
    
    public Integer getType()
    {
        return type;
    }
    
    public void setType(Integer type)
    {
        this.type = type;
    }    
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();     
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        AbstractIssue other = (AbstractIssue) obj;
        /* Id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        } 
        else if(!id.equals(other.id)){
            return false;
        }
        
        return true;
    }
}
