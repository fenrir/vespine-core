package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140416
 */
@TableGenerator(
	    name="AUDIT_REGISTRY_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.AuditRegistry", 
	    allocationSize=1)
@Entity
public class AuditRegistry implements Serializable
{
	private static final long serialVersionUID = 1324275433227428050L;

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE, generator="AUDIT_REGISTRY_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;
    
    private String action;
    
    private String objectId;
    
    private String resultCode;
    
    private String resultCause;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date auditDate;
    
    @ManyToOne
    @JoinColumn(name="USER_ID")
    private User user;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
    public String getAction()
    {
    	return action;
    }
    
    public void setAction(String action)
    {
    	this.action = action;
    }
    
    public String getObjectId()
    {
    	return objectId;
    }
    
    public void setObjectId(String objectId)
    {
    	this.objectId = objectId;
    }
    
    public String getResultCode()
    {
    	return resultCode;
    }
    
    public void setResultCode(String resultCode)
    {
    	this.resultCode = resultCode;
    }
    
    public String getResultCause()
    {
    	return resultCause;
    }
    
    public void setResultCause(String resultCause)
    {
    	this.resultCause = resultCause;
    }
    
    public Date getAuditDate() 
	{
		return auditDate;
	}

	public void setAuditDate(Date auditDate) 
	{
		this.auditDate = auditDate;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public void setUser(User user)
	{
		this.user = user;
	}
    
    public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date(); 
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        AuditRegistry other = (AuditRegistry)obj;
        /* Id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        } 
        else if(!id.equals(other.id)){
            return false;
        }
        
        return true;
    }
}
