package org.fenrir.vespine.core.entity;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.compass.annotations.Searchable;
import org.compass.annotations.SearchableConstant;
import org.compass.annotations.Index;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
@Searchable
@SearchableConstant(name="issueClass", values={"DeletedIssue"}, index=Index.ANALYZED)
@Entity
public class DeletedIssue extends AbstractIssue 
{    
	private static final long serialVersionUID = -5808211145572966988L;
	
	public static final int INHERITANCE_DISCRIMINATOR = 2;
    
    public DeletedIssue()
    {
        super();
    }
    
    public DeletedIssue(AbstractIssue issue)
    {
        super(issue);
    }
    
    @Override
    public boolean isDeleted()
    {
        return true;
    }
    
    @PreUpdate
    @PrePersist
    @Override
    public void updateDefaultValues()
    {
        super.updateDefaultValues();
        if(type==null || type.intValue()!=INHERITANCE_DISCRIMINATOR){
            type = new Integer(INHERITANCE_DISCRIMINATOR);
        }
    }
}
