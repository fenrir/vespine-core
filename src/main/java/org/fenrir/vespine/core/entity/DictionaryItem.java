package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140402
 */
@TableGenerator(
	    name="DICTIONARY_ITEM_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.DictionaryItem", 
	    allocationSize=1)
@Entity
@Table(uniqueConstraints=@UniqueConstraint(columnNames = {"dictionary", "value"}))
public class DictionaryItem implements Serializable
{
	private static final long serialVersionUID = 8146552380258762375L;

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE, generator="DICTIONARY_ITEM_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;

    @ManyToOne
    private Dictionary dictionary;
    
    private String value;
    
    private String description;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
    public Dictionary getDictionary()
    {
    	return dictionary;
    }
    
    public void setDictionary(Dictionary dictionary)
    {
    	this.dictionary = dictionary;
    }

    public String getValue()
    {
    	return value;
    }
    
    public void setValue(String value)
    {
    	this.value = value;
    }
    
    public String getDescription()
    {
    	return description;
    }
    
    public void setDescription(String description)
    {
    	this.description = description;
    }

    public Date getLastUpdated()
    {
    	return lastUpdated;
    }
    
    public void setLastUpdated(Date lastUpdated)
    {
    	this.lastUpdated = lastUpdated;
    }
    
    @Override
    public String toString()
    {
    	return "[" + value + "] " + description;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();        
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dictionary == null) ? 0 : dictionary.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode()); 
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        DictionaryItem other = (DictionaryItem)obj;
        /* Dictionary */
        if(dictionary==null){
            if(other.dictionary!=null){
                return false;
            }
        } 
        else if(!dictionary.equals(other.dictionary)){
            return false;
        }
        /* Value */
        if(value==null){
            if(other.value!=null){
                return false;
            }
        } 
        else if(!value.equals(other.value)){
            return false;
        }
        
        return true;
    }
}
