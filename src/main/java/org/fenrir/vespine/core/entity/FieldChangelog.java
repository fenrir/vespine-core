package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.annotations.Type;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140418
 */
@TableGenerator(
	    name="FIELD_CHANGELOG_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.FieldChangelog", 
	    allocationSize=1)
@Entity
public class FieldChangelog implements Serializable
{
	private static final long serialVersionUID = 1126795995537014676L;

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE, generator="FIELD_CHANGELOG_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;
        
    @ManyToOne
    private AuditRegistry auditRegistry;
    
    private String entity;
    private Long entityId;
    private String field;
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String value;
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
    public AuditRegistry getAuditRegistry()
    {
    	return auditRegistry;
    }
    
    public void setAuditRegistry(AuditRegistry auditRegistry)
    {
    	this.auditRegistry = auditRegistry;
    }
    
    public String getEntity() 
    {
		return entity;
	}

	public void setEntity(String entity) 
	{
		this.entity = entity;
	}

	public Long getEntityId()
	{
		return entityId;
	}
	
	public void setEntityId(Long entityId)
	{
		this.entityId = entityId;
	}
	
	public String getField() 
	{
		return field;
	}

	public void setField(String field) 
	{
		this.field = field;
	}

	public String getValue() 
	{
		return value;
	}

	public void setValue(String value) 
	{
		this.value = value;
	}

	public Date getStartDate() 
	{
		return startDate;
	}

	public void setStartDate(Date startDate) 
	{
		this.startDate = startDate;
	}

	public Date getEndDate() 
	{
		return endDate;
	}

	public void setEndDate(Date endDate) 
	{
		this.endDate = endDate;
	}

	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}

	/**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date(); 
        if(startDate==null){
        	startDate = new Date();
        }
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((entity == null) ? 0 : entity.hashCode());
        result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
        result = prime * result + ((field == null) ? 0 : field.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        FieldChangelog other = (FieldChangelog)obj;
        /* Id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        } 
        else if(!id.equals(other.id)){
            return false;
        }
        /* Entity */
        if(entity==null){
            if(other.entity!=null){
                return false;
            }
        } 
        else if(!entity.equals(other.entity)){
            return false;
        }
        /* EntityId */
        if(entityId==null){
            if(other.entityId!=null){
                return false;
            }
        } 
        else if(!entityId.equals(other.entityId)){
            return false;
        }
        /* Field */
        if(field==null){
            if(other.field!=null){
                return false;
            }
        } 
        else if(!field.equals(other.field)){
            return false;
        }
        /* Start date */
        if(startDate==null){
            if(other.startDate!=null){
                return false;
            }
        } 
        else if(!startDate.equals(other.startDate)){
            return false;
        }
        /* End date */
        if(endDate==null){
            if(other.endDate!=null){
                return false;
            }
        } 
        else if(!endDate.equals(other.endDate)){
            return false;
        }
        
        return true;
    }
}
