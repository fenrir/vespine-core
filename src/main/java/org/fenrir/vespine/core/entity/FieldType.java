package org.fenrir.vespine.core.entity;

import java.util.HashMap;
import java.util.Map;
import org.fenrir.vespine.core.entity.converter.DictionaryItemConverter;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.spi.util.BooleanConverter;
import org.fenrir.vespine.spi.util.DateConverter;
import org.fenrir.vespine.spi.util.IAttributeConverter;
import org.fenrir.vespine.spi.util.IntegerConverter;
import org.fenrir.vespine.spi.util.StringConverter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140325
 */
public enum FieldType implements IFieldType
{
	STRING("Cadena", TYPE_STRING, StringConverter.class),
	NUMERIC("Numèric", TYPE_NUMERIC, IntegerConverter.class),	
	BOOLEAN("Booleà", TYPE_BOOLEAN, BooleanConverter.class),
	DATE("Data", TYPE_DATE, DateConverter.class),
	TIMESTAMP("Data amb hora", TYPE_TIMESTAMP, DateConverter.class, DateConverter.PARAM_NAME_PATTERN, DateConverter.TIMESTAMP_FORMAT),
	DICTIONARY("Diccionari", TYPE_DICTIONARY, DictionaryItemConverter.class);

	private String name;
	private String type;
	private Class<? extends IAttributeConverter<?, ?>> converter;
	private HashMap<String, String> parameters = new HashMap<String,String>();
	
	private FieldType(String name, String type, Class<? extends IAttributeConverter<?, ?>> converter, String... params)
	{
		this.name = name;
		this.converter = converter;
		this.type = type;
		for(int i=0; i<params.length-1; i++){
			String paramName = params[i];
			String paramValue = params[i+1];
			parameters.put(paramName, paramValue);
		}
	}
	
	public String getName()
	{
		return name;
	}
	
	@Override
	public String getType()
	{
		return type;
	}
	
	@Override
	public Class<? extends IAttributeConverter<?, ?>> getConverter()
	{
		return converter;
	}
	
	@Override
	public Map<String, String> getConverterParameters()
	{
		return parameters;
	}
	
	@Override
	public boolean needDataProviderId()
	{
		return this.equals(DICTIONARY);
	}
	
	@Override
	public String toString()
	{
		return name;
	}
}
