package org.fenrir.vespine.core.entity;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IProviderDataDefinition 
{
	public String getProvider();
	public String getProviderId();
	public String getName();
}
