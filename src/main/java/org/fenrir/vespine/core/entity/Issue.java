package org.fenrir.vespine.core.entity;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.compass.annotations.Searchable;
import org.compass.annotations.SearchableConstant;
import org.compass.annotations.Index;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
@Searchable
@SearchableConstant(name="issueClass", values={"Issue"}, index=Index.ANALYZED)
@Entity
public class Issue extends AbstractIssue 
{    
	private static final long serialVersionUID = 1693520960750144016L;
	
	public static final int INHERITANCE_DISCRIMINATOR = 1;
    
    public Issue()
    {
        super();
    }
    
    public Issue(AbstractIssue issue)
    {
        super(issue);
    }
    
    @Override
    public boolean isDeleted()
    {
        return false;
    }
    
    @PreUpdate
    @PrePersist
    @Override
    public void updateDefaultValues()
    {
        super.updateDefaultValues();
        if(type==null || type.intValue()!=INHERITANCE_DISCRIMINATOR){
            type = new Integer(INHERITANCE_DISCRIMINATOR);
        }
    }
}
