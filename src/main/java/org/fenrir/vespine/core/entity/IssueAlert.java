package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140321
 */
@TableGenerator(
	    name="ISSUE_ALERT_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.IssueAlert", 
	    allocationSize=1)
@Entity
public class IssueAlert implements Serializable
{
	private static final long serialVersionUID = 6866053690747330164L;

	@Id
    @GeneratedValue(strategy= GenerationType.TABLE, generator="ISSUE_ALERT_ID_GENERATOR")
    private Long id;

    @Version
    private Long version;

    @ManyToOne
    private AbstractIssue issue;
    
    @ManyToOne
    private AlertType type;

    /* Data de creació de l'alerta
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getVersion()
    {
        return version;
    }

    public void setVersion(Long version)
    {
        this.version = version;
    }

    public AbstractIssue getIssue()
    {
        return issue;
    }

    public void setIssue(AbstractIssue issue)
    {
        this.issue = issue;
    }
    
    public AlertType getType()
    {
        return type;
    }
    
    public void setType(AlertType type)
    {
        this.type = type;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        if(creationDate==null){
            creationDate = new Date();        
        }
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((issue == null) ? 0 : issue.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }

        IssueAlert other = (IssueAlert) obj;
        /* Id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        }
        else if(!id.equals(other.id)){
            return false;
        }
        /* Type */
        if(type==null){
            if(other.type!=null){
                return false;
            }
        }
        else if(!type.equals(other.type)){
            return false;
        }
        /* Issue */
        if(issue==null){
            if(other.issue!=null){
                return false;
            }
        }
        else if(!issue.equals(other.issue)){
            return false;
        }

        return true;
    }
}
