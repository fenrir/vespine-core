package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140222
 */
@TableGenerator(
	    name="ISSUE_CUSTOM_FIELD_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.IssueCustomField", 
	    allocationSize=1)
@Entity
public class IssueCustomField implements Serializable
{
	private static final long serialVersionUID = 3112511390742786752L;

	@Id
    @GeneratedValue(strategy= GenerationType.TABLE, generator="ISSUE_CUSTOM_FIELD_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;
    
    @Enumerated(EnumType.ORDINAL)
    private FieldType fieldType;
    
    /* Només s'utilitza en el cas dels camps que requereixin proveidors de dades */
    private String dataProviderId;
    
    private String name;
    
    /* Si s'indica ManyToMany FetchType.EAGER es produeix un error perquè s'intenten carregar
     * inicialment varies col.leccions. Es pot utilizar LazyCollection en el seu lloc per que funcioni
     */
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(name = "CUSTOM_FIELD_PROJECTS",
        joinColumns = @JoinColumn(name="FIELD_ID", referencedColumnName="ID"),
        inverseJoinColumns = @JoinColumn(name="PROJECT_ID", referencedColumnName="ID")
    )
    private Collection<IssueProject> projects;
    
    @Column(columnDefinition="Boolean default false")
    private Boolean mandatory;
    
    /* Data de l'última actualització del registre a BDD
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }

    public FieldType getFieldType()
    {
        return fieldType;
    }
    
    public void setFieldType(FieldType fieldType)
    {
        this.fieldType = fieldType;
    }
    
    /**
     * Mètode d'obtenció de l'ID del proveidor de dades corresponent al camp. 
     * Només si el camp requereix d'un proveidor de dades
     * @see FieldType
     * @return String - Id del proveidor de dades associat al camp si aquest el requereix.
     * 			null en cas contrari.
     */
    public String getDataProviderId()
    {
    	return dataProviderId;
    }

    /**
     * Mètode d'especificació de l'ID del proveidor de dades corresponent al camp. 
     * Només si el camp requereix d'un proveidor de dades
     * @see FieldType
     * @param dataProviderId String - Id del proveidor de dades associat al camp si aquest el requereix.
     * 			En cas contrari, aquest atribut serà ignorat. 
     */
    public void setDataProviderId(String dataProviderId)
    {
    	this.dataProviderId = dataProviderId;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public Collection<IssueProject> getProjects()
    {
        return projects;
    }
    
    public void setProjects(Collection<IssueProject> projects)
    {
        this.projects = projects;
    }
    
    public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public Boolean isMandatory()
    {
    	return mandatory;
    }
    
    public void setMandatory(Boolean mandatory)
    {
    	this.mandatory = mandatory;
    }
    
    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();        
    }
    
    @Override
    public String toString()
    {
    	return name;
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        IssueCustomField other = (IssueCustomField) obj;
        /* Id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        } 
        else if(!id.equals(other.id)){
            return false;
        }
        
        return true;
    }
}
