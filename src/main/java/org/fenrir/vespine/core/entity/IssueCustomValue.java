package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.fenrir.vespine.core.entity.annotation.TrackedEntity;
import org.fenrir.vespine.core.entity.annotation.TrackedField;
import org.fenrir.vespine.core.util.IAuditableElement;
import org.fenrir.vespine.core.util.ITrackEntityRules;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140830
 */
@TableGenerator(
	    name="ISSUE_CUSTOM_VALUE_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.IssueCustomValue", 
	    allocationSize=1)
@Entity
@TrackedEntity(name="ISSUE_CUSTOM_VALUE")
public class IssueCustomValue implements Serializable, IAuditableElement, ITrackEntityRules
{
	private static final long serialVersionUID = 1619228061819055605L;

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE, generator="ISSUE_CUSTOM_VALUE_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;
    
    @Transient
    protected Long auditRecordReference;
    @Transient
    protected boolean instanceTracked = true;
    
    @ManyToOne
    private IssueCustomField field;
    
    @ManyToOne
    private AbstractIssue issue;
    
    @TrackedField
    private String value;
    
    /* Data de l'última actualització del registre a BDD
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
    @Override
    public Long getAuditRecordReference()
    {
    	return auditRecordReference;
    }
    
    public void setAuditRecordReference(Long auditRecordReference)
    {
    	this.auditRecordReference = auditRecordReference;
    }
    
    @Override
    public boolean isInstanceTracked()
    {
    	return instanceTracked;
    }
    
    public void setInstanceTracked(boolean instanceTracked)
    {
    	this.instanceTracked = instanceTracked;
    }

    public IssueCustomField getField()
    {
    	return field;
    }
    
    public void setField(IssueCustomField field)
    {
    	this.field = field;
    }
    
    public AbstractIssue getIssue()
    {
    	return issue;
    }
    
    public void setIssue(AbstractIssue issue)
    {
    	this.issue = issue;
    }
    
    public String getValue()
    {
    	return value;
    }
    
    public void setValue(String value)
    {
    	this.value = value;
    }
    
    public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();        
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        IssueCustomValue other = (IssueCustomValue) obj;
        /* Id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        } 
        else if(!id.equals(other.id)){
            return false;
        }
        
        return true;
    }
}
