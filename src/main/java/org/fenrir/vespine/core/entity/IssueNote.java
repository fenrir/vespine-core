package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.compass.annotations.Index;
import org.compass.annotations.Searchable;
import org.compass.annotations.SearchableProperty;
import org.hibernate.annotations.Type;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140321
 */
@TableGenerator(
	    name="ISSUE_NOTE_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.IssueNote", 
	    allocationSize=1)
@Entity
@Searchable(root=false)
public class IssueNote implements Serializable
{
	private static final long serialVersionUID = -4568435719192888064L;

	@Id
    @GeneratedValue(strategy= GenerationType.TABLE, generator="ISSUE_NOTE_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;
    
    @ManyToOne
    private AbstractIssue issue;
    
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(length=1000)
    @SearchableProperty(index = Index.ANALYZED)
    private String content;
    
    private String reporter;
    
    /**
     * Data d'enregistrament de la nota.
     */
    @Temporal(TemporalType.TIMESTAMP)
    protected Date sendDate;
    
    /**
     * Data de l'última edició de la nota
     */
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastEditionDate;
    
    /**
     * Data de l'última actualització del registre a BDD
     */
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }

    public AbstractIssue getIssue()
    {
        return issue;
    }
    
    public void setIssue(AbstractIssue issue)
    {
        this.issue = issue;
    }
    
    public String getContent()
    {
        return content;
    }
    
    public void setContent(String content)
    {
        this.content = content;
    }
    
    public String getReporter()
    {
        return reporter;
    }
    
    public void setReporter(String reporter)
    {
        this.reporter = reporter;
    }
    
    public Date getSendDate()
    {
        return sendDate;
    }
    
    public void setSendDate(Date sendDate)
    {
        this.sendDate = sendDate;
    }
    
    public Date getLastEditionDate()
    {
        return lastEditionDate;
    }
    
    public void setLastEditionDate(Date lastEditionDate)
    {
        this.lastEditionDate = lastEditionDate;
    }
    
    public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();        
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        IssueNote other = (IssueNote) obj;
        /* id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        } 
        else if(!id.equals(other.id)){
            return false;
        }
        
        return true;
    }
}
