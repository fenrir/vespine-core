package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140324
 */
@TableGenerator(
	    name="ISSUE_PROJECT_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.IssueProject", 
	    allocationSize=1)
@Entity
public class IssueProject implements Serializable
{
	public static final String PROJECT_ABBREVIATION_SEQUENCE_PATTERN = "org.fenrir.vespine.core.entity.IssueProject@{0}";
	
	private static final long serialVersionUID = 869106345445523478L;

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE, generator="ISSUE_PROJECT_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;

    private String name;    
    
    private String abbreviation;

    @ManyToOne
    private IssueProject parent;
    
    /**
     * TODO Mirar si hi ha forma de crear un proxy per obtenir la col.lecció a fi de que sigui LAZY
     * Les col.leccions s'han de posar en mode fetch=EAGER perquè es pot donar el cas que 
     * el get es faci en un entorn no transaccional, el que produeix un error.
     * S'utilitza l'anotació LazyCollection per evitar l'error "Cannot fetch multiple bags" 
     * si s'indica el FetchType a l'anotació OneToMany
     */
    @OneToMany(mappedBy="parent")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<IssueProject> children;
    
    /**
     * TODO Mirar si hi ha forma de crear un proxy per obtenir la col.lecció a fi de que sigui LAZY
     * Les col.leccions s'han de posar en mode fetch=EAGER perquè es pot donar el cas que 
     * el get es faci en un entorn no transaccional, el que produeix un error.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "PROVIDER_PROJECT_MAPPING",
	    joinColumns = @JoinColumn(name="PROJECT_ID", referencedColumnName="ID"),
	    inverseJoinColumns = @JoinColumn(name="PROVIDER_DATA_ID", referencedColumnName="ID")
	)
    private Set<ProviderProject> providerProjects;
    
    @ManyToOne
    private Workflow workflow;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }

    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getAbbreviation()
    {
    	return abbreviation;
    }
    
    public void setAbbreviation(String abbreviation)
    {
    	this.abbreviation = abbreviation;
    }
    
    public IssueProject getParent()
    {
        return parent;
    }
    
    public void setParent(IssueProject parent)
    {
        this.parent = parent;
    }
    
    public List<IssueProject> getChildren()
    {
        return children;
    }
    
    public void setChildren(List<IssueProject> children)
    {
        this.children = children;
    }
    
    public Set<ProviderProject> getProviderProjects()
    {
    	return providerProjects;
    }
    
    public void setProviderProjects(Set<ProviderProject> providerProjects)
    {
    	this.providerProjects = providerProjects;
    }
    
    public Workflow getWorkflow()
    {
    	return workflow;
    }
    
    public void setWorkflow(Workflow workflow)
    {
    	this.workflow = workflow;
    }
    
    public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }
    
    @Override
    public String toString()
    {
    	return name;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();        
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        IssueProject other = (IssueProject) obj;
        /* Id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        } 
        else if(!id.equals(other.id)){
            return false;
        }
        
        return true;
    }
}
