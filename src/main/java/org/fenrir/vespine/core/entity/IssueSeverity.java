package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131206
 */
@TableGenerator(
	    name="ISSUE_SEVERITY_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.IssueSeverity", 
	    allocationSize=1)
@Entity
public class IssueSeverity implements Serializable
{
	private static final long serialVersionUID = 8204484603664247677L;

	@Id
    @GeneratedValue(strategy= GenerationType.TABLE, generator="ISSUE_SEVERITY_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;

    private String name;
    private String slaPattern;
        
    /**
     * TODO Mirar si hi ha forma de crear un proxy per obtenir la col.lecció a fi de que sigui LAZY
     * Les col.leccions s'han de posar en mode fetch=EAGER perquè es pot donar el cas que 
     * el get es faci en un entorn no transaccional, el que produeix un error.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "PROVIDER_SEVERITY_MAPPING",
	    joinColumns = @JoinColumn(name="SEVERITY_ID", referencedColumnName="ID"),
	    inverseJoinColumns = @JoinColumn(name="PROVIDER_DATA_ID", referencedColumnName="ID")
	)
    private Set<ProviderSeverity> providerSeverities;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }

    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }

    public String getSlaPattern()
    {
        return slaPattern;
    }
    
    public void setSlaPattern(String slaPattern)
    {
        this.slaPattern = slaPattern;
    }
    
    public Set<ProviderSeverity> getProviderSeverities()
    {
    	return providerSeverities;
    }
    
    public void setProviderSeverities(Set<ProviderSeverity> providerSeverities)
    {
    	this.providerSeverities = providerSeverities;
    }
    
    public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }
    
    @Override
    public String toString()
    {
    	return name;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();        
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        IssueSeverity other = (IssueSeverity) obj;
        /* Id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        } 
        else if(!id.equals(other.id)){
            return false;
        }
        
        return true;
    }
}
