package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.annotations.Type;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140321
 */
@TableGenerator(
	    name="ISSUE_VIEW_FILTER_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.IssueViewFilter", 
	    allocationSize=1)
@Entity
public class IssueViewFilter implements Serializable
{
	private static final long serialVersionUID = 8464713343570508774L;

	@Id
    @GeneratedValue(strategy= GenerationType.TABLE, generator="ISSUE_VIEW_FILTER_ID_GENERATOR")
    private Long id;

    @Version
    private Long version;

    private String name;
    private String description;
    @Lob
    @Column(length=1000)
    @Type(type = "org.hibernate.type.TextType")
    private String filterQuery;
    @Lob
    @Column(length=1000)
    @Type(type = "org.hibernate.type.TextType")
    private String orderByClause;
    
    @Column(columnDefinition="Boolean default false")
    private Boolean applicationFilter;
    
    private Long filterOrder;

    /* Data de creació del filtre
     */
    @Temporal(TemporalType.DATE)
    private Date creationDate;

    /* Data de l'última actualització del registre a BDD
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getVersion()
    {
        return version;
    }

    public void setVersion(Long version)
    {
        this.version = version;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getFilterQuery()
    {
        return filterQuery;
    }

    public void setFilterQuery(String filterQuery)
    {
        this.filterQuery = filterQuery;
    }

    public String getOrderByClause()
    {
        return orderByClause;
    }

    public void setOrderByClause(String orderByClause)
    {
        this.orderByClause = orderByClause;
    }

    public Boolean isApplicationFilter()
    {
        return applicationFilter;
    }
    
    public void setApplicationFilter(Boolean applicationFilter)
    {
        this.applicationFilter = applicationFilter;
    }
    
    public Long getFilterOrder()
    {
        return filterOrder;
    }
    
    public void setFilterOrder(Long filterOrder)
    {
        this.filterOrder = filterOrder;
    }
    
    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public Date getLastUpdated()
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated)
    {
        this.lastUpdated = lastUpdated;
    }

    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre.
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();
        if(creationDate==null){
            creationDate = new Date();
        }
    }

    @Override
    public String toString()
    {
        return name;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }

        IssueViewFilter other = (IssueViewFilter) obj;
        /* Id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        }
        else if(!id.equals(other.id)){
            return false;
        }

        return true;
    }
}
