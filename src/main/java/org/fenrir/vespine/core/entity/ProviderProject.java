package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140321
 */
@TableGenerator(
	    name="PROVIDER_PROJECT_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.ProviderProject", 
	    allocationSize=1)
@Entity
@Table(uniqueConstraints=@UniqueConstraint(columnNames = {"provider", "providerId"}))
public class ProviderProject implements Serializable, IProviderDataDefinition
{
	private static final long serialVersionUID = 607257997290644281L;

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE, generator="PROVIDER_PROJECT_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;

    private String provider;
    private String providerId;
    
    private String name;

    @ManyToMany(mappedBy="providerProjects", fetch= FetchType.LAZY)
    private Collection<IssueProject> issueProjects;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }

    @Override
    public String getProvider()
    {
    	return provider;
    }
    
    public void setProvider(String provider)
    {
    	this.provider = provider;
    }
    
    @Override
    public String getProviderId()
    {
    	return providerId;
    }
    
    public void setProviderId(String providerId)
    {
    	this.providerId = providerId;
    }
    
    @Override
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public Collection<IssueProject> getIssueProjects()
    {
    	return issueProjects;
    }
    
    public void setIssueProjects(Collection<IssueProject> issueProjects)
    {
    	this.issueProjects = issueProjects;
    }
    
    public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();        
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((provider == null) ? 0 : provider.hashCode());
        result = prime * result + ((providerId == null) ? 0 : providerId.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        ProviderProject other = (ProviderProject) obj;
        /* Provider */
        if(provider==null){
            if(other.provider!=null){
                return false;
            }
        } 
        else if(!provider.equals(other.provider)){
            return false;
        }
        /* Provider Id */
        if(providerId==null){
            if(other.providerId!=null){
                return false;
            }
        } 
        else if(!providerId.equals(other.providerId)){
            return false;
        }
        
        return true;
    }
}
