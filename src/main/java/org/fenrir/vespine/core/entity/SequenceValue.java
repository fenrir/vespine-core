package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.id.enhanced.TableGenerator;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140323
 */
@GenericGenerator(name = "SEQUENCES_ID_GENERATOR", 
	strategy = "org.fenrir.vespine.core.entity.generator.SequencesTableGenerator", 
	parameters = {
	    @Parameter(name = TableGenerator.TABLE_PARAM, value = "SEQUENCES"),
	    @Parameter(name = TableGenerator.SEGMENT_VALUE_PARAM, value = "org.fenrir.vespine.core.entity.SequenceValue")
	}
)
@Entity
public class SequenceValue implements Serializable
{
	private static final long serialVersionUID = 1980492505508839191L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SEQUENCES_ID_GENERATOR")
	private Long id;
	
	@Version
	private Long version;
	
	@Transient
	private String sequenceName;
	
	private Long value;
	
	@Temporal(TemporalType.TIMESTAMP)
    protected Date lastUpdated;
	
	public SequenceValue(String sequenceName)
	{
		this.sequenceName = sequenceName;
	}
	
	public Long getId()
	{
		return id;
	}
	
	public void setId(Long id)
	{
		this.id = id;
	}
	
	public Long getVersion()
	{
		return version;
	}
	
	public void setVersion(Long version)
	{
		this.version = version;
	}
	
	public String getSequenceName()
	{
		return sequenceName;
	}
	
	public Long getValue()
	{
		return value;
	}
	
	public void setValue(Long value)
	{
		this.value = value;
	}
	
	/**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();     
    }

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SequenceValue other = (SequenceValue) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
