package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140829
 */
@TableGenerator(
	    name="SPRINT_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.Sprint", 
	    allocationSize=1)
@Entity
public class Sprint implements Serializable
{
	private static final long serialVersionUID = -3461449385486170437L;

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE, generator="SPRINT_ID_GENERATOR")
    protected Long id;
    
    @Version
    protected Long version;
    
    @ManyToOne
    private IssueProject project;
    
    @Temporal(TemporalType.DATE)
    protected Date startDate;
    
    @Temporal(TemporalType.DATE)
    protected Date endDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
    
    public IssueProject getProject()
    {
    	return project;
    }
    
    public void setProject(IssueProject project)
    {
    	this.project = project;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
    public Date getStartDate() 
    {
		return startDate;
	}

	public void setStartDate(Date startDate) 
	{
		this.startDate = startDate;
	}

	public Date getEndDate() 
	{
		return endDate;
	}

	public void setEndDate(Date endDate) 
	{
		this.endDate = endDate;
	}

	public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }
    
    @Override
    public String toString()
    {
    	SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    	StringBuilder sb = new StringBuilder();
    	return sb.append("[").append(project).append("] ")
			.append(df.format(startDate))
    		.append(" - ")
    		.append(df.format(endDate)).toString();
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();        
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        Sprint other = (Sprint) obj;
        /* Id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        } 
        else if(!id.equals(other.id)){
            return false;
        }
        
        return true;
    }
}
