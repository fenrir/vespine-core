package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.compass.annotations.Index;
import org.compass.annotations.Searchable;
import org.compass.annotations.SearchableProperty;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140321
 */
@TableGenerator(
	    name="USER_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.User", 
	    allocationSize=1)

@Entity
@Table(name="USER_DATA")
@Searchable(root=false)
public class User implements Serializable
{
	private static final long serialVersionUID = -1267382695927150740L;

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE, generator="USER_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;
    
    @Column(unique=true)
    @SearchableProperty(index = Index.ANALYZED)
    private String username;
    
    @SearchableProperty(index = Index.ANALYZED)
    private String completeName;
    
    @Column(columnDefinition="Boolean default true")
    protected Boolean active;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
    public String getUsername()
    {
    	return username;
    }
    
    public void setUsername(String username)
    {
    	this.username = username;
    }
    
    public String getCompleteName()
    {
    	return completeName;
    }
    
    public void setCompleteName(String completeName)
    {
    	this.completeName = completeName;
    }
    
    public Boolean isActive()
    {
    	return active;
    }
    
    public void setActive(Boolean active)
    {
    	this.active = active;
    }
    
    public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }
    
    @Override
    public String toString()
    {
    	return completeName + " [" + username + "]";
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();        
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((username == null) ? 0 : username.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        User other = (User) obj;
        /* username */
        if(username==null){
            if(other.username!=null){
                return false;
            }
        } 
        else if(!username.equals(other.username)){
            return false;
        }
        
        return true;
    }
}
