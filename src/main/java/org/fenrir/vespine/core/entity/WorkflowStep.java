package org.fenrir.vespine.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140321
 */
@TableGenerator(
	    name="WORKFLOW_STEP_ID_GENERATOR", 
	    table="SEQUENCES", 
	    pkColumnName="SEQUENCE_NAME", 
	    valueColumnName="NEXT_VAL", 
	    pkColumnValue="org.fenrir.vespine.core.entity.WorkflowStep", 
	    allocationSize=1)

@Entity
public class WorkflowStep implements Serializable
{
	private static final long serialVersionUID = -6073814536684430715L;

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE, generator="WORKFLOW_STEP_ID_GENERATOR")
    private Long id;
    
    @Version
    private Long version;

    @ManyToOne
    private Workflow workflow;
    
    @ManyToOne
    private IssueStatus sourceStatus;
    
    @ManyToOne
    private IssueStatus destinationStatus;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
	
    public void setVersion(Long version)
    {
        this.version = version;
    }

    public Workflow getWorkflow()
    {
    	return workflow;
    }
    
    public void setWorkflow(Workflow workflow)
    {
    	this.workflow = workflow;
    }
    
    public IssueStatus getSourceStatus()
    {
    	return sourceStatus;
    }
    
    public void setSourceStatus(IssueStatus sourceStatus)
    {
    	this.sourceStatus = sourceStatus;
    }
    
    public IssueStatus getDestinationStatus()
    {
    	return destinationStatus;
    }
    
    public void setDestinationStatus(IssueStatus destinationStatus)
    {
    	this.destinationStatus = destinationStatus;
    }
    
    public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString()
    {
    	return sourceStatus.toString() + " -> " + destinationStatus.toString();
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();        
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((workflow == null) ? 0 : workflow.hashCode());
        result = prime * result + ((sourceStatus == null) ? 0 : sourceStatus.hashCode());
        result = prime * result + ((destinationStatus == null) ? 0 : destinationStatus.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        WorkflowStep other = (WorkflowStep) obj;
        /* Workflow */
        if(workflow==null){
            if(other.workflow!=null){
                return false;
            }
        } 
        else if(!workflow.equals(other.workflow)){
            return false;
        }
        /* Source status */
        if(sourceStatus==null){
            if(other.sourceStatus!=null){
                return false;
            }
        } 
        else if(!sourceStatus.equals(other.sourceStatus)){
            return false;
        }
        /* Destination status */
        if(destinationStatus==null){
            if(other.destinationStatus!=null){
                return false;
            }
        } 
        else if(!destinationStatus.equals(other.destinationStatus)){
            return false;
        }
        
        return true;
    }
}
