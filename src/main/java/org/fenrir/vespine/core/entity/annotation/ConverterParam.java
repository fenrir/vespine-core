package org.fenrir.vespine.core.entity.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * TODO 1.0 javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ConverterParam 
{
	String name();
	String value();
}
