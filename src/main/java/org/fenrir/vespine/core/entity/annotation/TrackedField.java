package org.fenrir.vespine.core.entity.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.fenrir.vespine.spi.util.IAttributeConverter;
import org.fenrir.vespine.spi.util.StringConverter;

/**
 * TODO 1.0 javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface TrackedField 
{
	Class<? extends IAttributeConverter<?, ?>> converter() default StringConverter.class;
	ConverterParam[] parameters() default {};
}
