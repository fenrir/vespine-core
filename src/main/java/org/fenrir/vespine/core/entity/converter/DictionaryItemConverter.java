package org.fenrir.vespine.core.entity.converter;

import java.util.Iterator;
import javax.inject.Inject;
import org.fenrir.vespine.core.dao.IDictionaryDAO;
import org.fenrir.vespine.core.entity.Dictionary;
import org.fenrir.vespine.core.entity.DictionaryItem;
import org.fenrir.vespine.spi.util.IAttributeConverter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140201
 */
public class DictionaryItemConverter implements IAttributeConverter<DictionaryItem, String>
{
	@Inject
	private IDictionaryDAO dictionaryDAO;
	
	public void setDictionaryDAO(IDictionaryDAO dictionaryDAO)
	{
		this.dictionaryDAO = dictionaryDAO;
	}
	
	@Override
	public void setParameter(String name, String value)
	{
		// No aplica
	}
	
	@Override
	public String convertToDatabaseColumn(DictionaryItem attributeValue) 
	{
		return attributeValue.getId().toString();
	}

	@Override
	public DictionaryItem convertToEntityAttribute(String columnValue) 
	{
		Dictionary dictionary = dictionaryDAO.findDictionaryByItemId(Long.parseLong(columnValue));
		if(dictionary.getItems()!=null){
			Iterator<DictionaryItem> iterator = dictionary.getItems().iterator();
			while(iterator.hasNext()){
				DictionaryItem item = iterator.next();
				if(item.getId().toString().equals(columnValue)){
					return item;
				}
			}
		}
		return null;
	}
}
