package org.fenrir.vespine.core.entity.converter;

import javax.inject.Inject;
import org.fenrir.vespine.core.dao.IIssueCategoryDAO;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.spi.util.IAttributeConverter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueCategoryConverter implements IAttributeConverter<IssueCategory, String> 
{
	@Inject
	private IIssueCategoryDAO categoryDAO;
	
	public void setCategoryDAO(IIssueCategoryDAO categoryDAO)
	{
		this.categoryDAO = categoryDAO;
	}
	
	@Override
	public void setParameter(String name, String value)
	{
		// No aplica
	}
	
	@Override
	public String convertToDatabaseColumn(IssueCategory attributeValue) 
	{
		return attributeValue.getId().toString();
	}

	@Override
	public IssueCategory convertToEntityAttribute(String columnValue) 
	{
		return categoryDAO.findCategoryById(Long.parseLong(columnValue));
	}
}
