package org.fenrir.vespine.core.entity.converter;

import javax.inject.Inject;
import org.fenrir.vespine.core.dao.IIssueProjectDAO;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.spi.util.IAttributeConverter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueProjectConverter implements IAttributeConverter<IssueProject, String> 
{
	@Inject
	private IIssueProjectDAO projectDAO;
	
	public void setProjectDAO(IIssueProjectDAO projectDAO)
	{
		this.projectDAO = projectDAO;
	}
	
	@Override
	public void setParameter(String name, String value)
	{
		// No aplica
	}
	
	@Override
	public String convertToDatabaseColumn(IssueProject attributeValue) 
	{
		return attributeValue.getId().toString();
	}

	@Override
	public IssueProject convertToEntityAttribute(String columnValue) 
	{
		return projectDAO.findProjectById(Long.parseLong(columnValue));
	}
}
