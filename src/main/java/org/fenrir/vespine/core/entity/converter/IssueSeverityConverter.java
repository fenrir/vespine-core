package org.fenrir.vespine.core.entity.converter;

import javax.inject.Inject;
import org.fenrir.vespine.core.dao.IIssueSeverityDAO;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.spi.util.IAttributeConverter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueSeverityConverter implements IAttributeConverter<IssueSeverity, String> 
{
	@Inject
	private IIssueSeverityDAO severityDAO;
	
	public void setSeverityDAO(IIssueSeverityDAO severityDAO)
	{
		this.severityDAO = severityDAO;
	}
	
	@Override
	public void setParameter(String name, String value)
	{
		// No aplica
	}
	
	@Override
	public String convertToDatabaseColumn(IssueSeverity attributeValue) 
	{
		return attributeValue.getId().toString();
	}

	@Override
	public IssueSeverity convertToEntityAttribute(String columnValue) 
	{
		return severityDAO.findSeverityById(Long.parseLong(columnValue));
	}
}
