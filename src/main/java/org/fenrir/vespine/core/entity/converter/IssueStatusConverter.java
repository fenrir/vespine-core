package org.fenrir.vespine.core.entity.converter;

import javax.inject.Inject;
import org.fenrir.vespine.core.dao.IIssueStatusDAO;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.spi.util.IAttributeConverter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueStatusConverter implements IAttributeConverter<IssueStatus, String> 
{
	@Inject
	private IIssueStatusDAO statusDAO;
	
	public void setStatusDAO(IIssueStatusDAO statusDAO)
	{
		this.statusDAO = statusDAO;
	}
	
	@Override
	public void setParameter(String name, String value)
	{
		// No aplica
	}
	
	@Override
	public String convertToDatabaseColumn(IssueStatus attributeValue) 
	{
		return attributeValue.getId().toString();
	}

	@Override
	public IssueStatus convertToEntityAttribute(String columnValue) 
	{
		return statusDAO.findStatusById(Long.parseLong(columnValue));
	}
}
