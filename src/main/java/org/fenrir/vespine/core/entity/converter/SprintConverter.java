package org.fenrir.vespine.core.entity.converter;

import javax.inject.Inject;
import org.fenrir.vespine.core.dao.ISprintDAO;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.spi.util.IAttributeConverter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class SprintConverter implements IAttributeConverter<Sprint, String> 
{
	@Inject
	private ISprintDAO sprintDAO;
	
	public void setProjectDAO(ISprintDAO sprintDAO)
	{
		this.sprintDAO = sprintDAO;
	}
	
	@Override
	public void setParameter(String name, String value)
	{
		// No aplica
	}
	
	@Override
	public String convertToDatabaseColumn(Sprint attributeValue) 
	{
		return attributeValue.getId().toString();
	}

	@Override
	public Sprint convertToEntityAttribute(String columnValue) 
	{
		return sprintDAO.findSprintById(Long.parseLong(columnValue));
	}
}
