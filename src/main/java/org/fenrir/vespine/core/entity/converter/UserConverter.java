package org.fenrir.vespine.core.entity.converter;

import javax.inject.Inject;
import org.fenrir.vespine.core.dao.IUserDAO;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.spi.util.IAttributeConverter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class UserConverter implements IAttributeConverter<User, String> 
{
	@Inject
	private IUserDAO userDAO;
	
	public void setUserDAO(IUserDAO userDAO)
	{
		this.userDAO = userDAO;
	}
	
	@Override
	public void setParameter(String name, String value)
	{
		// No aplica
	}
	
	@Override
	public String convertToDatabaseColumn(User attributeValue) 
	{
		return attributeValue.getId().toString();
	}

	@Override
	public User convertToEntityAttribute(String columnValue) 
	{
		return userDAO.findUserById(Long.parseLong(columnValue));
	}
}