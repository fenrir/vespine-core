package org.fenrir.vespine.core.entity.generator;

import java.util.Properties;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.NamingStrategy;
import org.hibernate.cfg.ObjectNameNormalizer;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.id.enhanced.TableGenerator;
import org.hibernate.type.LongType;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140326
 */
class ConcreteSequenceTableGenerator extends TableGenerator
{
	private String tableName;
	private String sequenceName;
	
	public ConcreteSequenceTableGenerator(String tableName, String sequenceName)
	{
		this.tableName = tableName;
		this.sequenceName = sequenceName;
	}
	
	public void configure(SessionImplementor session)
	{
		Properties properties = new Properties();
    	properties.put(TableGenerator.TABLE_PARAM, tableName);
	    properties.put(TableGenerator.SEGMENT_VALUE_PARAM, sequenceName);
	    properties.put(TableGenerator.SEGMENT_COLUMN_PARAM, TableGenerator.DEF_SEGMENT_COLUMN);
	    properties.put(TableGenerator.SEGMENT_LENGTH_PARAM, TableGenerator.DEF_SEGMENT_LENGTH);
	    properties.put(TableGenerator.VALUE_COLUMN_PARAM, TableGenerator.DEF_VALUE_COLUMN);
	    properties.put(TableGenerator.IDENTIFIER_NORMALIZER, new ObjectNameNormalizer() 
	    {
            @Override
            protected boolean isUseQuotedIdentifiersGlobally() 
            {
                return false;
            }

            @Override
            protected NamingStrategy getNamingStrategy() 
            {
                return new Configuration().getNamingStrategy();
            }
        });
	    properties.put(TableGenerator.INCREMENT_PARAM, TableGenerator.DEFAULT_INCREMENT_SIZE);
	    properties.put(TableGenerator.INITIAL_PARAM, TableGenerator.DEFAULT_INITIAL_VALUE);
    	super.configure(new LongType(), properties, session.getFactory().getDialect());
	}
}
