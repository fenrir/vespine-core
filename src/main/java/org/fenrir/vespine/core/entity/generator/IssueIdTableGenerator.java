package org.fenrir.vespine.core.entity.generator;

import java.io.Serializable;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.id.enhanced.TableGenerator;
import org.fenrir.vespine.core.entity.AbstractIssue;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueIdTableGenerator extends TableGenerator
{
    @Override
    public synchronized Serializable generate(SessionImplementor session, Object obj) 
    {
        if(obj instanceof AbstractIssue && ((AbstractIssue)obj).getId()!=null){
            return ((AbstractIssue)obj).getId();
        }
        else{
            return (Long)super.generate(session, obj);
        }
    }
}
