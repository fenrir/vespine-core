package org.fenrir.vespine.core.entity.generator;

import java.io.Serializable;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.id.enhanced.TableGenerator;
import org.fenrir.vespine.core.entity.SequenceValue;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140323
 */
public class SequencesTableGenerator extends TableGenerator
{
    @Override
    public synchronized Serializable generate(SessionImplementor session, Object obj) 
    {
    	if(!(obj instanceof SequenceValue)){
    		throw new IllegalArgumentException("No està permesa la generació de valors de la classe " + obj.getClass().getName() + " a través d'aquest generador");
    	}
    	
    	SequenceValue objValue = (SequenceValue)obj;
    	if(objValue.getId()!=null){
    		return objValue.getId();
    	}
    	else{
    		Long id = (Long)super.generate(session, obj);
    		// Important utilitzar un generador independent per tal que no hi hagi salts a la sequence 
    		ConcreteSequenceTableGenerator valueGenerator = new ConcreteSequenceTableGenerator(getTableName(), objValue.getSequenceName());
    		valueGenerator.configure(session);
    		Long sequenceValue = (Long)valueGenerator.generate(session, obj);
    		objValue.setValue(sequenceValue);
    		
    		return id;
    	}
    }
}
