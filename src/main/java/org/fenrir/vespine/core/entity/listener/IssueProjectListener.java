package org.fenrir.vespine.core.entity.listener;

import java.text.MessageFormat;
import org.hibernate.StatelessSession;
import org.hibernate.event.PostDeleteEvent;
import org.hibernate.event.PostDeleteEventListener;
import org.hibernate.event.PostInsertEvent;
import org.hibernate.event.PostInsertEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.entity.IssueProject;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140324
 */
@SuppressWarnings("serial")
public class IssueProjectListener implements PostInsertEventListener, PostDeleteEventListener 
{
	private final Logger log = LoggerFactory.getLogger(IssueProjectListener.class);
	
	@Override
	public void onPostInsert(PostInsertEvent event) 
	{
		if(!IssueProject.class.getName().equals(event.getEntity().getClass().getName())){
			if(log.isDebugEnabled()){
				log.debug("Descartada entitat {}", event.getEntity().getClass().toString());
			}
			return;
		}
		
		Object entity = event.getEntity();
        String entityId = event.getPersister().hasIdentifierProperty() ? event.getPersister().getIdentifier(entity, event.getSession()).toString() : "";
        
        if(log.isDebugEnabled()){
        	log.debug("Tractant event post-insert: {}@{}", entity.getClass().getName(), entityId);
        }
        
        // Sessió independient per guardar el registre de la sequence corresponent a BDD 
        StatelessSession session = event.getPersister().getFactory().openStatelessSession();
        session.beginTransaction();
        
        String sequenceName = MessageFormat.format(IssueProject.PROJECT_ABBREVIATION_SEQUENCE_PATTERN, entityId);
        String sql = MessageFormat.format("insert into SEQUENCES(SEQUENCE_NAME, NEXT_VAL) values(''{0}'', {1})", 
        		sequenceName, "1");
        // La sentència ha de ser SQL natiu perquè no hi ha una entita associada a la taula SEQUENCES
        session.createSQLQuery(sql).executeUpdate();
        
        session.getTransaction().commit();
        session.close();
	}
	
	@Override
	public void onPostDelete(PostDeleteEvent event) 
	{
		if(!IssueProject.class.getName().equals(event.getEntity().getClass().getName())){
			if(log.isDebugEnabled()){
				log.debug("Descartada entitat {}", event.getEntity().getClass().toString());
			}
			return;
		}
		
		Object entity = event.getEntity();
        String entityId = event.getPersister().hasIdentifierProperty() ? event.getPersister().getIdentifier(entity, event.getSession()).toString() : "";
        
        if(log.isDebugEnabled()){
        	log.debug("Tractant event post-delete: {}@{}", entity.getClass().getName(), entityId);
        }
        
        // Sessió independient per guardar el registre de la sequence corresponent a BDD 
        StatelessSession session = event.getPersister().getFactory().openStatelessSession();
        session.beginTransaction();
        
        String sequenceName = MessageFormat.format(IssueProject.PROJECT_ABBREVIATION_SEQUENCE_PATTERN, entityId);
        String sql = MessageFormat.format("delete from SEQUENCES where SEQUENCE_NAME=''{0}''", sequenceName);
        // La sentència ha de ser SQL natiu perquè no hi ha una entita associada a la taula SEQUENCES
        session.createSQLQuery(sql).executeUpdate();
        
        session.getTransaction().commit();
        session.close();
	}
}
