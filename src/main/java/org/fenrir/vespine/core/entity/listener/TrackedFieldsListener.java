package org.fenrir.vespine.core.entity.listener;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.StatelessSession;
import org.hibernate.event.PostInsertEvent;
import org.hibernate.event.PostInsertEventListener;
import org.hibernate.event.PostUpdateEvent;
import org.hibernate.event.PostUpdateEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.fenrir.vespine.spi.ApplicationContextHandler;
import org.fenrir.vespine.spi.IApplicationContext;
import org.fenrir.vespine.spi.util.IAttributeConverter;
import org.fenrir.vespine.core.entity.AuditRegistry;
import org.fenrir.vespine.core.entity.FieldChangelog;
import org.fenrir.vespine.core.entity.annotation.ConverterParam;
import org.fenrir.vespine.core.entity.annotation.TrackedEntity;
import org.fenrir.vespine.core.entity.annotation.TrackedField;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.util.IAuditableElement;
import org.fenrir.vespine.core.util.ITrackEntityRules;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140828
 */
@SuppressWarnings("serial")
public class TrackedFieldsListener implements PostInsertEventListener, PostUpdateEventListener
{
	private final Logger log = LoggerFactory.getLogger(TrackedFieldsListener.class);
	
	@Override
	public void onPostInsert(PostInsertEvent event) 
	{
		// Només es tracten les entitats marcades
		TrackedEntity trackedEntity = event.getEntity().getClass().getAnnotation(TrackedEntity.class);
		if(trackedEntity==null){
			if(log.isDebugEnabled()){
				log.debug("Descartada entitat {}", event.getEntity().getClass().toString());
			}
			
			return;
		}

		Object entity = event.getEntity();
        String entityId = event.getPersister().hasIdentifierProperty() ? event.getPersister().getIdentifier(entity, event.getSession()).toString() : "";
        String entityName = trackedEntity.name();
        
        if(log.isDebugEnabled()){
        	log.debug("Tractant event post-insert: {}@{}", entityName, entityId);
        }
        
        // Es descarta l'instancia de l'entitat si les regles definides en ella així ho indiquen
        if(entity instanceof ITrackEntityRules && !((ITrackEntityRules)entity).isInstanceTracked()){
        	log.debug("Descartada instancia {}:{}", event.getEntity().getClass().toString(), entityId);
        	return;
        }
 
        IApplicationContext applicationContext = ApplicationContextHandler.getInstance().getApplicationContext();
        
        AuditRegistry auditRegistry = null;
        if(entity instanceof IAuditableElement){        	
        	Long auditRecordReference = ((IAuditableElement)entity).getAuditRecordReference();
        	IAuditService auditService = (IAuditService)applicationContext.getRegisteredComponent(IAuditService.class);
        	auditRegistry = auditService.findAuditRegistryById(auditRecordReference);
        }
        
        // Sessió independient per guardar el registre a BDD 
        StatelessSession session = event.getPersister().getFactory().openStatelessSession();
        
        try {
            session.beginTransaction();
            
            List<Field> trackedFields = getTrackedFields(entity.getClass());
    		for(Field field:trackedFields){
    			try{
    				Object value = PropertyUtils.getProperty(entity, field.getName());
    				// Només es guarda si s'ha indicat valor
    				if(value!=null){
	    				TrackedField trackedField = field.getAnnotation(TrackedField.class);
	    				IAttributeConverter converter = (IAttributeConverter)applicationContext.getRegisteredComponent(trackedField.converter());
	    				// Es recuperen els paràmetres
	    				ConverterParam[] params = trackedField.parameters();
	    				if(params!=null){
	    					for(ConverterParam elem:params){
	    						String paramName = elem.name();
	    						String paramValue = elem.value();
	    						converter.setParameter(paramName, paramValue);
	    					}
	    				}    				
	    				String strValue = (String)converter.convertToDatabaseColumn(value);
	    				
	    				FieldChangelog fieldChangelog = new FieldChangelog();
	    				fieldChangelog.setAuditRegistry(auditRegistry);
	    				fieldChangelog.setEntity(entityName);
	    				fieldChangelog.setEntityId(Long.parseLong(entityId));
	    				fieldChangelog.setField(field.getName());
	    				fieldChangelog.setValue(strValue);
	    				fieldChangelog.setStartDate(new Date());
	    				fieldChangelog.setLastUpdated(new Date());
	    				
	    	            session.insert(fieldChangelog);
    				}
    			}
    			catch(Exception e){
    				log.error("Error accedint al camp {}: {}", new Object[]{field.getName(), e.getMessage(), e});
    			}
    		}

            session.getTransaction().commit();
        } 
		catch(HibernateException e){
			log.error("Error al auditant operació INSERT: {}" , e.getMessage(), e);
			session.getTransaction().rollback();
        }
        finally{
        	session.close();
        }
	}
	
	@Override
	public void onPostUpdate(PostUpdateEvent event) 
	{
		// Només es tracten les entitats marcades
		TrackedEntity trackedEntity = event.getEntity().getClass().getAnnotation(TrackedEntity.class);
		if(trackedEntity==null){
			if(log.isDebugEnabled()){
				log.debug("Descartada entitat {}", event.getEntity().getClass().toString());
			}
			
			return;
		}

		Object entity = event.getEntity();
        String entityId = event.getPersister().hasIdentifierProperty() ? event.getPersister().getIdentifier(entity, event.getSession()).toString() : "";
        String entityName = trackedEntity.name();
        
        if(log.isDebugEnabled()){
        	log.debug("Tractant event post-update: {}@{}", entityName, entityId);
        }
 
        IApplicationContext applicationContext = ApplicationContextHandler.getInstance().getApplicationContext();
        
        AuditRegistry auditRegistry = null;
        if(entity instanceof IAuditableElement){        	
        	Long auditRecordReference = ((IAuditableElement)entity).getAuditRecordReference();
        	IAuditService auditService = (IAuditService)applicationContext.getRegisteredComponent(IAuditService.class);
        	auditRegistry = auditService.findAuditRegistryById(auditRecordReference);
        }
            
        // Sessió independient per guardar el registre a BDD 
        StatelessSession session = event.getPersister().getFactory().openStatelessSession();
        try{
            session.beginTransaction();
            
            // Es recuperen els camps de l'històric obert
            List<FieldChangelog> currentChangelog = session.createQuery("from FieldChangelog " +
            			"where entity=:entity " +
            			"and entityId=:entityId " +
            			"and endDate is null")
            		.setString("entity", entityName)
            		.setLong("entityId", Long.parseLong(entityId))
            		.list();
            
            List<Field> trackedFields = getTrackedFields(entity.getClass());
    		for(Field field:trackedFields){
    			try{
    				Object value = PropertyUtils.getProperty(entity, field.getName());
    				TrackedField trackedField = field.getAnnotation(TrackedField.class);
    				IAttributeConverter converter = (IAttributeConverter)applicationContext.getRegisteredComponent(trackedField.converter());
    				// Es recuperen els paràmetres
    				ConverterParam[] params = trackedField.parameters();
    				if(params!=null){
    					for(ConverterParam elem:params){
    						String paramName = elem.name();
    						String paramValue = elem.value();
    						converter.setParameter(paramName, paramValue);
    					}
    				}
    				String strValue = null;
    				if(value!=null){
    					strValue = (String)converter.convertToDatabaseColumn(value);
    				}
    				
    				Iterator<FieldChangelog> iterator = currentChangelog.iterator();
    				// Cas que tingui un valor vigent
    				boolean registryUpdated = false;
    				while(iterator.hasNext() && !registryUpdated){
    					FieldChangelog elem = iterator.next();
    					if(field.getName().equals(elem.getField())){
    						// Si el nou valor és diferent al ja registrat, és guarda. 
    						if(!StringUtils.defaultString(strValue)
    								.equals(StringUtils.defaultString(elem.getValue()))){
    							FieldChangelog fieldChangelog = new FieldChangelog();
    							fieldChangelog.setAuditRegistry(auditRegistry);
	    	    				fieldChangelog.setEntity(entityName);
	    	    				fieldChangelog.setEntityId(Long.parseLong(entityId));
	    	    				fieldChangelog.setField(field.getName());
	    	    				fieldChangelog.setValue(strValue);
	    	    				fieldChangelog.setStartDate(new Date());
	    	    				fieldChangelog.setLastUpdated(new Date());
	    	    				session.insert(fieldChangelog);
	    	    				
	    	    				// Es tanca el registre existent
								elem.setEndDate(new Date());
								elem.setLastUpdated(new Date());
								session.update(elem);
    						}

    	    				registryUpdated = true;
    						
							// S'elimina el camp perquè ja s'ha tractat
    	    				iterator.remove();
    					}
    				}
    				// En cas de no tenir un valor vigent, es crea un de nou
    				if(!registryUpdated && strValue!=null){
    					FieldChangelog fieldChangelog = new FieldChangelog();
    					fieldChangelog.setAuditRegistry(auditRegistry);
	    				fieldChangelog.setEntity(entityName);
	    				fieldChangelog.setEntityId(Long.parseLong(entityId));
	    				fieldChangelog.setField(field.getName());
	    				fieldChangelog.setValue(strValue);
	    				fieldChangelog.setStartDate(new Date());
	    				fieldChangelog.setLastUpdated(new Date());
	    				
	    				session.insert(fieldChangelog);
    				}
    			}
    			catch(Exception e){
    				log.error("Error accedint al camp {}: {}", new Object[]{field.getName(), e.getMessage(), e});
    			}
    		}

            session.getTransaction().commit();
        } 
		catch(HibernateException e){
			log.error("Error al auditant operació UPDATE: {}" , e.getMessage(), e);
			session.getTransaction().rollback();
        }
        finally{
        	session.close();
        }
	}

	private List<Field> getTrackedFields(Class<?> entityClass)
	{
		List<Field> fields = new ArrayList<Field>();

		// Es busca a totes les superclasses
		List<Class<?>> classes = ClassUtils.getAllSuperclasses(entityClass);
		// També es mira la propia
		classes.add(entityClass);
		for(Class<?> clazz:classes){
			Field[] classFields = clazz.getDeclaredFields();
			for(Field field:classFields){
				 if(field.getAnnotation(TrackedField.class)!=null){
					 fields.add(field);
				 }
			}
		}
		
		return fields;
	}
}
