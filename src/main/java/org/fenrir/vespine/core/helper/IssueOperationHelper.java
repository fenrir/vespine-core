package org.fenrir.vespine.core.helper;

import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.fenrir.vespine.core.dto.SearchQuery;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140704
 */
public class IssueOperationHelper 
{
	/**
	 * 
	 * @param sendDate
	 * @param slaPattern {@link String} - El patró complirà l'expressió \\d[mwdh]
	 * @return
	 */
	public static Date computeSLADate(Date sendDate, String slaPattern)
	{
		// Mesos
        if(slaPattern.matches("\\dm")){
            int amount = Integer.parseInt(StringUtils.substringBefore(slaPattern, "m"));
            return DateUtils.addMonths(sendDate, amount);
        }
        // Setmanes
        else if(slaPattern.matches("\\dw")){
            int amount = Integer.parseInt(StringUtils.substringBefore(slaPattern, "w"));
            return DateUtils.addWeeks(sendDate, amount);
        }
        // Dies
        else if(slaPattern.matches("\\dd")){
            int amount = Integer.parseInt(StringUtils.substringBefore(slaPattern, "d"));
            return DateUtils.addDays(sendDate, amount);
        }
        // Hores
        else if(slaPattern.matches("\\dh")){
            int amount = Integer.parseInt(StringUtils.substringBefore(slaPattern, "h"));
            return DateUtils.addHours(sendDate, amount);
        }

        return null;
	}
	
	public static SearchQuery buildSearchQuery(String term)
	{
		SearchQuery indexQuery = new SearchQuery();
        indexQuery.addIndexQueryPart(SearchQuery.INDEX_FIELD_ISSUE_DESCRIPTION, term, false);
        indexQuery.addIndexQueryPart(SearchQuery.INDEX_FIELD_ISSUE_SUMMARY, term, false);
        indexQuery.addIndexQueryPart(SearchQuery.INDEX_FIELD_NOTE_CONTENT, term, false);
        
        return indexQuery;
	}
}
