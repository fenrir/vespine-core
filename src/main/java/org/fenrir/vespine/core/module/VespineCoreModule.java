package org.fenrir.vespine.core.module;

import java.util.Map;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.matcher.Matchers;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;
import com.google.inject.persist.PersistService;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.compass.core.Compass;
import org.compass.gps.CompassGps;
import org.compass.gps.device.jpa.JpaGpsDevice;
import org.compass.gps.device.jpa.indexer.DefaultJpaIndexEntitiesIndexer;
import org.compass.gps.device.jpa.indexer.JpaIndexEntitiesIndexer;
import org.compass.gps.impl.SingleCompassGps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.spi.ApplicationContextHandler;
import org.fenrir.vespine.spi.ExtensionConstants;
import org.fenrir.vespine.spi.builder.IIssueURLBuilder;
import org.fenrir.vespine.spi.service.IIssueDataService;
import org.fenrir.vespine.spi.util.BooleanConverter;
import org.fenrir.vespine.spi.util.DateConverter;
import org.fenrir.vespine.spi.util.IntegerConverter;
import org.fenrir.vespine.spi.util.StringConverter;
import org.fenrir.vespine.core.provider.IssueContentProvider;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueAdministrationService;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.core.service.IIssueWorkService;
import org.fenrir.vespine.core.service.IProviderAdministrationService;
import org.fenrir.vespine.core.service.ISearchIndexService;
import org.fenrir.vespine.core.service.IUserManagementService;
import org.fenrir.vespine.core.service.impl.AuditServiceImpl;
import org.fenrir.vespine.core.service.impl.CompassIndexServiceImpl;
import org.fenrir.vespine.core.service.impl.CoreAdministrationServiceImpl;
import org.fenrir.vespine.core.service.impl.IssueAdministrationServiceImpl;
import org.fenrir.vespine.core.service.impl.IssueSearchServiceImpl;
import org.fenrir.vespine.core.service.impl.IssueWorkServiceImpl;
import org.fenrir.vespine.core.service.impl.ProviderAdministrationServiceImpl;
import org.fenrir.vespine.core.service.impl.UserManagementServiceImpl;
import org.fenrir.vespine.core.service.impl.WorkflowServiceImpl;
import org.fenrir.vespine.core.service.IWorkflowService;
import org.fenrir.vespine.core.dao.IAlertTypeDAO;
import org.fenrir.vespine.core.dao.IAuditRegistryDAO;
import org.fenrir.vespine.core.dao.IIssueAlertDAO;
import org.fenrir.vespine.core.dao.IIssueCategoryDAO;
import org.fenrir.vespine.core.dao.IIssueDAO;
import org.fenrir.vespine.core.dao.IIssueNoteDAO;
import org.fenrir.vespine.core.dao.IIssueProjectDAO;
import org.fenrir.vespine.core.dao.IIssueSeverityDAO;
import org.fenrir.vespine.core.dao.IIssueStatusDAO;
import org.fenrir.vespine.core.dao.IIssueViewFilterDAO;
import org.fenrir.vespine.core.dao.IIssueWorkRegistryDAO;
import org.fenrir.vespine.core.dao.IProviderCategoryDAO;
import org.fenrir.vespine.core.dao.IProviderProjectDAO;
import org.fenrir.vespine.core.dao.IProviderSeverityDAO;
import org.fenrir.vespine.core.dao.IProviderStatusDAO;
import org.fenrir.vespine.core.dao.ISearchIndexDAO;
import org.fenrir.vespine.core.dao.ISprintDAO;
import org.fenrir.vespine.core.dao.ITagDAO;
import org.fenrir.vespine.core.dao.IUserDAO;
import org.fenrir.vespine.core.dao.IWorkflowActionDAO;
import org.fenrir.vespine.core.dao.IWorkflowDAO;
import org.fenrir.vespine.core.dao.IWorkflowStepDAO;
import org.fenrir.vespine.core.dao.IDictionaryDAO;
import org.fenrir.vespine.core.dao.IDictionaryItemDAO;
import org.fenrir.vespine.core.dao.IIssueCustomFieldDAO;
import org.fenrir.vespine.core.dao.IIssueCustomValueDAO;
import org.fenrir.vespine.core.dao.IIssueWipRegistryDAO;
import org.fenrir.vespine.core.dao.ISequenceValueDAO;
import org.fenrir.vespine.core.dao.impl.AuditRegistryDAOImpl;
import org.fenrir.vespine.core.dao.impl.DictionaryDAOImpl;
import org.fenrir.vespine.core.dao.impl.DictionaryItemDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueCustomFieldDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueCustomValueDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueWipRegistryDAOImpl;
import org.fenrir.vespine.core.dao.impl.SequenceValueDAOImpl;
import org.fenrir.vespine.core.dao.impl.AlertTypeDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueAlertDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueCategoryDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueNoteDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueProjectDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueSeverityDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueStatusDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueViewFilterDAOImpl;
import org.fenrir.vespine.core.dao.impl.IssueWorkRegistryDAOImpl;
import org.fenrir.vespine.core.dao.impl.LuceneSearchIndexDAOImpl;
import org.fenrir.vespine.core.dao.impl.ProviderCategoryDAOImpl;
import org.fenrir.vespine.core.dao.impl.ProviderProjectDAOImpl;
import org.fenrir.vespine.core.dao.impl.ProviderSeverityDAOImpl;
import org.fenrir.vespine.core.dao.impl.ProviderStatusDAOImpl;
import org.fenrir.vespine.core.dao.impl.SprintDAOImpl;
import org.fenrir.vespine.core.dao.impl.TagDAOImpl;
import org.fenrir.vespine.core.dao.impl.UserDAOImpl;
import org.fenrir.vespine.core.dao.impl.WorkflowActionDAOImpl;
import org.fenrir.vespine.core.dao.impl.WorkflowDAOImpl;
import org.fenrir.vespine.core.dao.impl.WorkflowStepDAOImpl;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.DeletedIssue;
import org.fenrir.vespine.core.entity.Issue;
import org.fenrir.vespine.core.entity.IssueNote;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.core.entity.converter.DictionaryItemConverter;
import org.fenrir.vespine.core.entity.converter.IssueCategoryConverter;
import org.fenrir.vespine.core.entity.converter.IssueProjectConverter;
import org.fenrir.vespine.core.entity.converter.IssueSeverityConverter;
import org.fenrir.vespine.core.entity.converter.IssueStatusConverter;
import org.fenrir.vespine.core.entity.converter.SprintConverter;
import org.fenrir.vespine.core.entity.converter.UserConverter;
import org.fenrir.vespine.core.util.CompassIndexConnectionBuilder;

/**
 * TODO v1.0 Documentació
 * Mòdul de configuració de les depedències entre els components del Core.
 * S'ha de definir externament el mòdul de persistencia JPA.
 * @author Antonio Archilla Nava
 * @version v0.1.20141019
 */
public class VespineCoreModule extends AbstractModule
{
	public static final String PROPERTY_DATA_PERSISTENCE_UNIT = "DATA_PERSISTENCE_UNIT";
	public static final String PROPERTY_WORKSPACE_FOLDER = "WORKSPACE_FOLDER";
	public static final String PROPERTY_INDEX_STORE = "INDEX_STORE";
	public static final String PROPERTY_INDEX_STORE_FILESYSTEM_VALUE = "INDEX_2_FILESYSTEM";
	public static final String PROPERTY_INDEX_STORE_DATABASE_VALUE = "INDEX_2_DATABASE";
	public static final String PROPERTY_INDEX_LUCENE_JDBC_DIALECT = "LUCENE_JDBC_DIALECT";
	public static final String PROPERTY_INDEX_JDBC_DATASOURCE = "JDBC_DATASOURCE";
	
	private final Logger log = LoggerFactory.getLogger(VespineCoreModule.class);

	private Map<String, Object> properties;
	
	public VespineCoreModule(Map<String, Object> properties)
	{
		this.properties = properties;
	}
	
    @Override
    protected void configure()
    {
    	/* DAOs */
        bind(IIssueProjectDAO.class).to(IssueProjectDAOImpl.class).in(Singleton.class);
        bind(IIssueStatusDAO.class).to(IssueStatusDAOImpl.class).in(Singleton.class);
        bind(IIssueCategoryDAO.class).to(IssueCategoryDAOImpl.class).in(Singleton.class);
        bind(IIssueSeverityDAO.class).to(IssueSeverityDAOImpl.class).in(Singleton.class);
        bind(IIssueDAO.class).to(IssueDAOImpl.class).in(Singleton.class);
        bind(IIssueViewFilterDAO.class).to(IssueViewFilterDAOImpl.class).in(Singleton.class);
        bind(ITagDAO.class).to(TagDAOImpl.class).in(Singleton.class);        
        bind(IIssueAlertDAO.class).to(IssueAlertDAOImpl.class).in(Singleton.class);
        bind(IAlertTypeDAO.class).to(AlertTypeDAOImpl.class).in(Singleton.class);
        bind(IIssueNoteDAO.class).to(IssueNoteDAOImpl.class).in(Singleton.class);
        bind(ISearchIndexDAO.class).to(LuceneSearchIndexDAOImpl.class).in(Singleton.class);
        bind(ISprintDAO.class).to(SprintDAOImpl.class).in(Singleton.class);
        bind(IIssueWorkRegistryDAO.class).to(IssueWorkRegistryDAOImpl.class).in(Singleton.class);
        bind(IUserDAO.class).to(UserDAOImpl.class).in(Singleton.class);
        /* DAOs mestres proveidors de dades */
        bind(IProviderStatusDAO.class).to(ProviderStatusDAOImpl.class).in(Singleton.class);
        bind(IProviderCategoryDAO.class).to(ProviderCategoryDAOImpl.class).in(Singleton.class);
        bind(IProviderSeverityDAO.class).to(ProviderSeverityDAOImpl.class).in(Singleton.class);
        bind(IProviderProjectDAO.class).to(ProviderProjectDAOImpl.class).in(Singleton.class);
        /* DAOs workflow */
        bind(IWorkflowDAO.class).to(WorkflowDAOImpl.class).in(Singleton.class);
        bind(IWorkflowStepDAO.class).to(WorkflowStepDAOImpl.class).in(Singleton.class);
        bind(IWorkflowActionDAO.class).to(WorkflowActionDAOImpl.class).in(Singleton.class);
        /* DAOs custom fields */
        bind(IIssueCustomFieldDAO.class).to(IssueCustomFieldDAOImpl.class).in(Singleton.class);
        bind(IIssueCustomValueDAO.class).to(IssueCustomValueDAOImpl.class).in(Singleton.class);
        /* DAOs diccionaris de dades */
        bind(IDictionaryDAO.class).to(DictionaryDAOImpl.class).in(Singleton.class);
        bind(IDictionaryItemDAO.class).to(DictionaryItemDAOImpl.class).in(Singleton.class);
        /* DAO registres timer */
        bind(IIssueWipRegistryDAO.class).to(IssueWipRegistryDAOImpl.class).in(Singleton.class);
        /* DAO sequencies */
        bind(ISequenceValueDAO.class).to(SequenceValueDAOImpl.class).in(Singleton.class);
        /* DAO auditoria */
        bind(IAuditRegistryDAO.class).to(AuditRegistryDAOImpl.class).in(Singleton.class);
        
        /* Services */
        bind(ICoreAdministrationService.class).to(CoreAdministrationServiceImpl.class).in(Singleton.class);
        bind(IIssueSearchService.class).to(IssueSearchServiceImpl.class).in(Singleton.class);
        bind(IIssueAdministrationService.class).to(IssueAdministrationServiceImpl.class).in(Singleton.class); 
        bind(IIssueWorkService.class).to(IssueWorkServiceImpl.class).in(Singleton.class);
        bind(ISearchIndexService.class).to(CompassIndexServiceImpl.class).in(Singleton.class);
        bind(IProviderAdministrationService.class).to(ProviderAdministrationServiceImpl.class).in(Singleton.class);
        bind(IUserManagementService.class).to(UserManagementServiceImpl.class).in(Singleton.class);
        bind(IWorkflowService.class).to(WorkflowServiceImpl.class).in(Singleton.class);
        bind(IAuditService.class).to(AuditServiceImpl.class).in(Singleton.class);
        
        /* Altres */
        bind(IssueContentProvider.class).in(Singleton.class);
        // Conversors pels conversors necessaris per l'historic de camps
        bind(StringConverter.class);
        bind(IntegerConverter.class);
        bind(BooleanConverter.class);
        bind(DateConverter.class);
        bind(IssueProjectConverter.class);
        bind(IssueStatusConverter.class);
        bind(IssueSeverityConverter.class);
        bind(IssueCategoryConverter.class);
        bind(DictionaryItemConverter.class);
        bind(SprintConverter.class);
        bind(UserConverter.class);
        
        /* Bindings per les extensions. 
         * Si no s'inicialitzen es produeix un error en el moment en que es demana l'injecció de l'objecte.
         * Si no hi ha cap objecte configurat amb la key especificada, s'injectarà un Set buit  
         */
		Multibinder.newSetBinder(binder(), String.class, Names.named(ExtensionConstants.EXTENSION_PROVIDER_BINDING_ID));
		Multibinder.newSetBinder(binder(), IIssueDataService.class);
		Multibinder.newSetBinder(binder(), IIssueURLBuilder.class);
		
		/* Interceptor per inicialitzar el servei d'index quan s'inicia la persistència 
		 */
		bindInterceptor(Matchers.subclassesOf(PersistService.class), 
				Matchers.any(), 
				new MethodInterceptor() 
				{
					@Override
					public Object invoke(MethodInvocation invocation) throws Throwable 
					{
						String methodName = invocation.getMethod().getName();
						log.debug("Interceptat mètode del servei de persistència: {}", methodName);
						
						// S'inicialitza la persistència normalment
						Object result = invocation.proceed();						
						// Inicialització de l'índex de cerca
						if("start".equals(methodName)){
							ISearchIndexService indexService = (ISearchIndexService)ApplicationContextHandler.getInstance().getApplicationContext().getRegisteredComponent(ISearchIndexService.class);
							indexService.enableIndex();
						}
						
						return result;
					}
				});
    }
    
    @Provides
    @Singleton
    Compass createCompassInstance()
    {        
        try{
            String currentWorkspace = (String)properties.get(PROPERTY_WORKSPACE_FOLDER);
            CompassIndexConnectionBuilder connectionBuilder = new CompassIndexConnectionBuilder();
            
            // Localització de l'index
            if(PROPERTY_INDEX_STORE_FILESYSTEM_VALUE.equals(properties.get(PROPERTY_INDEX_STORE))){
            	connectionBuilder.setStoreDir(currentWorkspace);
            }
            else if(PROPERTY_INDEX_STORE_DATABASE_VALUE.equals(properties.get(PROPERTY_INDEX_STORE))){
            	connectionBuilder.setJdbcDialect((String)properties.get(PROPERTY_INDEX_LUCENE_JDBC_DIALECT));
            	connectionBuilder.setJdbcDatasource((DataSource)properties.get(PROPERTY_INDEX_JDBC_DATASOURCE));
            }
            else{
            	throw new IllegalStateException("No s'ha especificat cap ubicació per l'índex de cerca");
            }
            
            // Entitats gestionades per l'índex
            connectionBuilder.addManagedEntity(IssueNote.class)
            		.addManagedEntity(User.class)
            		.addManagedEntity(AbstractIssue.class)
            		.addManagedEntity(Issue.class)
            		.addManagedEntity(DeletedIssue.class);
            
            return connectionBuilder.createConnection();
        }
        catch(Exception e){
            log.error("Error en configurar la connexió a l'índex: {}", e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    
    @Provides
    @Singleton
    CompassGps createCompassGpsInstance(Compass compass, EntityManager manager)
    {        
        CompassGps compassGps = new SingleCompassGps(compass);
        JpaGpsDevice device = new JpaGpsDevice((String)properties.get(PROPERTY_DATA_PERSISTENCE_UNIT), manager.getEntityManagerFactory());
        // Permetre sincronitzar-se amb els canvis a la base de dades
        device.setInjectEntityLifecycleListener(true);
        /* Indexador d'entitats */
        JpaIndexEntitiesIndexer indexer = new DefaultJpaIndexEntitiesIndexer();
        indexer.setJpaGpsDevice(device);
        device.setEntitiesIndexer(indexer);
        compassGps.addGpsDevice(device);
        
        return compassGps;
    }
}
