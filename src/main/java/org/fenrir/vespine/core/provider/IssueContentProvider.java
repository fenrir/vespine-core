package org.fenrir.vespine.core.provider;

import java.util.Collection;
import java.util.Set;
import javax.inject.Inject;
import com.google.inject.name.Named;
import org.fenrir.vespine.spi.ExtensionConstants;
import org.fenrir.vespine.spi.builder.IIssueURLBuilder;
import org.fenrir.vespine.spi.service.IIssueDataService;

/**
 * TODO v1.0 Documentació
 * @author Anatonio Archilla Nava
 * @version v0.1.20140103
 */
public class IssueContentProvider 
{
	@Inject
	@Named(ExtensionConstants.EXTENSION_PROVIDER_BINDING_ID)
	private Set<String> providers;
	
	@Inject
	private Set<IIssueDataService> issueDataServices;
	
	@Inject
	private Set<IIssueURLBuilder> issueURLBuilders;
	
	public void setProviders(Set<String> providers)
	{
		this.providers = providers;
	}
	
	public void setIssueDataServices(Set<IIssueDataService> issueDataServices)
	{
		this.issueDataServices = issueDataServices;
	}
	
	public void setIssueURLBuilders(Set<IIssueURLBuilder> issueURLBuilders)
	{
		this.issueURLBuilders = issueURLBuilders;
	}
	
	public Collection<String> getProviders()
	{
		return providers;
	}
	
	public IIssueDataService getIssueDataService(String provider)
	{
		for(IIssueDataService service:issueDataServices){
			if(provider.equals(service.getProvider())){
				return service;
			}
		}
		
		return null;
	}
	
	public IIssueURLBuilder getIssueURLBuilder(String provider)
	{
		for(IIssueURLBuilder builder:issueURLBuilders){
			if(provider.equals(builder.getProvider())){
				return builder;
			}
		}
		
		return null;
	}
}
