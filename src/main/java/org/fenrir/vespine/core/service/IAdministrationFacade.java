package org.fenrir.vespine.core.service;

import java.util.List;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140624
 */
public interface IAdministrationFacade 
{
	/*---------------------------------*
     *           Projectes             *
     *---------------------------------*/
	public List<IProjectDTO> findAllProjects() throws BusinessException;
	public List<IProjectDTO> findProjectsByWorkflow(String workflowId) throws BusinessException;
	public IProjectDTO findProjectById(String id) throws BusinessException;
	public IProjectDTO createProject(String name, String abbreviation, String workflow, List<String> providerProjects) throws BusinessException;
	public IProjectDTO updateProject(String id, String name, String workflow, List<String> providerProjects) throws BusinessException;
	public void deleteProject(String id) throws BusinessException;
	
	/*---------------------------------*
     *           Categories            *
     *---------------------------------*/
    public List<ICategoryDTO> findAllCategories() throws BusinessException;
    public ICategoryDTO findCategoryById(String id) throws BusinessException;
    public ICategoryDTO createCategory(String name, List<String> providerCategories) throws BusinessException;
    public ICategoryDTO updateCategory(String id, String name, List<String> providerCategories) throws BusinessException;
    public void deleteCategory(String id) throws BusinessException;
    
    /*---------------------------------*
     *           Severitats            *
     *---------------------------------*/
    public List<ISeverityDTO> findAllSeverities() throws BusinessException;
    public ISeverityDTO findSeverityById(String id) throws BusinessException;
    public ISeverityDTO createSeverity(String name, String slaPattern, List<String> providerSeverities) throws BusinessException;
    public ISeverityDTO updateSeverity(String id, String name, String slaPattern, List<String> providerSeverities) throws BusinessException;
    public void deleteSeverity(String id) throws BusinessException;
    
    /*---------------------------------*
     *             Estats              *
     *---------------------------------*/
    public List<IStatusDTO> findAllStatus() throws BusinessException;
    public IStatusDTO findStatusById(String id) throws BusinessException;
    public List<IStatusDTO> findWorkflowNextStatus(String projectId, String currentStatus) throws BusinessException;
    public String getStatusColor(String statusId) throws BusinessException;
    public IStatusDTO createStatus(String name, String color, List<String> providerStatus) throws BusinessException;
    public IStatusDTO updateStatus(String id, String name, String color, List<String> providerStatus) throws BusinessException;
    public void deleteIssueStatus(String id) throws BusinessException;
    
    /*---------------------------------*
     *           Workflow              *
     *---------------------------------*/
    public List<IWorkflowDTO> findAllWorkflows() throws BusinessException;
    public IWorkflowDTO findWorkflowById(String id) throws BusinessException;
    public boolean hasWorkflowRelatedProjects(String workflowId) throws BusinessException;
    public IWorkflowDTO createWorkflow(String name, String initialStatus, List<IWorkflowStepDTO> steps) throws BusinessException;
	public IWorkflowDTO updateWorkflow(String id, String name, String initialStatus, List<IWorkflowStepDTO> steps) throws BusinessException;
	public void deleteWorkflow(String id) throws BusinessException;
	
	/*---------------------------------*
     *          Tipus d'alerta         *
     *---------------------------------*/
	public List<IAlertTypeDTO> findAllAlertTypes() throws BusinessException;
	public IAlertTypeDTO findAlertTypeById(String id) throws BusinessException;
	public IAlertTypeDTO createAlertType(String name, String description, String icon, String alertRule, boolean generateOld) throws BusinessException;
    public IAlertTypeDTO updateAlertType(String id, String name, String description, String icon, String alertRule) throws BusinessException;
    public void deleteAlertType(String id) throws BusinessException;
    
    /*---------------------------------*
     *              Tags               *
     *---------------------------------*/
    public List<ITagDTO> findAllTags() throws BusinessException;
    public List<ITagDTO> findTagsFiltered(String prefix) throws BusinessException;
    public List<ITagDTO> findTagsByName(String name) throws BusinessException;
    public List<ITagDTO> findPreferredTags() throws BusinessException;
    public List<ITagDTO> findPreferredTagsFiltered(String prefix) throws BusinessException;
    public ITagDTO findTagById(String tagId) throws BusinessException;
    public ITagDTO createTag(String name, boolean preferred) throws BusinessException;
    public ITagDTO updateTag(String tagId, String name, boolean preferred) throws BusinessException;
    public void deleteTag(String tagId) throws BusinessException;
    
    /*---------------------------------*
     *           Diccionaris           *
     *---------------------------------*/
    public List<IDictionaryDTO> findAllDictionaries() throws BusinessException;
    public IDictionaryDTO findDictionaryById(String dictionaryId) throws BusinessException;
    public List<IDictionaryItemDTO> findDictionaryItems(String dictionaryId) throws BusinessException;
    public IDictionaryItemDTO findDictionaryItem(String itemId) throws BusinessException;
    
    public IDictionaryDTO createDictionary(String name, String description, List<IDictionaryItemDTO> items) throws BusinessException;
    public IDictionaryDTO updateDictionary(String id, String name, String description, List<IDictionaryItemDTO> items) throws BusinessException;
    public void deleteDictionary(String id) throws BusinessException;
    
    /*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
    public List<IFieldType> getAllFieldTypes() throws BusinessException;
    public List<ICustomFieldDTO> findAllCustomFields() throws BusinessException;
    public List<ICustomFieldDTO> findCustomFieldsByProject(String projectId) throws BusinessException;
    public ICustomFieldDTO findCustomFieldById(String fieldId) throws BusinessException;
    
    public ICustomFieldDTO createCustomField(String name, IFieldType fieldType, String dataProviderId, boolean mandatory, List<String> projects) throws BusinessException;
    public ICustomFieldDTO updateCustomField(String id, String name, IFieldType fieldType, String dataProviderId, boolean mandatory, List<String> projects) throws BusinessException;
    public void deleteCustomField(String id) throws BusinessException;
    
    /*---------------------------------*
     *       Providers Projectes       *
     *---------------------------------*/
    public List<IProviderElementDTO> findAllProviderProjects() throws BusinessException;
    public List<IProviderElementDTO> findProviderProjectsByProvider(String provider) throws BusinessException; 
    public IProviderElementDTO findProviderProjectById(String id) throws BusinessException;
    public IProviderElementDTO findProviderProjectByProviderData(String provider, String providerId) throws BusinessException;

    /*---------------------------------*
     *         Providers Estats        *
     *---------------------------------*/
    public List<IProviderElementDTO> findAllProviderStatus() throws BusinessException;
    public List<IProviderElementDTO> findProviderStatusByProvider(String provider) throws BusinessException; 
    public IProviderElementDTO findProviderStatusById(String id) throws BusinessException;
    public IProviderElementDTO findProviderStatusByProviderData(String provider, String providerId) throws BusinessException;
    
    /*---------------------------------*
     *       Providers Severitats      *
     *---------------------------------*/
    public List<IProviderElementDTO> findAllProviderSeverities() throws BusinessException;
    public List<IProviderElementDTO> findProviderSeveritiesByProvider(String provider) throws BusinessException; 
    public IProviderElementDTO findProviderSeverityById(String id) throws BusinessException;
    public IProviderElementDTO findProviderSeverityByProviderData(String provider, String providerId) throws BusinessException; 
    
    /*---------------------------------*
     *       Providers Categories      *
     *---------------------------------*/
    public List<IProviderElementDTO> findAllProviderCategories() throws BusinessException;
    public List<IProviderElementDTO> findProviderCategoriesByProvider(String provider) throws BusinessException; 
    public IProviderElementDTO findProviderCategoryById(String id) throws BusinessException;
    public IProviderElementDTO findProviderCategoryByProviderData(String provider, String providerId) throws BusinessException;
    
    /*---------------------------------*
     *             Filtres             *
     *---------------------------------*/
    public List<IViewFilterDTO> findApplicationViewFilters() throws BusinessException;
    public List<IViewFilterDTO> findUserViewFilters() throws BusinessException;
    public IViewFilterDTO findViewFilterById(String filterId) throws BusinessException;
    public void reorderViewFilter(String filterId, Integer order) throws BusinessException;
    public IViewFilterDTO createViewFilter(String filterName, String filterDescription, String filterQuery, String filterOrderClause) throws BusinessException;
    public IViewFilterDTO updateViewFilter(String filterId, String filterName, String filterDescription, String filterQuery, String filterOrderClause) throws BusinessException;
    public void deleteViewFilter(String filterId) throws BusinessException;
}
