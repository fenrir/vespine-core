package org.fenrir.vespine.core.service;

import org.fenrir.vespine.core.entity.AuditRegistry;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140422
 */
public interface IAuditService 
{
	public AuditRegistry findAuditRegistryById(Long registryId);
	
	public Long auditAction(String action, String username);
	public void auditAction(String action, String objectId, String username, String resultCode, String resultCause);	
	public void resolveAuditAction(Long registryId, String objectId, String resultCode, String resultCause);
}
