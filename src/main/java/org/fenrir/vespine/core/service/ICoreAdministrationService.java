package org.fenrir.vespine.core.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.fenrir.vespine.core.entity.AlertType;
import org.fenrir.vespine.core.entity.Dictionary;
import org.fenrir.vespine.core.entity.DictionaryItem;
import org.fenrir.vespine.core.entity.FieldType;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.IssueViewFilter;
import org.fenrir.vespine.core.entity.ProviderCategory;
import org.fenrir.vespine.core.entity.ProviderProject;
import org.fenrir.vespine.core.entity.ProviderSeverity;
import org.fenrir.vespine.core.entity.ProviderStatus;
import org.fenrir.vespine.core.entity.Tag;
import org.fenrir.vespine.core.entity.Workflow;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140302
 */
public interface ICoreAdministrationService 
{
	/*---------------------------------*
     *           Projectes             *
     *---------------------------------*/
	public List<IssueProject> findAllProjects();
    public IssueProject findProjectById(Long id);
	
    public boolean isInitialUpdateNeeded();
    public IssueProject createIssueProject(String name, String abbreviation, Workflow workflow, Set<ProviderProject> providerProjects);
    public IssueProject updateIssueProject(Long id, String name, Workflow workflow, Set<ProviderProject> providerProjects);
    public void deleteIssueProject(Long id);
    
    /*---------------------------------*
     *           Categories            *
     *---------------------------------*/
    public List<IssueCategory> findAllCategories();
    public IssueCategory findCategoryById(Long id);
    
    public IssueCategory createIssueCategory(String name, Set<ProviderCategory> providerCategories);
    public IssueCategory updateIssueCategory(Long id, String name, Set<ProviderCategory> providerCategories);
    public void deleteIssueCategory(Long id);
    
    /*---------------------------------*
     *           Severitats            *
     *---------------------------------*/
    public List<IssueSeverity> findAllSeverities();
    public IssueSeverity findSeverityById(Long id);
    
    public IssueSeverity createIssueSeverity(String name, String slaPattern, Set<ProviderSeverity> providerSeverities);
    public IssueSeverity updateIssueSeverity(Long id, String name, String slaPattern, Set<ProviderSeverity> providerSeverities);
    public void deleteIssueSeverity(Long id);
    
    /*---------------------------------*
     *             Estats              *
     *---------------------------------*/
    public List<IssueStatus> findAllStatus();
    public IssueStatus findStatusById(Long id);
    
    public IssueStatus createIssueStatus(String name, String color, Set<ProviderStatus> providerStatus);
    public IssueStatus updateIssueStatus(Long id, String name, String color, Set<ProviderStatus> providerStatus);
    public void deleteIssueStatus(Long id);
    
    /*---------------------------------*
     *          Filtres vistes         *
     *---------------------------------*/
    public List<IssueViewFilter> findApplicationViewFilters();
    public List<IssueViewFilter> findUserViewFilters();
    public IssueViewFilter findViewFilterById(Long id);
    
    public IssueViewFilter createIssueViewFilter(String name, String description, String filterQuery, String orderByClause);
    public IssueViewFilter updateIssueViewFilter(Long id, String name, String description, String filterQuery, String orderByClause);
    public void reorderIssueViewFilter(Long id, int order);
    public void deleteIssueViewFilter(Long id);
    
    /*---------------------------------*
     *              Tags               *
     *---------------------------------*/
    public List<Tag> findAllTags();
    public List<Tag> findPreferredTags();
    public List<Tag> findTagsByName(String description);
    public List<Tag> findTagsFiltered(String prefix);    
    public List<Tag> findPreferredTagsFiltered(String prefix);
    public Tag findTagById(Long id);
    
    public Tag createTag(String name, Boolean preferred);
    public Tag updateTag(Long id, String name, Boolean preferred);
    public void deleteTag(Long id);
    
    /*---------------------------------*
     *          Tipus d'alerta         *
     *---------------------------------*/
    public List<AlertType> findAllAlertTypes();
    public AlertType findAlertTypeById(Long alertTypeId);
    
    public AlertType createAlertType(String name, String description, String icon, String alertRule, boolean generateOld) throws BusinessException;
    public AlertType updateAlertType(Long id, String name, String description, String icon, String alertRule);
    public void deleteAlertType(Long id);
    
    /*---------------------------------*
     *           Diccionaris           *
     *---------------------------------*/
    public List<Dictionary> findAllDictionaries();
    public Dictionary findDictionaryById(Long dictionaryId);
    public List<DictionaryItem> findDictionaryItems(Long dictionaryId);
    public DictionaryItem findDictionaryItem(Long itemId);
    
    public Dictionary createDictionary(String name, String description, List<DictionaryItem> items);
    public Dictionary updateDictionary(Long id, String name, String description, List<DictionaryItem> items);
    public void deleteDictionary(Long id);
    
    /*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
    public List<IssueCustomField> findAllCustomFields();
    public List<IssueCustomField> findCustomFieldsByProject(IssueProject project);
    public IssueCustomField findCustomFieldById(Long fieldId);
    
    public IssueCustomField createCustomField(String name, FieldType fieldType, String dataProviderId, boolean mandatory, Collection<IssueProject> projects);
    public IssueCustomField updateCustomField(Long id, String name, FieldType fieldType, String dataProviderId, boolean mandatory, Collection<IssueProject> projects);
    public void deleteCustomField(Long id);
}
