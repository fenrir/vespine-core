package org.fenrir.vespine.core.service;

import java.util.Date;
import java.util.Map;

import javax.script.ScriptException;
import org.fenrir.vespine.core.entity.AlertType;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueNote;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public interface IIssueAdministrationService 
{
    /**
     * Constants amb el valor del resultat de la gestió de les incidències 
     * @see #manageIssue(org.fenrir.vespine.spi.dto.IIssueDTO) 
     */
    public static final int ISSUE_IGNORED = 0;
    public static final int ISSUE_CREATED_OR_UPDATED = 1;
    public static final int ISSUE_DELETED = 2;

    /*---------------------------------*
     *           Incidencies           *
     *---------------------------------*/
    public AbstractIssue createIssue(IssueProject project,
    		IssueCategory category,
    		IssueSeverity severity, 
    		IssueStatus status,
    		String summary, 
    		String description,
    		User reporterUser,
    		User assignedUser,
    		Sprint sprint,
    		Integer estimatedTime,
    		Date slaDate,
    		Map<IssueCustomField, String> customFields,
    		Long auditRegistryReference);
    
    public AbstractIssue updateIssue(Long issueId, IssueProject project,
    		IssueCategory category,
    		IssueSeverity severity, 
    		IssueStatus status,
    		String summary, 
    		String description,
    		User assignedUser,
    		Sprint sprint,
    		Integer estimatedTime,
    		Date slaDate,
    		Date resolutionDate,
    		Map<IssueCustomField, String> customFields,
    		Long auditRegistryReference);

    public void deleteIssue(Long issueId);
    public void deleteIssueRecycleBinAware(Long issueId);
    public void restoreIssueFromRecycleBin(Long issueId);
    public AbstractIssue setIssueSLADate(Long issueId, boolean isSla);
    public AbstractIssue setIssueSLADate(Long issueId, Date slaDate);
    public Date computeIssueSlaDate(Date sendDate, IssueSeverity severity);
    
    /* Repassar deprecats */
    public int manageIssue(IIssueDTO issueDTO);
    public AbstractIssue createOrUpdateIssue(IIssueDTO issueDTO);    

    /*---------------------------------*
     *              Tags               *
     *---------------------------------*/
    public void tagIssue(Long issueId, Long tagId);
    public void dettachIssueTag(Long issueId, Long tagId);
    
    /*---------------------------------*
     *              Notes              *
     *---------------------------------*/
    public IssueNote createIssueNote(AbstractIssue issue, User user, String noteContents);
    public IssueNote updateIssueNote(Long noteId, String noteContents);
    public void deleteIssueNote(Long noteId);

    /*---------------------------------*
     *             Alertes             *
     *---------------------------------*/
    public void createIssueAlerts(AbstractIssue oldIssue, AbstractIssue newIssue) throws BusinessException;
    public void createIssueAlerts(AlertType alertType, AbstractIssue oldIssue, AbstractIssue newIssue) throws BusinessException;
    public void validateIssueAlerts(AlertType alertType);
    public boolean evaluateIssueAlert(AlertType type, AbstractIssue oldIssue, AbstractIssue newIssue) throws ScriptException;
    public void createIssueAlerts(AlertType alertType);
    public void deleteIssueAlert(Long alertId);
}
