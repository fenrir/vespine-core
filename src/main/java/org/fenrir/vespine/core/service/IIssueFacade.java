package org.fenrir.vespine.core.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.fenrir.vespine.core.dto.IIssueAlertDTO;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.dto.SearchQuery;
import org.fenrir.vespine.spi.dto.ICustomValueDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140625
 */
public interface IIssueFacade 
{
	/*---------------------------------*
     *           Incidències           *
     *---------------------------------*/
	public List<IIssueDTO> findAllIssues(int page, int pageSize) throws BusinessException;
	public List<IIssueDTO> findTaggedIssues(String tagId, int page, int pageSize) throws BusinessException;
	public long countTaggedIssues(String tagId) throws BusinessException;
	public Collection<IIssueDTO> findIssueSearchHits(SearchQuery query, int page, int pageSize) throws BusinessException;
	public List<IIssueDTO> findFilteredIssues(IViewFilterDTO filter, int page, int pageSize) throws BusinessException;
	
	public SearchQuery buildIssueTermSearchQuery(String term) throws BusinessException;
	
    public long countIssueSearchHits(SearchQuery query) throws BusinessException;
	public long countIssuesByProject(String projectId) throws BusinessException;
	public long countIssuesByCategory(String categoryId) throws BusinessException;
	public long countIssuesBySeverity(String severityId) throws BusinessException;
	public long countIssuesByStatus(String statusId) throws BusinessException;
	public long countFilteredIssues(IViewFilterDTO filter) throws BusinessException;
	
	public IIssueDTO findIssueById(String id) throws BusinessException;
	public IIssueDTO findIssueByVisibleId(String visibleId) throws BusinessException;
	
	public boolean issueFilterMatches(IViewFilterDTO filter, String issueId) throws BusinessException;
	
	public IIssueDTO createIssue(String projectId,
			String categoryId,
			String severityId, 
			String statusId,
    		String summary, 
    		String description,
    		String assignedUser,
    		String sprintId,
    		Integer estimatedTime,
    		Date slaDate,
    		Map<String, String> customFields) throws BusinessException;
	
	public IIssueDTO updateIssue(String issueId, 
			String projectId,
			String categoryId,
			String severityId, 
			String statusId,
    		String summaryId, 
    		String description,
    		String assignedUser,
    		String sprintId,
    		Integer estimatedTime,
    		Date slaDate,
    		Date resolutionDate,
    		Map<String, String> customFields) throws BusinessException;
	
	public void deleteIssue(String id, boolean bin) throws BusinessException;
	public Date computeSLADate(Date sendDate, String severityId) throws BusinessException;
	public void updateIssueSLADate(String issueId, Date slaDate) throws BusinessException;
	public void updateIssueSLADate(String issueId, boolean isSla) throws BusinessException;
	public void restoreIssue(String id) throws BusinessException;
	
	/*---------------------------------*
     *              Notes              *
     *---------------------------------*/
	public List<IIssueNoteDTO> findIssueNotes(String issueId) throws BusinessException;
	public IIssueNoteDTO findIssueNote(String noteId, String issueId) throws BusinessException;
	public IIssueNoteDTO createIssueNote(String issueId, String noteContents) throws BusinessException;
	public IIssueNoteDTO updateIssueNote(String noteId, String issueId, String noteContents) throws BusinessException;
	public void deleteIssueNote(String issueId, String noteId) throws BusinessException;
	
	/*---------------------------------*
     *              Tags               *
     *---------------------------------*/
	public List<ITagDTO> findIssueTags(String issueId) throws BusinessException;
	public boolean isIssueTagged(String issueId, String tagId) throws BusinessException;
	public void tagIssue(String issueId, String tagId) throws BusinessException;
	public void tagIssueByDescription(String issueId, String tagDescription) throws BusinessException;
	public void dettachIssueTag(String issueId, String tagId) throws BusinessException;
	
	/*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
    public List<ICustomValueDTO> findCustomValuesByField(String fieldId) throws BusinessException;
    public List<ICustomValueDTO> findIssueCustomValues(String issueId) throws BusinessException;
    
    /*---------------------------------*
     *             Alertes             *
     *---------------------------------*/
    public List<IIssueAlertDTO> findAllAlertsByType(String alertTypeId) throws BusinessException;
    public List<IIssueAlertDTO> findIssueAlertsByIssueId(String issueId) throws BusinessException;
    public void createIssueAlerts(String alertTypeId) throws BusinessException;
    public void validateIssueAlerts(String alertTypeId)throws BusinessException;
    public void deleteIssueAlert(String alertId, String issueId) throws BusinessException;
}
