package org.fenrir.vespine.core.service;

import java.util.Collection;
import java.util.List;
import org.fenrir.vespine.core.dto.SearchQuery;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueAlert;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueCustomValue;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.IssueStatus;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140613
 */
public interface IIssueSearchService 
{
    /*---------------------------------*
     *           Incidències           *
     *---------------------------------*/
    public List<AbstractIssue> findAllIssues();
    public List<AbstractIssue> findAllIssues(int page, int pageSize);
    public long countAllIssues();
    public SearchQuery buildIssueTermSearchQuery(String term);
    public Collection<AbstractIssue> findIssueSearchHits(SearchQuery query, int page, int pageSize);
    public long countIssueSearchHits(SearchQuery query);
    public List<AbstractIssue> findTaggedIssues(Long tagId, int page, int pageSize);
    public long countTaggedIssues(Long tagId);
    public boolean isIssueTagged(Long issueId, Long tagId);
    public List<AbstractIssue> findIssuesByProject(IssueProject project);
    public long countIssuesByProject(IssueProject project);
    public List<AbstractIssue> findIssuesByCategory(IssueCategory category);
    public long countIssuesByCategory(IssueCategory category);
    public List<AbstractIssue> findIssuesBySeverity(IssueSeverity severity);
    public long countIssuesBySeverity(IssueSeverity severity);
    public List<AbstractIssue> findIssuesByStatus(IssueStatus status);
    public long countIssuesByStatus(IssueStatus status);
    public AbstractIssue findIssueById(Long id);
    public AbstractIssue findIssueByVisibleId(String visibleId);
    public boolean issueFilterMatches(String filterQuery, Long issueId);
    public long countFilteredIssues(String filterQuery);
        
    public List<AbstractIssue> findFilteredIssues(String filterQuery, String filterOrder, int page, int pageSize);
    
    /**
     * Recupera l'última incidència registrada modificada per projecte
     * @param id Long Id intern del registre corresponent al projecte del que es vol recuperar
     *      l'última incidència modificada
     * @return AbstractIssue Última incidència registrada modificacia al projecte
     */
    public AbstractIssue findLastModifiedIssueByProject(Long id);  

    /*---------------------------------*
     *             Alertes             *
     *---------------------------------*/
    public List<IssueAlert> findAllAlertsByType(Long alertType);
    public List<IssueAlert> findIssueAlertsByIssueId(Long issueId);
    
    /*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
    public List<IssueCustomValue> findCustomValuesByField(Long fieldId);
    public List<IssueCustomValue> findIssueCustomValues(Long issueId);
}
