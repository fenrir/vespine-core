package org.fenrir.vespine.core.service;

import java.util.Date;
import java.util.List;

import org.fenrir.vespine.core.dto.IIssueWipRegistryDTO;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140624
 */
public interface IIssueWorkFacade 
{
	/*---------------------------------*
     *             Sprints             *
     *---------------------------------*/
	public List<ISprintDTO> findAllSprints() throws BusinessException;
	public List<ISprintDTO> findSprintsByProjectNameLike(String name) throws BusinessException;
	public List<ISprintDTO> findSprintsByStartDateLike(String startDatePart) throws BusinessException;
	public List<ISprintDTO> findSprintsByEndDateLike(String endDatePart) throws BusinessException;
	public ISprintDTO findSprintById(String sprintId) throws BusinessException;
	
	public ISprintDTO createSprint(String projectId, Date startDate, Date endDate) throws BusinessException;
	public ISprintDTO updateSprint(String sprintId, String projectId, Date startDate, Date endDate) throws BusinessException;
	public void deleteSprint(String sprintId) throws BusinessException;
	
	/*---------------------------------*
     *      Registres de treball       *
     *---------------------------------*/
	public List<IWorkRegistryDTO> findAllIssueWorkRegistries(String issueId) throws BusinessException;
	public IWorkRegistryDTO findIssueWorkRegistryById(String registryId, String issueId) throws BusinessException;
	public IWorkRegistryDTO createIssueWorkRegistry(String issueId, Date workingDay, boolean includeInSprint, String description, String username, Integer time) throws BusinessException;
	public IWorkRegistryDTO updateIssueWorkRegistry(String registryId, String issueId, Date workingDay, boolean includeInSprint, String description, String username, Integer time) throws BusinessException;
	public void deleteIssueWorkRegistry(String registryId, String issueId) throws BusinessException;
	
	/*---------------------------------*
     *      	Registres W.I.P.       *
     *---------------------------------*/
	public List<IIssueWipRegistryDTO> findIssueWorkInProgressRegistriesByUser(String username) throws BusinessException;
	public IIssueWipRegistryDTO findIssueWorkInProgressRegistry(String issueId, String username) throws BusinessException;
	public IIssueWipRegistryDTO findIssueWorkInProgressRegistryById(String registryId, String issueId) throws BusinessException;
	public IIssueWipRegistryDTO createWorkInProgressRegistry(String issueId, String username) throws BusinessException;
	public void deleteWorkInProgressRegistry(String registryId, String issueId) throws BusinessException;
	public void deleteIssueWorkInProgressRegistry(String issueId, String username) throws BusinessException;
}
