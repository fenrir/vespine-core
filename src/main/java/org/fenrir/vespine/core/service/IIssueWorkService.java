package org.fenrir.vespine.core.service;

import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueWipRegistry;
import org.fenrir.vespine.core.entity.IssueWorkRegistry;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.core.entity.User;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140304
 */
public interface IIssueWorkService 
{
	/*---------------------------------*
     *             Sprints             *
     *---------------------------------*/
	public List<Sprint> findAllSprints();
	public List<Sprint> findSprintsByProjectNameLike(String name);
	public List<Sprint> findSprintsByStartDateLike(String startDatePart);
	public List<Sprint> findSprintsByEndDateLike(String endDatePart);
	public Sprint findSprintById(Long id);
	
	public Sprint createSprint(IssueProject project, Date startDate, Date endDate);
	public Sprint updateSprint(Long id, IssueProject project, Date startDate, Date endDate);
	public void deleteSprint(Long id);
	
	/*---------------------------------*
     *      Registres de treball       *
     *---------------------------------*/
	public List<IssueWorkRegistry> findAllIssueWorkRegistriesByIssue(AbstractIssue issue);
	public IssueWorkRegistry findIssueWorkRegistryById(Long id);
	
	public IssueWorkRegistry createIssueWorkRegistry(AbstractIssue issue, Date workingDay, boolean includeInSprint, String description, User user, Integer time);
	public IssueWorkRegistry updateIssueWorkRegistry(Long id, AbstractIssue issue, Date workingDay, boolean includeInSprint, String description, User user, Integer time);
	public void deleteIssueWorkRegistry(Long id);
	
	/*---------------------------------*
     *       Registres de timer        *
     *---------------------------------*/
	public List<IssueWipRegistry> findIssueWorkInProgressRegistriesByUser(User user);
	public IssueWipRegistry findIssueWorkInProgressRegistry(Long issueId, User user);
	public IssueWipRegistry findIssueWorkInProgressRegistryById(Long registryId);
	public IssueWipRegistry createWorkInProgressRegistry(Long issueId, User user);
	public void deleteWorkInProgressRegistry(Long id);
	public void deleteIssueWorkInProgressRegistry(Long issueId, User user);
}
