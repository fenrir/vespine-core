package org.fenrir.vespine.core.service;

import java.util.List;
import org.fenrir.vespine.core.entity.ProviderCategory;
import org.fenrir.vespine.core.entity.ProviderProject;
import org.fenrir.vespine.core.entity.ProviderSeverity;
import org.fenrir.vespine.core.entity.ProviderStatus;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140302
 */
public interface IProviderAdministrationService 
{
	/*---------------------------------*
     *            Projectes            *
     *---------------------------------*/
    public List<ProviderProject> findAllProviderProjects();
    public List<ProviderProject> findProviderProjectsByProvider(String provider); 
    public ProviderProject findProviderProjectById(Long id);
    public ProviderProject findProviderProjectByProviderData(String provider, String providerId);

    /*---------------------------------*
     *             Estats              *
     *---------------------------------*/
    public List<ProviderStatus> findAllProviderStatus();
    public List<ProviderStatus> findProviderStatusByProvider(String provider); 
    public ProviderStatus findProviderStatusById(Long id);
    public ProviderStatus findProviderStatusByProviderData(String provider, String providerId);
    
    /*---------------------------------*
     *           Severitats            *
     *---------------------------------*/
    public List<ProviderSeverity> findAllProviderSeverities();
    public List<ProviderSeverity> findProviderSeveritiesByProvider(String provider); 
    public ProviderSeverity findProviderSeverityById(Long id);
    public ProviderSeverity findProviderSeverityByProviderData(String provider, String providerId); 
    
    /*---------------------------------*
     *           Categories            *
     *---------------------------------*/
    public List<ProviderCategory> findAllProviderCategories();
    public List<ProviderCategory> findProviderCategoriesByProvider(String provider); 
    public ProviderCategory findProviderCategoryById(Long id);
    public ProviderCategory findProviderCategoryByProviderData(String provider, String providerId);
}
