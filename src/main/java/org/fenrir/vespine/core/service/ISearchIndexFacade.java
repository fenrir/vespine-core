package org.fenrir.vespine.core.service;

import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140624
 */
public interface ISearchIndexFacade 
{
	public void createIndex() throws BusinessException;
	public void enableIndex() throws BusinessException;
	public void disableIndex() throws BusinessException;
}
