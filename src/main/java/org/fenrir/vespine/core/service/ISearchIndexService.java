package org.fenrir.vespine.core.service;

import java.util.Collection;
import org.fenrir.vespine.core.dto.SearchQuery;
import org.fenrir.vespine.core.entity.AbstractIssue;

/**
 * TODO Documentació
 * @author Antonio Archilla Nava
 * @version ISearchIndexService
 */
public interface ISearchIndexService 
{
	public void createIndex();
	public void enableIndex();
	public void disableIndex();
	public void index(Class<?>... types);	
	public void indexObject(Object obj, boolean manageTransaction);
        
        public Collection<AbstractIssue> findIssuesBySearchQuery(SearchQuery query);
        public int countDescriptionsBySearchQuery(SearchQuery query);
}