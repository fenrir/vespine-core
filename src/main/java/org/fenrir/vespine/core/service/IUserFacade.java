package org.fenrir.vespine.core.service;

import java.util.List;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.vespine.spi.dto.IUserDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140504
 */
public interface IUserFacade 
{
	public List<IUserDTO> findAllActiveUsers() throws BusinessException;
	public List<IUserDTO> findUsersCompleteNameLike(String name) throws BusinessException;
	public List<IUserDTO> findUsersUsernameLike(String name) throws BusinessException;
	public IUserDTO findUserByUsername(String username) throws BusinessException;
}
