package org.fenrir.vespine.core.service;

import java.util.List;
import org.fenrir.vespine.core.entity.User;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140504
 */
public interface IUserManagementService 
{
	public List<User> findAllActiveUsers();
	public List<User> findUsersCompleteNameLike(String name);
	public List<User> findUsersUsernameLike(String name);
	public User findUserByUsername(String username);
}
