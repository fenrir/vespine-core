package org.fenrir.vespine.core.service;

import java.util.List;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.Workflow;
import org.fenrir.vespine.core.entity.WorkflowStep;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140106
 */
public interface IWorkflowService 
{
	public List<Workflow> findAllWorkflows();
	public List<IssueProject> findProjectsByWorkflow(Workflow workflow);
	public boolean hasWorkflowRelatedProjects(Workflow workflow);
	
	public List<IssueStatus> findNextWorkflowStatus(IssueProject project, IssueStatus status);
	
	public Workflow findWorkflowById(Long id);
	public WorkflowStep findWorkflowStepById(Long id);
	
	public Workflow createWorkflow(String name, IssueStatus initialStatus, List<WorkflowStep> steps);
	public Workflow updateWorkflow(Long id, String name, IssueStatus initialStatus, List<WorkflowStep> steps);
	public void deleteWorkflow(Long id);
}
