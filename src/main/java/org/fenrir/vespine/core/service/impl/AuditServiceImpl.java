package org.fenrir.vespine.core.service.impl;

import java.util.Date;
import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dao.IAuditRegistryDAO;
import org.fenrir.vespine.core.entity.AuditRegistry;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.IUserManagementService;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140422
 */
public class AuditServiceImpl implements IAuditService 
{
	@Inject
	private IUserManagementService userManagementService;
	
	@Inject
	private IAuditRegistryDAO auditRegistryDAO;
	
	public void setUserManagementService(IUserManagementService userManagementService)
	{
		this.userManagementService = userManagementService;
	}
	
	public void setAuditRegistryDAO(IAuditRegistryDAO auditRegistryDAO)
	{
		this.auditRegistryDAO = auditRegistryDAO;
	}

	@Override
	@Transactional
	public AuditRegistry findAuditRegistryById(Long registryId)
	{
		return auditRegistryDAO.findRegistryById(registryId);
	}
	
	@Override
	@Transactional
	public Long auditAction(String action, String username)
	{
		User user = userManagementService.findUserByUsername(username);
		
		AuditRegistry registry = new AuditRegistry();
		registry.setAction(action);
		registry.setUser(user);
		registry.setAuditDate(new Date());
		
		registry = auditRegistryDAO.createRegistry(registry);
		return registry.getId();
	}
	
	@Override
	@Transactional
	public void auditAction(String action, String objectId, String username, String resultCode, String resultCause)
	{
		User user = userManagementService.findUserByUsername(username);
		
		AuditRegistry registry = new AuditRegistry();
		registry.setAction(action);
		registry.setUser(user);
		registry.setObjectId(objectId);
		registry.setResultCode(resultCode);
		registry.setResultCause(resultCause);
		registry.setAuditDate(new Date());
		
		auditRegistryDAO.createRegistry(registry);
	}

	@Override
	@Transactional
	public void resolveAuditAction(Long registryId, String objectId, String resultCode, String resultCause)
	{
		AuditRegistry registry = auditRegistryDAO.findRegistryById(registryId);
		registry.setObjectId(objectId);
		registry.setResultCode(resultCode);
		registry.setResultCause(resultCause);
		
		auditRegistryDAO.updateRegistry(registry);
	}
}
