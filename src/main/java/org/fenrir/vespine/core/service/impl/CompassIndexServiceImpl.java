package org.fenrir.vespine.core.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Inject;
import org.compass.gps.CompassGps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.dao.IIssueDAO;
import org.fenrir.vespine.core.service.ISearchIndexService;
import org.fenrir.vespine.core.dao.ISearchIndexDAO;
import org.fenrir.vespine.core.dto.SearchQuery;
import org.fenrir.vespine.core.entity.AbstractIssue;

/**
 * TODO Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140103
 */
public class CompassIndexServiceImpl implements ISearchIndexService  
{
    private final Logger log = LoggerFactory.getLogger(CompassIndexServiceImpl.class);
    
    @Inject
    private CompassGps compassGps;
    
    @Inject
    private ISearchIndexDAO indexDAO;
    
    @Inject
    private IIssueDAO issueDAO;
    
    public void setCompassGps(CompassGps compassGps) 
    {
        this.compassGps = compassGps;
    }
    
    public void setIndexDAO(ISearchIndexDAO indexDAO) 
    {
        this.indexDAO = indexDAO;
    }
    
    public void setIssueDAO(IIssueDAO issueDAO)
    {
        this.issueDAO = issueDAO;
    }
	
    @Override
    public void createIndex() 
    {    
        if(!compassGps.isPerformingIndexOperation()){
            compassGps.index();
        }        
    }
	
    @Override
    public void enableIndex() 
    {
        if(!compassGps.isRunning()){
            compassGps.start();
        }
    }
	
    @Override
    public void disableIndex() 
    {
        if(compassGps.isRunning()){
            compassGps.stop();
        }
    }
	
    @Override
    public void index(Class<?>... types) 
    {
        compassGps.index(types);
    }		
    
    @Override
    public void indexObject(Object obj, boolean manageTransaction) 
    {
        indexDAO.indexObject(obj, manageTransaction);		
    }
    
    @Override
    public Collection<AbstractIssue> findIssuesBySearchQuery(SearchQuery query) 
    {
        Collection<AbstractIssue> issues = new ArrayList<AbstractIssue>();
        
        if(log.isDebugEnabled()){
            log.debug("Llançada query a l'índex: {}", query.getIndexQuery());
        }
        
        Collection<AbstractIssue> indexIssues = indexDAO.searchIssues(query.getIndexQuery(), query.getPage(), query.getSize());			
        // S'han de "rehidratar" les entitats perquè no s'han recuperat de la base de dades, sino de l'índex
        for(AbstractIssue indexIssue:indexIssues){
            AbstractIssue issue = issueDAO.findIssueById(indexIssue.getId());
            issues.add(issue);
        }
        
        return issues;
    }
    
    @Override
    public int countDescriptionsBySearchQuery(SearchQuery query) 
    {        
        if(log.isDebugEnabled()){
            log.debug("Llançada query (count) a l'índex: {}", query.getIndexQuery());
        }
        
        return indexDAO.countIssueSearchResults(query.getIndexQuery());
    }
}