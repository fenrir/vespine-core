package org.fenrir.vespine.core.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueAdministrationService;
import org.fenrir.vespine.core.dao.IAlertTypeDAO;
import org.fenrir.vespine.core.dao.IDictionaryDAO;
import org.fenrir.vespine.core.dao.IDictionaryItemDAO;
import org.fenrir.vespine.core.dao.IIssueAlertDAO;
import org.fenrir.vespine.core.dao.IIssueCategoryDAO;
import org.fenrir.vespine.core.dao.IIssueCustomFieldDAO;
import org.fenrir.vespine.core.dao.IIssueCustomValueDAO;
import org.fenrir.vespine.core.dao.IIssueDAO;
import org.fenrir.vespine.core.dao.IIssueProjectDAO;
import org.fenrir.vespine.core.dao.IIssueSeverityDAO;
import org.fenrir.vespine.core.dao.IIssueStatusDAO;
import org.fenrir.vespine.core.dao.IIssueViewFilterDAO;
import org.fenrir.vespine.core.dao.ITagDAO;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.AlertType;
import org.fenrir.vespine.core.entity.Dictionary;
import org.fenrir.vespine.core.entity.DictionaryItem;
import org.fenrir.vespine.core.entity.FieldType;
import org.fenrir.vespine.core.entity.IssueAlert;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueCustomValue;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.IssueViewFilter;
import org.fenrir.vespine.core.entity.ProviderCategory;
import org.fenrir.vespine.core.entity.ProviderProject;
import org.fenrir.vespine.core.entity.ProviderSeverity;
import org.fenrir.vespine.core.entity.ProviderStatus;
import org.fenrir.vespine.core.entity.Tag;
import org.fenrir.vespine.core.entity.Workflow;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.1.20140829
 */
public class CoreAdministrationServiceImpl implements ICoreAdministrationService 
{
	private final Logger log = LoggerFactory.getLogger(CoreAdministrationServiceImpl.class);

	@Inject
	private IIssueAdministrationService issueAdministrationService;
	
	@Inject
    private IIssueProjectDAO projectDAO;
    
    @Inject 
    private IIssueCategoryDAO categoryDAO;

    @Inject
    private IIssueSeverityDAO severityDAO;

    @Inject
    private IIssueStatusDAO statusDAO;
    
    @Inject
    private IIssueDAO issueDAO;
    
    @Inject
    private IIssueViewFilterDAO viewFilterDAO;
    
    @Inject
    private ITagDAO tagDAO;
    
    @Inject
    private IAlertTypeDAO alertTypeDAO;
    
    @Inject
    private IIssueAlertDAO alertDAO;
    
    @Inject
    private IIssueCustomFieldDAO customFieldDAO;
    
    @Inject
    private IIssueCustomValueDAO customValueDAO;
    
    @Inject
    private IDictionaryDAO dictionaryDAO;
    
    @Inject
    private IDictionaryItemDAO dictionaryItemDAO;
    
    public void setIssueAdministrationService(IIssueAdministrationService issueAdministrationService)
    {
    	this.issueAdministrationService = issueAdministrationService;
    }
    
    public void setProjectDAO(IIssueProjectDAO projectDAO)
    {
        this.projectDAO = projectDAO;
    }
    
    public void setCategoryDAO(IIssueCategoryDAO categoryDAO)
    {
        this.categoryDAO = categoryDAO;
    }
    
    public void setSeverityDAO(IIssueSeverityDAO severityDAO)
    {
        this.severityDAO = severityDAO;
    }

    public void setStatusDAO(IIssueStatusDAO statusDAO)
    {
        this.statusDAO = statusDAO;
    }
    
    public void setIssueDAO(IIssueDAO issueDAO)
    {
        this.issueDAO = issueDAO;
    }
    
    public void setTagDAO(ITagDAO tagDAO)
    {
        this.tagDAO = tagDAO;
    }
    
    public void setViewFilterDAO(IIssueViewFilterDAO viewFilterDAO)
    {
        this.viewFilterDAO = viewFilterDAO;
    }
    
    public void setAlertTypeDAO(IAlertTypeDAO alertTypeDAO)
    {
        this.alertTypeDAO = alertTypeDAO;
    }
    
    public void setAlertDAO(IIssueAlertDAO alertDAO)
    {
        this.alertDAO = alertDAO;
    }
    
    public void setCustomFieldDAO(IIssueCustomFieldDAO customFieldDAO)
    {
    	this.customFieldDAO = customFieldDAO;
    }
    
    public void setCustomValueDAO(IIssueCustomValueDAO customValueDAO)
    {
    	this.customValueDAO = customValueDAO;
    }
    
    public void setDictionaryDAO(IDictionaryDAO dictionaryDAO)
    {
    	this.dictionaryDAO = dictionaryDAO;
    }
    
    public void setDictionaryItemDAO(IDictionaryItemDAO dictionaryItemDAO)
    {
    	this.dictionaryItemDAO = dictionaryItemDAO;
    }
	
	/*---------------------------------*
     *           Projectes             *
     *---------------------------------*/
    @Override
    @Transactional
    public List<IssueProject> findAllProjects()
    {
        return projectDAO.findAllProjects();
    }            
    
    @Override
    @Transactional
    public IssueProject findProjectById(Long id)
    {
        return projectDAO.findProjectById(id);        
    }
    
    @Transactional
    @Override
    public boolean isInitialUpdateNeeded()
    {
    	return projectDAO.findAllProjects().isEmpty();
    }
    
    @Transactional
    @Override
    public IssueProject createIssueProject(String name, String abbreviation, Workflow workflow, Set<ProviderProject> providerProjects)
    {
    	IssueProject project = new IssueProject();
    	project.setName(name);
    	project.setAbbreviation(abbreviation);
    	project.setWorkflow(workflow);
    	project.setProviderProjects(providerProjects);
    	return projectDAO.createProject(project);
    }
    
    @Transactional
    @Override
    public IssueProject updateIssueProject(Long id, String name, Workflow workflow, Set<ProviderProject> providerProjects)
    {
    	// El camp 'Abbreviation' no es pot modificar
    	IssueProject project = projectDAO.findProjectById(id);
    	project.setName(name);
    	project.setWorkflow(workflow);
    	project.setProviderProjects(providerProjects);
    	return projectDAO.updateProject(project);
    }
    
    @Transactional
    @Override
    public void deleteIssueProject(Long id)
    {
    	IssueProject project = projectDAO.findProjectById(id);
    	long issues = issueDAO.countIssuesByProject(project);
    	if(issues>0){
    		throw new IllegalArgumentException("No es pot eliminar el projecte perquè té incidencies relacionades");
    	}
    	// S'actualitzen els camps custom que tenien relacionats el projecte
    	List<IssueCustomField> fields = customFieldDAO.findCustomFieldsByProject(project);
    	for(IssueCustomField field:fields){
    		field.getProjects().remove(project);
    		customFieldDAO.updateCustomField(field);
    	}
    	projectDAO.deleteProject(project);
    }
    
    
    /*---------------------------------*
     *           Categories            *
     *---------------------------------*/
    @Override
    @Transactional
    public List<IssueCategory> findAllCategories()
    {
    	return categoryDAO.findAllCategories();
    }
    
    @Override
    @Transactional
    public IssueCategory findCategoryById(Long id)
    {
    	return categoryDAO.findCategoryById(id);
    }
    
    @Transactional
    @Override
    public IssueCategory createIssueCategory(String name, Set<ProviderCategory> providerCategories)
    {
    	IssueCategory category = new IssueCategory();
    	category.setName(name);
    	category.setProviderCategories(providerCategories);
    	return categoryDAO.createCategory(category);
    }
    
    @Transactional
    @Override
    public IssueCategory updateIssueCategory(Long id, String name, Set<ProviderCategory> providerCategories)
    {
    	IssueCategory category = categoryDAO.findCategoryById(id);
    	category.setName(name);
    	category.setProviderCategories(providerCategories);
    	return categoryDAO.updateCategory(category);
    }
    
    @Transactional
    @Override
    public void deleteIssueCategory(Long id)
    {
    	IssueCategory category = categoryDAO.findCategoryById(id);
    	long issues = issueDAO.countIssuesByCategory(category);
    	if(issues>0){
    		throw new IllegalArgumentException("No es pot eliminar la categoria perquè té incidencies relacionades");
    	}
    	categoryDAO.deleteCategory(category);
    }

    /*---------------------------------*
     *           Severitats            *
     *---------------------------------*/
    @Override
    @Transactional
    public List<IssueSeverity> findAllSeverities()
    {
        return severityDAO.findAllSeverities();
    }
    
    @Override
    @Transactional
    public IssueSeverity findSeverityById(Long id)
    {
    	return severityDAO.findSeverityById(id);
    }
    
    @Transactional
    @Override
    public IssueSeverity createIssueSeverity(String name, String slaPattern, Set<ProviderSeverity> providerSeverities)
    {
    	IssueSeverity severity = new IssueSeverity();
    	severity.setName(name);
    	severity.setSlaPattern(slaPattern);
    	severity.setProviderSeverities(providerSeverities);
    	return severityDAO.createSeverity(severity);
    }
    
    @Transactional
    @Override
    public IssueSeverity updateIssueSeverity(Long id, String name, String slaPattern, Set<ProviderSeverity> providerSeverities)
    {
    	IssueSeverity severity = severityDAO.findSeverityById(id);
    	severity.setName(name);
    	severity.setSlaPattern(slaPattern);
    	severity.setProviderSeverities(providerSeverities);
    	return severityDAO.updateSeverity(severity);
    }
    
    @Transactional
    @Override
    public void deleteIssueSeverity(Long id)
    {
    	IssueSeverity severity = severityDAO.findSeverityById(id);
    	long issues = issueDAO.countIssuesBySeverity(severity);
    	if(issues>0){
    		throw new IllegalArgumentException("No es pot eliminar la severitat perquè té incidencies relacionades");
    	}
    	severityDAO.deleteSeverity(severity);
    }
    
    /*---------------------------------*
     *             Estats              *
     *---------------------------------*/
    @Override
    @Transactional
    public List<IssueStatus> findAllStatus()
    {
        return statusDAO.findAllStatus();
    }

    @Override
    @Transactional
    public IssueStatus findStatusById(Long id)
    {
        return statusDAO.findStatusById(id);
    }
    
    @Transactional
    @Override
    public IssueStatus createIssueStatus(String name, String color, Set<ProviderStatus> providerStatus)
    {
    	IssueStatus status = new IssueStatus();
    	status.setName(name);
    	status.setColor(color);
    	status.setProviderStatus(providerStatus);
    	return statusDAO.createStatus(status);
    }
    
    @Transactional
    @Override
    public IssueStatus updateIssueStatus(Long id, String name, String color, Set<ProviderStatus> providerStatus)
    {
    	IssueStatus status = statusDAO.findStatusById(id);
    	status.setName(name);
    	status.setColor(color);
    	status.setProviderStatus(providerStatus);
    	return statusDAO.updateStatus(status);
    }
    
    @Transactional
    @Override
    public void deleteIssueStatus(Long id)
    {
    	IssueStatus status = statusDAO.findStatusById(id);
    	long issues = issueDAO.countIssuesByStatus(status);
    	if(issues>0){
    		throw new IllegalArgumentException("No es pot eliminar l'estat perquè té incidencies relacionades");
    	}
    	statusDAO.deleteStatus(status);
    }
    
    /*---------------------------------*
     *          Filtres vistes         *
     *---------------------------------*/
    @Override
    @Transactional
    public List<IssueViewFilter> findApplicationViewFilters()
    {
        return viewFilterDAO.findViewFilters(Boolean.TRUE);
    }
    
    @Override
    @Transactional
    public List<IssueViewFilter> findUserViewFilters()
    {
        return viewFilterDAO.findViewFilters(Boolean.FALSE);
    }

    @Override
    @Transactional
    public IssueViewFilter findViewFilterById(Long id)
    {
        return viewFilterDAO.findViewFilterById(id);
    }
    
    @Override
    @Transactional
    public IssueViewFilter createIssueViewFilter(String name, String description, String filterQuery, String orderByClause)
    {
        IssueViewFilter viewFilter = new IssueViewFilter();
        viewFilter.setName(name);
        viewFilter.setDescription(description);
        viewFilter.setFilterQuery(filterQuery);
        viewFilter.setOrderByClause(orderByClause);
        viewFilter.setApplicationFilter(Boolean.FALSE);
        // Només es poden ordenar els filtres d'usuari
        long filterOrder = viewFilterDAO.countViewFilters(Boolean.FALSE);
        viewFilter.setFilterOrder(filterOrder);
        return viewFilterDAO.createViewFilter(viewFilter);
    }

    @Override
    @Transactional
    public IssueViewFilter updateIssueViewFilter(Long id, String name, String description, String filterQuery, String orderByClause)
    {
        IssueViewFilter viewFilter = new IssueViewFilter();
        viewFilter.setId(id);
        viewFilter.setName(name);
        viewFilter.setDescription(description);
        viewFilter.setFilterQuery(filterQuery);
        viewFilter.setOrderByClause(orderByClause);
        viewFilter.setApplicationFilter(Boolean.FALSE);
        long filterCount = viewFilterDAO.countViewFilters(Boolean.FALSE);
        viewFilter.setFilterOrder(filterCount);
        return viewFilterDAO.updateViewFilter(viewFilter);
    }
    
    @Override
    @Transactional
    public void reorderIssueViewFilter(Long id, int order)
    {
        List<IssueViewFilter> filters = viewFilterDAO.findViewFilters(Boolean.FALSE);    
        IssueViewFilter filter = viewFilterDAO.findViewFilterById(id);
        filters.remove(filter);
        filter.setFilterOrder(new Long(order));
        filters.add(order, filter);
        for(int i=0; i<filters.size(); i++){
            filter = filters.get(i);
            if(filter.getFilterOrder()==null || filter.getFilterOrder()!=i){
                filter.setFilterOrder(new Long(i));
                viewFilterDAO.updateViewFilter(filter);
            }
        }
    }

    @Override
    @Transactional
    public void deleteIssueViewFilter(Long id)
    {
        IssueViewFilter viewFilter = viewFilterDAO.findViewFilterById(id);
        viewFilterDAO.deleteViewFilter(viewFilter);
        // Es força la reordenació dels filtres
        List<IssueViewFilter> filters = viewFilterDAO.findViewFilters(Boolean.FALSE);
        if(!filters.isEmpty()){
            reorderIssueViewFilter(filters.get(0).getId(), 0);
        }
    }
    
    /*---------------------------------*
     *              Tags               *
     *---------------------------------*/
    @Override
    @Transactional
    public List<Tag> findAllTags()
    {
        return tagDAO.findAllTags();
    }
    
    @Override
    @Transactional
    public List<Tag> findPreferredTags()
    {
        return tagDAO.findPreferredTags(true);
    }

    @Override
    @Transactional
    public List<Tag> findTagsFiltered(String prefix)
    {
        return tagDAO.findTagsNameLike(prefix);
    }
    
    @Override
    @Transactional
    public List<Tag> findTagsByName(String name)
    {
        return tagDAO.findTagsByName(name);
    }    
    
    @Override
    @Transactional
    public List<Tag> findPreferredTagsFiltered(String prefix)
    {
        return tagDAO.findPreferredTagsNameLike(prefix);
    }
    
    @Override
    @Transactional
    public Tag findTagById(Long id)
    {
        return tagDAO.findTagById(id);
    }
    
    @Override
    @Transactional
    public Tag createTag(String name, Boolean preferred)
    {
        Tag tag = new Tag();
        tag.setName(name);
        tag.setPreferred(preferred);
        return tagDAO.createTag(tag);
    }
    
    @Override
    @Transactional
    public Tag updateTag(Long id, String name, Boolean preferred)
    {
        Tag tag = tagDAO.findTagById(id);        
        tag.setName(name);
        tag.setPreferred(preferred);
        return tagDAO.updateTag(tag);
    }
    
    @Override
    @Transactional
    public void deleteTag(Long id)
    {
        Tag tag = tagDAO.findTagById(id);
        tagDAO.deleteTag(tag);
    }
    
    /*---------------------------------*
     *          Tipus d'alerta         *
     *---------------------------------*/
    @Override
    @Transactional
    public List<AlertType> findAllAlertTypes()
    {
        return alertTypeDAO.findAllAlertTypes();
    }
    
    @Override
    @Transactional
    public AlertType findAlertTypeById(Long alertTypeId)
    {
    	return alertTypeDAO.findAlertTypeById(alertTypeId);
    }
    
    @Override
    @Transactional
    public AlertType createAlertType(String name, String description, String icon, String alertRule, boolean generateOld) throws BusinessException
    {
        AlertType alertType = new AlertType();
        alertType.setName(name);
        alertType.setDescription(description);
        alertType.setIcon(icon);
        alertType.setAlertRule(alertRule);
        alertType = alertTypeDAO.createAlertType(alertType);
        
        // Si s'ha indicat, es generen les alertes necessaries per les incidències ja registrades
        if(generateOld){
            if(log.isDebugEnabled()){
                log.debug("Generant alertes de tipus {} per les incidències ja registrades", alertType.getName());
            }
            List<AbstractIssue> issues = issueDAO.findAllIssues();
            for(AbstractIssue issue:issues){
                issueAdministrationService.createIssueAlerts(alertType, issue, issue);
            }
        }

        return alertType;
    }

    @Override
    @Transactional
    public AlertType updateAlertType(Long id, String name, String description, String icon, String alertRule)
    {
        AlertType alertType = new AlertType();
        alertType.setId(id);
        alertType.setName(name);
        alertType.setDescription(description);
        alertType.setIcon(icon);
        alertType.setAlertRule(alertRule);

        return alertTypeDAO.updateAlertType(alertType);
    }    
    
    @Override
    @Transactional
    public void deleteAlertType(Long id)
    {
        AlertType alertType = alertTypeDAO.findAlertTypeById(id);
        // S'esborren les alertes del tipus sel.leccionat
        List<IssueAlert> issueAlerts = alertDAO.findAlertsByType(alertType);
        for(IssueAlert alert:issueAlerts){
            alertDAO.deleteAlert(alert);
        }
        // S'esborra el registre del tipus d'alerta
        alertTypeDAO.deleteAlertType(alertType);
    }
    
    /*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
    @Override
    @Transactional
    public List<IssueCustomField> findAllCustomFields()
    {
    	return customFieldDAO.findAllCustomFields();
    }
    
    @Override
    @Transactional
    public List<IssueCustomField> findCustomFieldsByProject(IssueProject project)
    {
    	return customFieldDAO.findCustomFieldsByProject(project);
    }
    
    @Override
    @Transactional
    public IssueCustomField findCustomFieldById(Long fieldId)
    {
    	return customFieldDAO.findCustomFieldById(fieldId);
    }
    
    @Override
    @Transactional
    public IssueCustomField createCustomField(String name, FieldType fieldType, String dataProviderId, boolean mandatory, Collection<IssueProject> projects)
    {
    	IssueCustomField field = new IssueCustomField();
    	field.setName(name);
    	field.setFieldType(fieldType);
    	field.setProjects(projects);
    	field.setMandatory(mandatory);
    	if(fieldType.needDataProviderId()){
    		field.setDataProviderId(dataProviderId);
    	}
    	return customFieldDAO.createCustomField(field);    	
    }
    
    @Override
    @Transactional
    public IssueCustomField updateCustomField(Long id, String name, FieldType fieldType, String dataProviderId, boolean mandatory, Collection<IssueProject> projects)
    {
    	IssueCustomField field = customFieldDAO.findCustomFieldById(id);
    	// En el cas que hagi canviat el tipus de dada del camp, s'esborren els valors associats
    	if(!field.getFieldType().equals(fieldType)
    			|| fieldType.needDataProviderId() && !dataProviderId.equals(field.getDataProviderId())){
    		List<IssueCustomValue> values = customValueDAO.findCustomValues(field);
    		for(IssueCustomValue value:values){
    			customValueDAO.deleteCustomValue(value);
    		}
    	}
    	// S'especifica el valor de la resta de camps
    	field.setName(name);
    	field.setFieldType(fieldType);
    	field.setProjects(projects);
    	field.setMandatory(mandatory);
    	if(fieldType.needDataProviderId()){
    		field.setDataProviderId(dataProviderId);
    	}
    	
    	return customFieldDAO.updateCustomField(field);    	
    }
    
    @Override
    @Transactional
    public void deleteCustomField(Long id)
    {
    	IssueCustomField field = customFieldDAO.findCustomFieldById(id);
    	List<IssueCustomValue> values = customValueDAO.findCustomValues(field);
    	for(IssueCustomValue value:values){
    		customValueDAO.deleteCustomValue(value);
    	}
    	customFieldDAO.deleteCustomField(field);
    }
    
    /*---------------------------------*
     *           Diccionaris           *
     *---------------------------------*/
    @Override
    @Transactional
    public List<Dictionary> findAllDictionaries()
    {
    	return dictionaryDAO.findAllDictionaries();
    }

    @Override
    @Transactional
    public Dictionary findDictionaryById(Long dictionaryId)
    {
    	return dictionaryDAO.findDictionaryById(dictionaryId);
    }
    
    @Override
    @Transactional
    public List<DictionaryItem> findDictionaryItems(Long dictionaryId)
    {
    	Dictionary dictionary = dictionaryDAO.findDictionaryById(dictionaryId);
    	return dictionary.getItems();
    }
    
    @Override
    @Transactional
    public DictionaryItem findDictionaryItem(Long itemId)
    {
    	return dictionaryItemDAO.findItemById(itemId);
    }
    
    @Override
    @Transactional
    public Dictionary createDictionary(String name, String description, List<DictionaryItem> items)
    {
    	Dictionary dictionary = new Dictionary();
    	dictionary.setName(name);
    	dictionary.setDescription(description);
    	dictionary = dictionaryDAO.createDictionary(dictionary);    	
    	// Items
    	List<DictionaryItem> persistedItems = new ArrayList<DictionaryItem>();
    	for(DictionaryItem item:items){
    		item.setDictionary(dictionary);
    		persistedItems.add(dictionaryItemDAO.createItem(item));
    	}
    	// S'ha retornar amb els items carregats
    	dictionary.setItems(persistedItems);
    	return dictionary;
    }
    
    @Override
    @Transactional
    public Dictionary updateDictionary(Long id, String name, String description, List<DictionaryItem> items)
    {
    	Dictionary dictionary = dictionaryDAO.findDictionaryById(id);
    	dictionary.setId(id);
    	dictionary.setName(name);
    	dictionary.setDescription(description);
    	// Actualització dels items
    	List<DictionaryItem> currentItems = dictionary.getItems();
    	// Esborrar
    	Iterator<DictionaryItem> iterator = currentItems.iterator();
    	while(iterator.hasNext()){
    		DictionaryItem item = iterator.next();
    		// equals per ID
    		if(!items.contains(item)){
    			iterator.remove();
    		}
    	}
    	for(DictionaryItem item:items){
    		// Crear    		
    		if(item.getId()==null){
    			item.setDictionary(dictionary);
    			DictionaryItem createdItem = dictionaryItemDAO.createItem(item);
    			currentItems.add(createdItem);
    		}
    		// Actualitzar
    		else{
    			DictionaryItem updatedItem = dictionaryItemDAO.updateItem(item);
    			currentItems.remove(item);
    			currentItems.add(updatedItem);
    		}
    	}
    	dictionary = dictionaryDAO.updateDictionary(dictionary);
    	
    	return dictionary;
    }
    
    @Override
    @Transactional
    public void deleteDictionary(Long id)
    {
    	Dictionary dictionary = dictionaryDAO.findDictionaryById(id);
    	dictionaryDAO.deleteDictionary(dictionary);
    }
}
