package org.fenrir.vespine.core.service.impl;

import java.text.MessageFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Date;
import javax.inject.Inject;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.Compilable;
import javax.script.Bindings;
import javax.script.ScriptException;
import com.google.inject.persist.Transactional;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.dao.IIssueDAO;
import org.fenrir.vespine.core.service.IIssueAdministrationService;
import org.fenrir.vespine.core.dao.IIssueAlertDAO;
import org.fenrir.vespine.core.dao.IAlertTypeDAO;
import org.fenrir.vespine.core.dao.IIssueCustomValueDAO;
import org.fenrir.vespine.core.dao.IIssueNoteDAO;
import org.fenrir.vespine.core.dao.ISequenceValueDAO;
import org.fenrir.vespine.core.dao.ITagDAO;
import org.fenrir.vespine.core.entity.AlertType;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.DeletedIssue;
import org.fenrir.vespine.core.entity.Issue;
import org.fenrir.vespine.core.entity.IssueAlert;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueCustomValue;
import org.fenrir.vespine.core.entity.IssueNote;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.SequenceValue;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.core.entity.Tag;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.1.20140811
 */
public class IssueAdministrationServiceImpl implements IIssueAdministrationService
{
    private final Logger log = LoggerFactory.getLogger(IssueAdministrationServiceImpl.class);
    
    @Inject
    private IIssueDAO issueDAO;

    @Inject
    private ITagDAO tagDAO;
    
    @Inject
    private IIssueNoteDAO noteDAO;

    @Inject
    private IIssueAlertDAO alertDAO;
    
    @Inject
    private IAlertTypeDAO alertTypeDAO;
    
    @Inject
    private IIssueCustomValueDAO customValueDAO;
    
    @Inject
    private ISequenceValueDAO sequenceValueDAO;

    private Map<AlertType, CompiledScript> alertScripts = new HashMap<AlertType, CompiledScript>();
    private ScriptEngine engine;

    public void setIssueDAO(IIssueDAO issueDAO)
    {
        this.issueDAO = issueDAO;
    }

    public void setTagDAO(ITagDAO tagDAO)
    {
        this.tagDAO = tagDAO;
    }

    public void setNoteDAO(IIssueNoteDAO noteDAO)
    {
        this.noteDAO = noteDAO;
    }
    
    public void setAlertDAO(IIssueAlertDAO alertDAO)
    {
        this.alertDAO = alertDAO;
    }
    
    public void setAlertTypeDAO(IAlertTypeDAO alertTypeDAO)
    {
        this.alertTypeDAO = alertTypeDAO;
    }
    
    public void setCustomValueDAO(IIssueCustomValueDAO customValueDAO)
    {
    	this.customValueDAO = customValueDAO;
    }
    
    public void setSequenceValueDAO(ISequenceValueDAO sequenceValueDAO)
    {
    	this.sequenceValueDAO = sequenceValueDAO;
    }
    
    /*---------------------------------*
     *           Incidencies           *
     *---------------------------------*/
    @Transactional
    @Override
    public AbstractIssue createIssue(IssueProject project,
    		IssueCategory category,
    		IssueSeverity severity, 
    		IssueStatus status,
    		String summary, 
    		String description,
    		User reporterUser,
    		User assignedUser,
    		Sprint sprint,
    		Integer estimatedTime,
    		Date slaDate,
    		Map<IssueCustomField, String> customFields,
    		Long auditRegistryReference)
    {
    	AbstractIssue issue = new Issue();

    	issue.setProject(project);
    	issue.setCategory(category);
    	issue.setStatus(status);
    	issue.setSeverity(severity);
    	
    	issue.setSummary(summary);
        issue.setDescription(description);
        
        issue.setSendDate(new Date());
        issue.setSlaDateModified(Boolean.FALSE);
        /* Es recalcula la data SLA per les SLAs ja gestionades si se li ha canviat la severitat. 
         * En el cas que s'hagi canviat la data manualment, no es recalcularà
         */
        Date finalSlaDate = slaDate;
        if(slaDate!=null){
        	finalSlaDate = computeIssueSlaDate(issue.getSendDate(), severity);
        	issue.setSlaDateModified(!DateUtils.isSameDay(slaDate, finalSlaDate));
        }
        issue.setSlaDate(finalSlaDate);
        issue.setModifiedDate(new Date());
        issue.setSprint(sprint);
        issue.setEstimatedTime(estimatedTime);
        
        issue.setReporter(reporterUser);
        issue.setAssignedUser(assignedUser);
        
        issue.setAuditRecordReference(auditRegistryReference);
        
        String sequenceName = MessageFormat.format(IssueProject.PROJECT_ABBREVIATION_SEQUENCE_PATTERN, project.getId());
        SequenceValue sequenceValue = new SequenceValue(sequenceName);
        sequenceValueDAO.createValue(sequenceValue);
        String visibleId = MessageFormat.format(AbstractIssue.VISIBLE_ID_PATTERN, project.getAbbreviation(), sequenceValue.getValue());
        issue.setVisibleId(visibleId);
        // Una vegada s'ha fet servir el valor de la sequence, s'elimina
        sequenceValueDAO.deleteValue(sequenceValue);
        
        issue = issueDAO.createIssue(issue);
        
        // Es creen els camps custom
        for(IssueCustomField customField:customFields.keySet()){
        	String value = customFields.get(customField);
        	if(value!=null){
	        	IssueCustomValue customValue = new IssueCustomValue();
	        	customValue.setIssue(issue);
	        	customValue.setField(customField);
	        	customValue.setAuditRecordReference(auditRegistryReference);
	        	customValue.setValue(value);
	        	customValueDAO.createCustomValue(customValue);
        	}
        }
        
        // Es creen les alertes associades si fa falta
        try{
            createIssueAlerts(null, issue);
        }
        catch(BusinessException e){
            // No importa que falli la creació de l'alerta. El registre ha estat creat
            log.error("Error en registrar alertes per la incidència {}: {}", new Object[]{issue, e.getMessage(), e});
        }
        
        return issue;
    }
    
    @Transactional
    @Override
    public AbstractIssue updateIssue(Long issueId,
    		IssueProject project,
    		IssueCategory category,
    		IssueSeverity severity, 
    		IssueStatus status,
    		String summary, 
    		String description,
    		User assignedUser,
    		Sprint sprint,
    		Integer estimatedTime,
    		Date slaDate,
    		Date resolutionDate,
    		Map<IssueCustomField, String> issueCustomFields,
    		Long auditRegistryReference)
    {
    	AbstractIssue issue = issueDAO.findIssueById(issueId);
    	// Copia per després poder comparar a la creació d'alertes
    	AbstractIssue originalIssue = null;
    	if(issue.isDeleted()){
    		originalIssue = new DeletedIssue(issue);
    	}
    	else{
    		originalIssue = new Issue(issue);
    	}
    	originalIssue.setTags(issue.getTags());
    	originalIssue.setNotes(issue.getNotes());

    	/* S'actualitzen les dades de l'incidencia */
    	// Dades relacionades
    	issue.setProject(project);
    	issue.setCategory(category);
    	issue.setStatus(status);
    	issue.setSeverity(severity);
    	// Descripcions i altres dades
    	issue.setSummary(summary);
        issue.setDescription(description);
        
        issue.setSprint(sprint);
        issue.setEstimatedTime(estimatedTime);
        issue.setAssignedUser(assignedUser);
        
        issue.setAuditRecordReference(auditRegistryReference);
        
        // Dates
        issue.setSlaDateModified(Boolean.FALSE);
        /* Es recalcula la data SLA per les SLAs ja gestionades si se li ha canviat la severitat. 
         * En el cas que s'hagi canviat la data manualment, no es recalcularà
         */
        Date finalSlaDate = slaDate;
        if(slaDate!=null){
        	finalSlaDate = computeIssueSlaDate(issue.getSendDate(), severity);
        	issue.setSlaDateModified(!DateUtils.isSameDay(slaDate, finalSlaDate));        	
        }
        issue.setSlaDate(finalSlaDate);
        issue.setResolutionDate(resolutionDate);
        issue.setModifiedDate(new Date());

        issue = issueDAO.updateIssue(issue);
        
        // S'actualitzen els camps custom
        List<IssueCustomValue> issueCustomValues = customValueDAO.findIssueCustomValues(issue);
        for(IssueCustomValue elem:issueCustomValues){
        	// Esborrar eliminats
        	if(!issueCustomFields.containsKey(elem.getField())){
        		issueCustomFields.remove(elem.getField());
        		customValueDAO.deleteCustomValue(elem);
        	}
        	// Actualitzar existents
        	else{
        		String objValue = issueCustomFields.remove(elem.getField());
        		elem.setValue(objValue);
        		elem.setAuditRecordReference(auditRegistryReference);
            	customValueDAO.updateCustomValue(elem);
        	}
        }
        // Crear nous valors
        for(IssueCustomField customField:issueCustomFields.keySet()){
        	String value = issueCustomFields.get(customField);
        	if(value!=null){
	        	IssueCustomValue customValue = new IssueCustomValue();
	        	customValue.setIssue(issue);
	        	customValue.setField(customField);
	        	customValue.setValue(value);
	        	customValue.setAuditRecordReference(auditRegistryReference);
	        	customValueDAO.createCustomValue(customValue);
        	}
        }        
        
        // Es creen les alertes associades si fa falta
        try{
            createIssueAlerts(originalIssue, issue);
        }
        catch(BusinessException e){
            // No importa que falli la creació de l'alerta. El registre ha estat creat
            log.error("Error en registrar alertes per la incidència {}: {}", new Object[]{issue, e.getMessage(), e});
        }
        
        return issue;
    }
    
    /**
     * Mètode que esborra definitivament una incidència. També esborra les alertes associades
     * i actualitza els tags relacionats per tal d'eliminar-ne la referència. 
     * Les notes associades seràn esborrades a través de la operació Cascade.REMOVE
     * @param id {@link Long} - ID de la incidència que es vol esborrar
     */
    @Transactional
    @Override
    public void deleteIssue(Long id)
    {
        AbstractIssue issue = issueDAO.findIssueById(id);

        // S'esborren les alertes associades
        List<IssueAlert> issueAlerts = alertDAO.findIssueAlerts(issue);
        for(IssueAlert alert:issueAlerts){
            alertDAO.deleteAlert(alert);
        }        
        // S'esborren les associacions amb els tags
        List<Tag> tags = tagDAO.findIssueTags(issue);
        for(Tag tag:tags){
            tag.getIssues().remove(issue);
            tagDAO.updateTag(tag);
        }
        // S'esborren els camps custom
        List<IssueCustomValue> customFields = customValueDAO.findIssueCustomValues(issue);
        for(IssueCustomValue elem:customFields){
        	customValueDAO.deleteCustomValue(elem);
        }
        // S'esborra el registre de la incidència
        issueDAO.deleteIssue(issue);
    }
    
    @Transactional
    @Override
    public void deleteIssueRecycleBinAware(Long issueId)
    {
        AbstractIssue issue = issueDAO.findIssueById(issueId);
        if(issue.isDeleted()){
            deleteIssue(issue.getId());
        }
        else{
            swapIssue(issue);
        }
    }
    
    @Transactional
    @Override
    public void restoreIssueFromRecycleBin(Long issueId)
    {
        AbstractIssue issue = issueDAO.findIssueById(issueId);
        swapIssue(issue);
    }
    
    @Transactional
    private void swapIssue(AbstractIssue issue)
    {
        AbstractIssue newIssue;
        if(issue.isDeleted()){
            newIssue = new Issue(issue);
        }
        else{
            newIssue = new DeletedIssue(issue);            
        }
        // No es registraran els canvis en els camps de la nova instancia per ser un duplicat de l'original
        newIssue.setInstanceTracked(false);
                
        List<IssueAlert> alerts = alertDAO.findIssueAlerts(issue);
        List<Tag> tags = tagDAO.findIssueTags(issue);        
        Set<IssueNote> notes = issue.getNotes();
        List<IssueCustomValue> customValues = customValueDAO.findIssueCustomValues(issue);
        
        deleteIssue(issue.getId());        
        newIssue = issueDAO.createIssue(newIssue);
        // Es tornen a crear les alertes
        for(IssueAlert alert:alerts){
            IssueAlert newAlert = new IssueAlert();
            newAlert.setType(alert.getType());
            newAlert.setIssue(newIssue);
            newAlert.setCreationDate(alert.getCreationDate());
            
            alertDAO.createAlert(newAlert);
        }        
        // Es tornen a crear els tags
        for(Tag tag:tags){
            tag.getIssues().add(newIssue);
            tagDAO.updateTag(tag);
        }        
        // Es tornen a crear les notes
        for(IssueNote note:notes){
            note.setId(null);
            note.setIssue(newIssue);
            noteDAO.createNote(note);
        }
        // Es tornen a crear els valors custom
        for(IssueCustomValue value:customValues){
        	IssueCustomValue customValue = new IssueCustomValue();
        	// No es registraran els canvis del valor perquès es recrea tal i com estava
        	customValue.setInstanceTracked(false);
        	customValue.setIssue(newIssue);
        	customValue.setField(value.getField());
        	customValue.setValue(value.getValue());
        	customValueDAO.createCustomValue(customValue);
        }
    }

    @Transactional
    @Override
    public AbstractIssue setIssueSLADate(Long issueId, boolean isSla)
    {
        AbstractIssue oldIssue = issueDAO.findIssueById(issueId);
        if(isSla && !oldIssue.isSlaDateModified()){
            Date slaDate = computeIssueSlaDate(oldIssue.getSendDate(), oldIssue.getSeverity());
            oldIssue.setSlaDate(slaDate);
        }
        else if(!isSla){
            oldIssue.setSlaDate(null);
            oldIssue.setSlaDateModified(false);
        }
        AbstractIssue newIssue = issueDAO.updateIssue(oldIssue);
        // S'actualitzen les alertes de la incidència
        try{
            createIssueAlerts(oldIssue, newIssue);
        }
        catch(BusinessException e){
            log.error("Error generant alertes per la incidència {}: {}", new Object[]{newIssue.getId(), e.getMessage(), e});
        }

        return newIssue;
    }

    @Transactional
    @Override
    public AbstractIssue setIssueSLADate(Long issueId, Date slaDate)
    {
        AbstractIssue oldIssue = issueDAO.findIssueById(issueId);
        oldIssue.setSlaDate(slaDate);
        oldIssue.setSlaDateModified(Boolean.TRUE);

        AbstractIssue newIssue = issueDAO.updateIssue(oldIssue);

        // S'actualitzen les alertes de la incidència
        try{
            createIssueAlerts(oldIssue, newIssue);
        }
        catch(BusinessException e){
            log.error("Error generant alertes per la incidència {}: {}", new Object[]{newIssue.getId(), e.getMessage(), e});
        }

        return newIssue;
    }
    
    @Override
    public Date computeIssueSlaDate(Date sendDate, IssueSeverity severity)
    {
        // El patró complirà l'expressió \\d[mwdh]
        String slaPattern = severity.getSlaPattern();
        // Mesos
        if(slaPattern.matches("\\dm")){
            int amount = Integer.parseInt(StringUtils.substringBefore(slaPattern, "m"));
            return DateUtils.addMonths(sendDate, amount);
        }
        // Setmanes
        else if(slaPattern.matches("\\dw")){
            int amount = Integer.parseInt(StringUtils.substringBefore(slaPattern, "w"));
            return DateUtils.addWeeks(sendDate, amount);
        }
        // Dies
        else if(slaPattern.matches("\\dd")){
            int amount = Integer.parseInt(StringUtils.substringBefore(slaPattern, "d"));
            return DateUtils.addDays(sendDate, amount);
        }
        // Hores
        else if(slaPattern.matches("\\dh")){
            int amount = Integer.parseInt(StringUtils.substringBefore(slaPattern, "h"));
            return DateUtils.addHours(sendDate, amount);
        }

        return null;
    }
    
    /* Repassar deprecats */    
    @Transactional
    @Override
    public int manageIssue(IIssueDTO issueDTO) 
    {
    	throw new UnsupportedOperationException("Mètode no suportat");        
    }

    @Transactional
    @Override
    public AbstractIssue createOrUpdateIssue(IIssueDTO issueDTO)
    {
    	throw new UnsupportedOperationException("Mètode no suportat");
    }

    /*---------------------------------*
     *              Tags               *
     *---------------------------------*/
    @Override
    @Transactional
    public void tagIssue(Long issueId, Long tagId)
    {
        Tag tag = tagDAO.findTagById(tagId);
        AbstractIssue issue = issueDAO.findIssueById(issueId);        
        // Si el tag s'acaba de crear es possible que la llista d'incidències sigui null
        if(tag.getIssues()==null){
            ArrayList<AbstractIssue> issues = new ArrayList<AbstractIssue>();
            issues.add(issue);
            tag.setIssues(issues);
        }
        else{
            tag.getIssues().add(issue);
        }
        tagDAO.updateTag(tag);
    }
        
    @Override
    @Transactional
    public void dettachIssueTag(Long issueId, Long tagId)
    {
        Tag tag = tagDAO.findTagById(tagId);                
        AbstractIssue issue = issueDAO.findIssueById(issueId);
        tag.getIssues().remove(issue);        
        tagDAO.updateTag(tag);
    }
    
    /*---------------------------------*
     *              Notes              *
     *---------------------------------*/
    @Override
    @Transactional
    public IssueNote createIssueNote(AbstractIssue issue, User user, String noteContents)
    {
    	IssueNote note = new IssueNote();
        note.setIssue(issue);
        note.setContent(noteContents);
        note.setReporter(user.toString());
        note.setLastEditionDate(new Date());
        note.setSendDate(new Date());
        
        return noteDAO.createNote(note);
    }
    
    @Override
    @Transactional
    public IssueNote updateIssueNote(Long noteId, String noteContents)
    {
    	IssueNote note = noteDAO.findNoteById(noteId);
        note.setContent(noteContents);
        note.setLastEditionDate(new Date());
        
        return noteDAO.updateNote(note);
    }
    
    @Override
    @Transactional
    public void deleteIssueNote(Long noteId)
    {
    	IssueNote note = noteDAO.findNoteById(noteId);
    	noteDAO.deleteNote(note);
    }

    /*---------------------------------*
     *             Alertes             *
     *---------------------------------*/
    @Override
    @Transactional
    public void createIssueAlerts(AlertType alertType)
    {
        List<AbstractIssue> issues = issueDAO.findAllIssues();
        for(AbstractIssue issue:issues){
            try{
                createIssueAlerts(alertType, issue, issue);
            }
            catch(BusinessException e){
                log.error("Error evaluant alerta {} sobre la incidència {}: {}", new Object[]{alertType.getName(), issue.getId(), e.getMessage(), e});
            }
        }
    }
    
    @Override
    @Transactional
    public void createIssueAlerts(AbstractIssue oldIssue, AbstractIssue newIssue) throws BusinessException
    {
        List<AlertType> alertTypes = alertTypeDAO.findAllAlertTypes();
        for(AlertType type:alertTypes){
            createIssueAlerts(type, oldIssue, newIssue);
        }
    }

    @Override
    @Transactional
    public void createIssueAlerts(AlertType alertType, AbstractIssue oldIssue, AbstractIssue newIssue) throws BusinessException
    {
        // Es busquen les alertes actives per la incidència
        List<IssueAlert> activeAlerts = alertDAO.findIssueAlertsByType(alertType, newIssue);
        // Es mira si la incidència compleix la regla del tipus
        try{
            boolean evaluationSucceeded = evaluateIssueAlert(alertType, oldIssue, newIssue);
            // Cas que s'hagi de crear l'alerta
            if(evaluationSucceeded && activeAlerts.isEmpty()){
                if(log.isDebugEnabled()){
                    log.debug("Creació d'alertes: Es crea una nova alerta de tipus {} per la incidència {}", alertType.getName(), newIssue.getId());
                }
                IssueAlert alert = new IssueAlert();
                alert.setIssue(newIssue);
                alert.setType(alertType);
                alertDAO.createAlert(alert);
            }
            // Cas que hi hagi alertes creades anteriorment que s'hagin d'esborrar per no ser vàlides
            else if(!evaluationSucceeded && !activeAlerts.isEmpty()){
                if(log.isDebugEnabled()){
                   log.debug("Creació d'alertes: La incidència {} no compleix la regla especificada al tipus {}; S'esborren les alertes existents", newIssue.getId(), alertType.getName());
                }
                for(IssueAlert alert:activeAlerts){
                    alertDAO.deleteAlert(alert);
                }
            }
            // Cas que s'ignori l'alerta
            else{
                if(log.isDebugEnabled()){
                   log.debug("Creació d'alertes: La incidència {} no compleix la regla especificada al tipus {}", newIssue.getId(), alertType.getName());
                }
            }
        }
        catch(ScriptException e){
            log.error("Error en evaluar el tipus d'alerta {} per la incidència {}: {}", new Object[]{alertType.getName(), newIssue.getId(), e.getMessage(), e});
            throw new BusinessException("Error en evaluar el tipus d'alerta " + alertType.getName() + " per la incidència " + newIssue.getId(), e);
        }
    }

    @Override
    @Transactional
    public void validateIssueAlerts(AlertType alertType)
    {
        List<IssueAlert> alerts = alertDAO.findAlertsByType(alertType);
        for(IssueAlert alert:alerts){
            try{
                boolean result = evaluateIssueAlert(alertType, alert.getIssue(), alert.getIssue());
                if(!result){
                   alertDAO.deleteAlert(alert);
                }
            }
            catch(ScriptException e){
                log.error("Error evaluant alerta {} sobre la incidència {}: {}", new Object[]{alertType.getName(), alert.getIssue().getId(), e.getMessage(), e});
            }
        }        
    }
    
    @Override
    @Transactional
    public boolean evaluateIssueAlert(AlertType type, AbstractIssue oldIssue, AbstractIssue newIssue) throws ScriptException
    {
        if(engine==null){
            ScriptEngineManager manager = new ScriptEngineManager();
            engine = manager.getEngineByName("js");
        }
        // S'obté l'script compila a executar
        CompiledScript script;
        if(alertScripts.containsKey(type)){
            script = alertScripts.get(type);
        }
        else{
            Compilable compileEngine = (Compilable)engine;
            script = compileEngine.compile(type.getAlertRule());
            alertScripts.put(type, script);
        }
        // Es prepara l'entorn d'execució
        Bindings bindings = engine.createBindings();
        bindings.put("oldIssue", oldIssue);
        bindings.put("newIssue", newIssue);
        Object scriptResult = script.eval(bindings);

        return Boolean.TRUE.equals(scriptResult);
    }        
        
    @Override
    @Transactional
    public void deleteIssueAlert(Long id)
    {
        IssueAlert alert = alertDAO.findAlertById(id);
        alertDAO.deleteAlert(alert);
    }
}

