package org.fenrir.vespine.core.service.impl;

import java.util.List;
import java.util.Collection;
import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.apache.commons.lang.StringUtils;
import org.fenrir.vespine.core.dao.IAlertTypeDAO;
import org.fenrir.vespine.core.dao.IIssueAlertDAO;
import org.fenrir.vespine.core.dao.IIssueCustomFieldDAO;
import org.fenrir.vespine.core.dao.IIssueCustomValueDAO;
import org.fenrir.vespine.core.dao.IIssueDAO;
import org.fenrir.vespine.core.dao.IIssueProjectDAO;
import org.fenrir.vespine.core.dao.ITagDAO;
import org.fenrir.vespine.core.dto.SearchQuery;
import org.fenrir.vespine.core.entity.AlertType;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueAlert;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueCustomValue;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.Tag;
import org.fenrir.vespine.core.helper.IssueOperationHelper;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.core.service.ISearchIndexService;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140613
 */
public class IssueSearchServiceImpl implements IIssueSearchService
{
    @Inject
    private ISearchIndexService indexService;
    
    @Inject
    private IIssueProjectDAO issueProjectDAO;

    @Inject
    private IIssueDAO issueDAO;

    @Inject
    private ITagDAO tagDAO;

    @Inject
    private IAlertTypeDAO alertTypeDAO;
    
    @Inject
    private IIssueAlertDAO alertDAO;
    
    @Inject
    private IIssueCustomFieldDAO customFieldDAO;
    
    @Inject
    private IIssueCustomValueDAO customValueDAO;
        
    public void setIndexService(ISearchIndexService indexService)
    {
        this.indexService = indexService;
    }
    
    public void setIssueProjectDAO(IIssueProjectDAO issueProjectDAO)
    {
        this.issueProjectDAO = issueProjectDAO;
    }

    public void setIssueDAO(IIssueDAO issueDAO)
    {
        this.issueDAO = issueDAO;
    }

    public void setTagDAO(ITagDAO tagDAO)
    {
        this.tagDAO = tagDAO;
    }

    public void setAlertTypeDAO(IAlertTypeDAO alertTypeDAO)
    {
        this.alertTypeDAO = alertTypeDAO;
    }
    
    public void setAlertDAO(IIssueAlertDAO alertDAO)
    {
        this.alertDAO = alertDAO;
    }
    
    public void setCustomFieldDAO(IIssueCustomFieldDAO customFieldDAO)
    {
    	this.customFieldDAO = customFieldDAO;
    }
    
    public void setCustomValueDAO(IIssueCustomValueDAO customValueDAO)
    {
    	this.customValueDAO = customValueDAO;
    }
    
    /*---------------------------------*
     *           Incidències           *
     *---------------------------------*/
    @Override
    @Transactional
    public List<AbstractIssue> findAllIssues()
    {
        return issueDAO.findAllIssues();
    }

    @Override
    @Transactional
    public List<AbstractIssue> findAllIssues(int page, int pageSize)
    {
        return issueDAO.findAllIssues(page, pageSize);
    }
    
    @Override
    @Transactional
    public long countAllIssues()
    {
        return issueDAO.countAllIssues();
    }

    @Override
    public SearchQuery buildIssueTermSearchQuery(String term)
    {
        return IssueOperationHelper.buildSearchQuery(term);
    }
    
    @Override
    public Collection<AbstractIssue> findIssueSearchHits(SearchQuery query, int page, int pageSize)
    {        
        query.setPage(page);
        query.setSize(pageSize);
        return indexService.findIssuesBySearchQuery(query);
    }
    
    @Override
    public long countIssueSearchHits(SearchQuery query)
    {
        return indexService.countDescriptionsBySearchQuery(query);
    }
    
    @Override
    @Transactional
    public List<AbstractIssue> findFilteredIssues(String filterQuery, String filterOrder, int page, int pageSize)
    {
        if(StringUtils.isNotBlank(filterOrder)){
            filterQuery += " order by " + filterOrder;
        }
        return issueDAO.findFilteredIssues(filterQuery, page, pageSize);
    }

    @Override
    @Transactional
    public boolean issueFilterMatches(String filterQuery, Long issueId)
    {
        StringBuilder queryPart = new StringBuilder();
        queryPart.append("from issue")
                .append(" where issue_id=").append(issueId)
                .append(" and issue.id in(")
                .append(" select issue.id ")
                .append(filterQuery)
                .append(")");

        return !issueDAO.findFilteredIssues(queryPart.toString()).isEmpty();
    }

    @Override
    @Transactional
    public long countFilteredIssues(String filterQuery)
    {
        return issueDAO.countFilteredIssues(filterQuery);
    }    
    
    @Override
    @Transactional
    public List<AbstractIssue> findTaggedIssues(Long tagId, int page, int pageSize)
    {
        Tag tag = tagDAO.findTagById(tagId);
        return issueDAO.findTaggedIssues(tag, page, pageSize);
    }

    @Override
    @Transactional
    public long countTaggedIssues(Long tagId)
    {
        Tag tag = tagDAO.findTagById(tagId);
        return issueDAO.countTaggedIssues(tag);
    }

    @Override
    @Transactional
    public boolean isIssueTagged(Long issueId, Long tagId)
    {
        Tag tag = tagDAO.findTagById(tagId);
        AbstractIssue issue = issueDAO.findTaggedIssue(issueId, tag);
        return issue!=null;
    }

    @Override
    @Transactional
    public List<AbstractIssue> findIssuesByProject(IssueProject project)
    {
    	return issueDAO.findIssuesByProject(project);
    }
    
    @Override
    @Transactional
    public long countIssuesByProject(IssueProject project)
    {
    	return issueDAO.countIssuesByProject(project);
    }
    
    @Override
    @Transactional
    public List<AbstractIssue> findIssuesByCategory(IssueCategory category)
    {
    	return issueDAO.findIssuesByCategory(category);
    }
    
    @Override
    @Transactional
    public long countIssuesByCategory(IssueCategory category)
    {
    	return issueDAO.countIssuesByCategory(category);
    }
    
    @Override
    @Transactional
    public List<AbstractIssue> findIssuesBySeverity(IssueSeverity severity)
    {
    	return issueDAO.findIssuesBySeverity(severity);
    }
    
    @Override
    @Transactional
    public long countIssuesBySeverity(IssueSeverity severity)
    {
    	return issueDAO.countIssuesBySeverity(severity);
    }
    
    @Override
    @Transactional
    public List<AbstractIssue> findIssuesByStatus(IssueStatus status)
    {
    	return issueDAO.findIssuesByStatus(status);
    }
    
    @Override
    @Transactional
    public long countIssuesByStatus(IssueStatus status)
    {
    	return issueDAO.countIssuesByStatus(status);
    }
    
    @Override
    @Transactional
    public AbstractIssue findIssueById(Long id)
    {
        return issueDAO.findIssueById(id);
    }
    
    @Override
    @Transactional
    public AbstractIssue findIssueByVisibleId(String visibleId)
    {
    	return issueDAO.findIssueByVisibleId(visibleId);
    }
    
    @Override
    @Transactional
    public AbstractIssue findLastModifiedIssueByProject(Long id)
    {
        IssueProject project = issueProjectDAO.findProjectById(id);
        return issueDAO.findLastModifiedIssueByProject(project);
    }

    /*---------------------------------*
     *             Alertes             *
     *---------------------------------*/
    @Override
    @Transactional
    public List<IssueAlert> findAllAlertsByType(Long alertType)
    {
        AlertType objAlertType = alertTypeDAO.findAlertTypeById(alertType);
        return alertDAO.findAlertsByType(objAlertType);
    }

    @Override
    @Transactional
    public List<IssueAlert> findIssueAlertsByIssueId(Long issueId)
    {
        AbstractIssue issue = issueDAO.findIssueById(issueId);
        return alertDAO.findIssueAlerts(issue);
    }

    /*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
    @Override
    @Transactional
    public List<IssueCustomValue> findCustomValuesByField(Long fieldId)
    {
    	IssueCustomField field = customFieldDAO.findCustomFieldById(fieldId);
    	return customValueDAO.findCustomValues(field);
    }
    
    @Override
    @Transactional
    public List<IssueCustomValue> findIssueCustomValues(Long issueId)
    {
    	AbstractIssue issue = issueDAO.findIssueById(issueId);
    	return customValueDAO.findIssueCustomValues(issue);
    }
}
