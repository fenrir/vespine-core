package org.fenrir.vespine.core.service.impl;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import com.google.inject.persist.Transactional;

import org.apache.commons.lang.StringUtils;
import org.fenrir.vespine.core.dao.IIssueDAO;
import org.fenrir.vespine.core.dao.IIssueWipRegistryDAO;
import org.fenrir.vespine.core.dao.IIssueWorkRegistryDAO;
import org.fenrir.vespine.core.dao.ISprintDAO;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueWipRegistry;
import org.fenrir.vespine.core.entity.IssueWorkRegistry;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.core.service.IIssueWorkService;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140719
 */
public class IssueWorkServiceImpl implements IIssueWorkService 
{
	@Inject
	private ISprintDAO sprintDAO;
	
	@Inject
	private IIssueWorkRegistryDAO issueWorkRegistryDAO;
	
	@Inject
	private IIssueDAO issueDAO;
	
	@Inject
	private IIssueWipRegistryDAO wipRegistryDAO;
	
	public void setSprintDAO(ISprintDAO sprintDAO)
	{
		this.sprintDAO = sprintDAO;
	}
	
	public void setIssueWorkRegistryDAO(IIssueWorkRegistryDAO IssueWorkRegistry)
	{
		this.issueWorkRegistryDAO = IssueWorkRegistry;
	}
	
	public void setIssueDAO(IIssueDAO issueDAO)
	{
		this.issueDAO = issueDAO;
	}
	
	public void setWipRegistryDAO(IIssueWipRegistryDAO wipRegistryDAO)
	{
		this.wipRegistryDAO = wipRegistryDAO;
	}
	
	@Override
	@Transactional
	public List<Sprint> findAllSprints()
	{
		return sprintDAO.findAllSprints();
	}
	
	@Override
	@Transactional
	public List<Sprint> findSprintsByProjectNameLike(String name)
	{
		return sprintDAO.findSprintsByProjectNameLike(name);
	}
	
	@Override
	@Transactional
	public List<Sprint> findSprintsByStartDateLike(String startDatePart)
	{
		Pattern pattern = Pattern.compile("\\d+");
		Matcher matcher = pattern.matcher(startDatePart);
		StringBuilder builder = new StringBuilder();
		while(matcher.find()){
			String elem = matcher.group(0);		
			if(builder.length()>0){
				builder.insert(0, "-");
			}
			builder.insert(0, elem);
		}
		return sprintDAO.findSprintsByStartDateLike(builder.toString());
	}
	
	@Override
	@Transactional
	public List<Sprint> findSprintsByEndDateLike(String endDatePart)
	{
		Pattern pattern = Pattern.compile("\\d+");
		Matcher matcher = pattern.matcher(endDatePart);
		StringBuilder builder = new StringBuilder();
		while(matcher.find()){
			String elem = matcher.group(0);		
			if(builder.length()>0){
				builder.insert(0, "-");
			}
			builder.insert(0, elem);
		}
		return sprintDAO.findSprintsByEndDateLike(builder.toString());
	}
	
	@Override
	@Transactional
	public Sprint findSprintById(Long id)
	{
		return sprintDAO.findSprintById(id);
	}
	
	@Override
	@Transactional
	public Sprint createSprint(IssueProject project, Date startDate, Date endDate)
	{
		Sprint sprint = new Sprint();
		sprint.setProject(project);
		sprint.setStartDate(startDate);
		sprint.setEndDate(endDate);
		return sprintDAO.createSprint(sprint);
	}
	
	@Override
	@Transactional
	public Sprint updateSprint(Long id, IssueProject project, Date startDate, Date endDate)
	{
		Sprint sprint = findSprintById(id);
		sprint.setProject(project);
		sprint.setStartDate(startDate);
		sprint.setEndDate(endDate);
		return sprintDAO.updateSprint(sprint);
	}
	
	@Override
	@Transactional
	public void deleteSprint(Long id)
	{
		Sprint sprint = sprintDAO.findSprintById(id);
    	long issues = issueDAO.countIssuesBySprint(sprint);
    	if(issues>0){
    		throw new IllegalArgumentException("No es pot eliminar l'sprint perquè té incidencies relacionades");
    	}
    	sprintDAO.deleteSprint(sprint);
	}
	
	@Override
	@Transactional
	public List<IssueWorkRegistry> findAllIssueWorkRegistriesByIssue(AbstractIssue issue)
	{
		return issueWorkRegistryDAO.findWorkRegistriesByIssue(issue);
	}
	
	@Override
	@Transactional
	public IssueWorkRegistry findIssueWorkRegistryById(Long id)
	{
		return issueWorkRegistryDAO.findWorkRegistryById(id);
	}
	
	@Override
	@Transactional
	public IssueWorkRegistry createIssueWorkRegistry(AbstractIssue issue, Date workingDay, boolean includeInSprint, String description, User user, Integer time)
	{
		IssueWorkRegistry registry = new IssueWorkRegistry();
		registry.setIssue(issue);
		if(StringUtils.isNotBlank(description)){
			registry.setDescription(description);
		}
		if(includeInSprint){
			Sprint sprint = sprintDAO.findSprintByDate(issue.getProject(), workingDay);
			registry.setSprint(sprint);
		}
		registry.setWorkingDay(workingDay);
		registry.setUser(user);
		registry.setTime(time);
		
		return issueWorkRegistryDAO.createWorkRegistry(registry);
	}
	
	@Override
	@Transactional
	public IssueWorkRegistry updateIssueWorkRegistry(Long id, AbstractIssue issue, Date workingDay, boolean includeInSprint, String description, User user, Integer time)
	{
		IssueWorkRegistry registry = issueWorkRegistryDAO.findWorkRegistryById(id);
		registry.setIssue(issue);
		if(StringUtils.isNotBlank(description)){
			registry.setDescription(description);
		}
		else{
			registry.setDescription(null);
		}
		if(includeInSprint){
			Sprint sprint = sprintDAO.findSprintByDate(issue.getProject(), workingDay);
			registry.setSprint(sprint);
		}
		else{
			registry.setSprint(null);
		}
		registry.setWorkingDay(workingDay);
		registry.setUser(user);
		registry.setTime(time);
		
		return issueWorkRegistryDAO.updateWorkRegistry(registry);
	}
	
	@Override
	@Transactional
	public void deleteIssueWorkRegistry(Long id)
	{
		IssueWorkRegistry registry = issueWorkRegistryDAO.findWorkRegistryById(id);
		if(registry!=null){
			issueWorkRegistryDAO.deleteWorkRegistry(registry);
		}
	}
	
	/*---------------------------------*
     *       Registres de timer        *
     *---------------------------------*/
	@Override
	@Transactional
	public List<IssueWipRegistry> findIssueWorkInProgressRegistriesByUser(User user)
	{
		return wipRegistryDAO.findRegistryByUser(user);
	}
	
	@Override
	@Transactional
	public IssueWipRegistry findIssueWorkInProgressRegistry(Long issueId, User user)
	{
		AbstractIssue issue = issueDAO.findIssueById(issueId);
		return wipRegistryDAO.findRegistryByUserIssue(issue, user);
	}
	
	@Override
	@Transactional
	public IssueWipRegistry findIssueWorkInProgressRegistryById(Long registryId)
	{
		return wipRegistryDAO.findRegistryById(registryId);
	}
		
	@Override
	@Transactional
	public IssueWipRegistry createWorkInProgressRegistry(Long issueId, User user)
	{
		AbstractIssue issue = issueDAO.findIssueById(issueId);
		
		IssueWipRegistry wipRegistry = wipRegistryDAO.findRegistryByUserIssue(issue, user);
		if(wipRegistry==null){
			wipRegistry = new IssueWipRegistry();
			wipRegistry.setIssue(issue);
			wipRegistry.setUser(user);
			wipRegistryDAO.createRegistry(wipRegistry);
		}
		
		return wipRegistry;
	}
		
	@Override
	@Transactional
	public void deleteWorkInProgressRegistry(Long id)
	{
		IssueWipRegistry registry = wipRegistryDAO.findRegistryById(id);
		wipRegistryDAO.deleteRegistry(registry);
	}
	
	@Override
	@Transactional
	public void deleteIssueWorkInProgressRegistry(Long issueId, User user)
	{
		AbstractIssue issue = issueDAO.findIssueById(issueId);
		IssueWipRegistry registry = wipRegistryDAO.findRegistryByUserIssue(issue, user);
		wipRegistryDAO.deleteRegistry(registry);
	}
}
