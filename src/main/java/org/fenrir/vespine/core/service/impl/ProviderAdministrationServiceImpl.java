package org.fenrir.vespine.core.service.impl;

import java.util.List;

import javax.inject.Inject;

import com.google.inject.persist.Transactional;

import org.fenrir.vespine.core.dao.IProviderCategoryDAO;
import org.fenrir.vespine.core.dao.IProviderProjectDAO;
import org.fenrir.vespine.core.dao.IProviderSeverityDAO;
import org.fenrir.vespine.core.dao.IProviderStatusDAO;
import org.fenrir.vespine.core.entity.ProviderCategory;
import org.fenrir.vespine.core.entity.ProviderProject;
import org.fenrir.vespine.core.entity.ProviderSeverity;
import org.fenrir.vespine.core.entity.ProviderStatus;
import org.fenrir.vespine.core.service.IProviderAdministrationService;

public class ProviderAdministrationServiceImpl implements IProviderAdministrationService 
{	
	@Inject
    private IProviderProjectDAO providerProjectDAO;
    
    @Inject
    private IProviderCategoryDAO providerCategoryDAO;
	
	@Inject
    private IProviderStatusDAO providerStatusDAO;
	
	@Inject
    private IProviderSeverityDAO providerSeverityDAO;
	
	public void setProviderStatusDAO(IProviderStatusDAO providerStatusDAO)
    {
    	this.providerStatusDAO = providerStatusDAO;
    }
    
    public void setProviderSeverityDAO(IProviderSeverityDAO providerSeverityDAO)
    {
    	this.providerSeverityDAO = providerSeverityDAO;
    }

    public void setProviderProjectDAO(IProviderProjectDAO providerProjectDAO)
    {
    	this.providerProjectDAO = providerProjectDAO;
    }
    
    public void setProviderCategoryDAO(IProviderCategoryDAO providerCategoryDAO)
    {
    	this.providerCategoryDAO = providerCategoryDAO;
    }
	
	/*---------------------------------*
     *            Projectes            *
     *---------------------------------*/
    @Override
    @Transactional
    public List<ProviderProject> findAllProviderProjects()
    {
    	return providerProjectDAO.findAllProjects();
    }
    
    @Override
    @Transactional
    public List<ProviderProject> findProviderProjectsByProvider(String provider)
    {
    	return providerProjectDAO.findProjectsByProvider(provider);
    }
    
    @Override
    @Transactional
    public ProviderProject findProviderProjectById(Long id)
    {
    	return providerProjectDAO.findProjectById(id);
    }
    
    @Override
    @Transactional
    public ProviderProject findProviderProjectByProviderData(String provider, String providerId)
    {
    	return providerProjectDAO.findProjectByProviderData(provider, providerId);
    }
	
	/*---------------------------------*
     *           Categories            *
     *---------------------------------*/
    @Override
    @Transactional
    public List<ProviderCategory> findAllProviderCategories()
    {
    	return providerCategoryDAO.findAllCategories();
    }
    
    @Override
    @Transactional
    public List<ProviderCategory> findProviderCategoriesByProvider(String provider)
    {
    	return providerCategoryDAO.findCategoriesByProvider(provider);
    }
    
    @Override
    @Transactional
    public ProviderCategory findProviderCategoryById(Long id)
    {
    	return providerCategoryDAO.findCategoryById(id);
    }
    
    @Override
    @Transactional
    public ProviderCategory findProviderCategoryByProviderData(String provider, String providerId)
    {
    	return providerCategoryDAO.findCategoryByProviderData(provider, providerId);
    }
	
	/*---------------------------------*
     *           Severitats            *
     *---------------------------------*/
    @Override
    @Transactional
    public List<ProviderSeverity> findAllProviderSeverities()
    {
    	return providerSeverityDAO.findAllSeverities();
    }
    
    @Override
    @Transactional
    public List<ProviderSeverity> findProviderSeveritiesByProvider(String provider)
    {
    	return providerSeverityDAO.findSeveritiesByProvider(provider);
    }
    
    @Override
    @Transactional
    public ProviderSeverity findProviderSeverityById(Long id)
    {
    	return providerSeverityDAO.findSeverityById(id);
    }
    
    @Override
    @Transactional
    public ProviderSeverity findProviderSeverityByProviderData(String provider, String providerId)
    {
    	return providerSeverityDAO.findSeverityByProviderData(provider, providerId);
    }
	
	/*---------------------------------*
     *             Estats              *
     *---------------------------------*/
	@Override
    @Transactional
    public List<ProviderStatus> findAllProviderStatus()
    {
    	return providerStatusDAO.findAllStatus();
    }
    
    @Override
    @Transactional
    public List<ProviderStatus> findProviderStatusByProvider(String provider)
    {
    	return providerStatusDAO.findStatusByProvider(provider);
    }
    
    @Override
    @Transactional
    public ProviderStatus findProviderStatusById(Long id)
    {
    	return providerStatusDAO.findStatusById(id);
    }
    
    @Override
    @Transactional
    public ProviderStatus findProviderStatusByProviderData(String provider, String providerId)
    {
    	return providerStatusDAO.findStatusByProviderData(provider, providerId);
    }
}
