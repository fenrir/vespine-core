package org.fenrir.vespine.core.service.impl;

import java.util.List;
import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.service.IUserManagementService;
import org.fenrir.vespine.core.dao.IUserDAO;
import org.fenrir.vespine.core.entity.User;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140504
 */
public class UserManagementServiceImpl implements IUserManagementService 
{
	@Inject
	private IUserDAO userDAO;
	
	public void setUserDAO(IUserDAO userDAO)
	{
		this.userDAO = userDAO;
	}
	
	@Override
	@Transactional
	public List<User> findAllActiveUsers()
	{
		return userDAO.findAllActiveUsers();
	}
	
	@Override
	@Transactional
	public List<User> findUsersCompleteNameLike(String name)
	{
		return userDAO.findUsersCompleteNameLike(name);
	}
	
	@Override
	@Transactional
	public List<User> findUsersUsernameLike(String name)
	{
		return userDAO.findUsersUsernameLike(name);
	}
	
	@Override
	@Transactional
	public User findUserByUsername(String username)
	{
		return userDAO.findUserByUsername(username);
	}
}
