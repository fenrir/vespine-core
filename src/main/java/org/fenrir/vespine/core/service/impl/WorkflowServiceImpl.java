package org.fenrir.vespine.core.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.apache.commons.collections.CollectionUtils;
import org.fenrir.vespine.core.dao.IIssueProjectDAO;
import org.fenrir.vespine.core.dao.IIssueStatusDAO;
import org.fenrir.vespine.core.dao.IWorkflowDAO;
import org.fenrir.vespine.core.dao.IWorkflowStepDAO;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.Workflow;
import org.fenrir.vespine.core.entity.WorkflowStep;
import org.fenrir.vespine.core.service.IWorkflowService;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140829
 */
public class WorkflowServiceImpl implements IWorkflowService 
{
	@Inject
	private IIssueProjectDAO projectDAO;
	
	@Inject
	private IWorkflowDAO workflowDAO;
	
	@Inject
	private IWorkflowStepDAO workflowStepDAO;
	
	@Inject
	private IIssueStatusDAO issueStatusDAO;
	
	public void setProjectDAO(IIssueProjectDAO projectDAO)
    {
        this.projectDAO = projectDAO;
    }
	
	public void setWorkflowDAO(IWorkflowDAO workflowDAO)
	{
		this.workflowDAO = workflowDAO;
	}
	
	public void setWorkflowStepDAO(IWorkflowStepDAO workflowStepDAO)
	{
		this.workflowStepDAO = workflowStepDAO;
	}
	
	public void setIssueStatusDAO(IIssueStatusDAO issueStatusDAO)
	{
		this.issueStatusDAO = issueStatusDAO;
	}
	
	@Override
	@Transactional
	public List<Workflow> findAllWorkflows() 
	{
		return workflowDAO.findAllWorkflows();
	}

	@Override
	@Transactional
	public List<IssueProject> findProjectsByWorkflow(Workflow workflow) 
	{
		return projectDAO.findProjectsByWorkflow(workflow);
	}

	@Override
	@Transactional
	public boolean hasWorkflowRelatedProjects(Workflow workflow) 
	{
		long projects = projectDAO.countProjectsByWorkflow(workflow);
		return projects>0;
	}
	
	@Override
	@Transactional
	public List<IssueStatus> findNextWorkflowStatus(IssueProject project, IssueStatus status)
	{
		List<IssueStatus> destinationStatus = new ArrayList<IssueStatus>();
		
		Workflow workflow = project.getWorkflow();
		// Si el projecte no té definit un workflow, es retornen tots els estats
		if(workflow==null){
			return issueStatusDAO.findAllStatus();
		}
		// En cas contrari es busca els següents estats depenent de l'actual
		else{
			if(status==null){
				destinationStatus.add(workflow.getInitialStatus());
			}
			else{
				List<WorkflowStep> steps = workflowStepDAO.findStepsBySourceStatus(workflow, status);
				for(WorkflowStep elem:steps){
					destinationStatus.add(elem.getDestinationStatus());
				}
			}
		}
		
		return destinationStatus;
	}

	@Override
	@Transactional	
	public Workflow findWorkflowById(Long id)
	{
		return workflowDAO.findWorkflowById(id);
	}
	
	@Override
	@Transactional	
	public WorkflowStep findWorkflowStepById(Long id)
	{
		return workflowStepDAO.findWorkflowStepById(id);
	}
	
	@Override
	@Transactional
	public Workflow createWorkflow(String name, IssueStatus initialStatus, List<WorkflowStep> steps) 
	{
		Workflow workflow = new Workflow();
		workflow.setName(name);
		workflow.setInitialStatus(initialStatus);
		workflow = workflowDAO.createWorkflow(workflow);
		
		List<WorkflowStep> persistedSteps = new ArrayList<WorkflowStep>();
		for(WorkflowStep step:steps){
			step.setWorkflow(workflow);
			persistedSteps.add(workflowStepDAO.createWorkflowStep(step));
		}
		// S'ha retornar amb els items carregats
		workflow.setSteps(persistedSteps);
		return workflow;
	}

	@Override
	@Transactional
	public Workflow updateWorkflow(Long id, String name, IssueStatus initialStatus, List<WorkflowStep> steps) 
	{
		Workflow workflow = workflowDAO.findWorkflowById(id);
		workflow.setName(name);
		workflow.setInitialStatus(initialStatus);
		Collection<WorkflowStep> diffSteps = CollectionUtils.disjunction(steps, workflow.getSteps());
		for(WorkflowStep step:diffSteps){
			if(workflow.getSteps().contains(step)){
				workflowStepDAO.deleteWorkflowStep(step);
				workflow.getSteps().remove(step);
			}
			else{
				WorkflowStep createdStep = workflowStepDAO.createWorkflowStep(step);
				workflow.getSteps().add(createdStep);
			}
		}
		workflow = workflowDAO.updateWorkflow(workflow);

		return workflow;
	}

	@Override
	@Transactional
	public void deleteWorkflow(Long id) 
	{
		Workflow workflow = workflowDAO.findWorkflowById(id);
    	long projects = projectDAO.countProjectsByWorkflow(workflow);
    	if(projects>0){
    		throw new IllegalArgumentException("No es pot eliminar el fluxe de treball perquè té projectes relacionats");
    	}
    	workflowDAO.deleteWorkflow(workflow);
	}
}
