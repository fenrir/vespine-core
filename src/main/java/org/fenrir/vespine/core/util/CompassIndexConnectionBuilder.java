package org.fenrir.vespine.core.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.io.FileUtils;
import org.compass.core.Compass;
import org.compass.core.config.CompassConfiguration;
import org.compass.core.config.CompassEnvironment;
import org.compass.core.lucene.engine.store.jdbc.ExternalDataSourceProvider;

/**
 * TODO Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140411
 */
public class CompassIndexConnectionBuilder 
{
	public static final int CONNECTION_FILESYSTEM = 1;
	public static final int CONNECTION_DATABASE = 2;
	
    private String storeDir;
    private String jdbcDialect;    
    private DataSource jdbcDatasource;
    private boolean restore;
    private List<Class<?>> managedEntities = new ArrayList<Class<?>>();
	
    public CompassIndexConnectionBuilder()
    {
        restore = false;
    }
	
    public CompassIndexConnectionBuilder(String storeDir, boolean restore)
    {
        this.storeDir = storeDir;
        this.restore = restore;
    }
	
    public CompassIndexConnectionBuilder setStoreDir(String storeDir)
    {
        this.storeDir = storeDir;
        return this;
    }
    
    public CompassIndexConnectionBuilder setJdbcDialect(String jdbcDialect)
    {
        this.jdbcDialect = jdbcDialect;
        return this;
    } 
    
    public CompassIndexConnectionBuilder setJdbcDatasource(DataSource jdbcDatasource)
    {
        this.jdbcDatasource = jdbcDatasource;
        return this;
    } 
    
    public CompassIndexConnectionBuilder setRestore(boolean restore)
    {
        this.restore = restore;
        return this;
    }
    
    public CompassIndexConnectionBuilder addManagedEntity(Class<?> entity)
    {
        managedEntities.add(entity);
        return this;
    }
    
    public Compass createConnection() throws Exception
    {
    	// S'esborra el directori i el seu contingut en cas d'indicar-se a les propietats de l'aplicació
        if(Boolean.valueOf(restore)){
            FileUtils.deleteDirectory(new File(storeDir));
        }
        CompassConfiguration compassConf = new CompassConfiguration();
        if(jdbcDatasource!=null){
        	compassConf.setSetting(CompassEnvironment.CONNECTION, "jdbc://");
        	compassConf.setSetting("compass.engine.store.jdbc.dialect", jdbcDialect);
        	ExternalDataSourceProvider.setDataSource(jdbcDatasource);
        	compassConf.setSetting("compass.engine.store.jdbc.connection.provider.class", "org.compass.core.lucene.engine.store.jdbc.ExternalDataSourceProvider");
        }
        else{
        	compassConf.setSetting(CompassEnvironment.CONNECTION, storeDir);
        }
        compassConf.setSetting("compass.transaction.factory", "org.compass.core.transaction.LocalTransactionFactory");                
        // Enitats gestionades
        for(Class<?> entity:managedEntities){
            compassConf.addClass(entity);
        }

        return compassConf.buildCompass();
    }
}