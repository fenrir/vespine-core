package org.fenrir.vespine.core.util;

import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.vespine.spi.util.IAttributeConverter;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140528
 */
public class DictionaryItemDTOConverter implements IAttributeConverter<IDictionaryItemDTO, String> 
{
	private final Logger log = LoggerFactory.getLogger(DictionaryItemDTOConverter.class);
	
	@Inject
	private IAdministrationFacade administrationService;
	
	public void setAdministrationService(IAdministrationFacade administrationService)
	{
		this.administrationService = administrationService;
	}
	
	@Override
	public void setParameter(String name, String value) 
	{
		// No aplica
	}

	@Override
	public String convertToDatabaseColumn(IDictionaryItemDTO attributeValue) 
	{
		return attributeValue.getItemId();
	}

	@Override
	public IDictionaryItemDTO convertToEntityAttribute(String columnValue) 
	{
		try{
			return administrationService.findDictionaryItem(columnValue);
		}
		catch(BusinessException e){
			log.error("Error recuperant item de diccionari {}: {}", new Object[]{columnValue, e.getMessage(), e});
			return null;
		}
	}
}
