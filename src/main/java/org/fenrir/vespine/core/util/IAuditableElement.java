package org.fenrir.vespine.core.util;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140418
 */
public interface IAuditableElement 
{
	public Long getAuditRecordReference();
}
